<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceFeesPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_fees_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('paid_amount');
            $table->integer('invoice_fee_id');
            $table->integer('payment_id');
            $table->smallInteger('status');
            $table->timestamps();
            $table->foreign('invoice_fee_id')->references('id')->on('invoice_fees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('payment_id')->references('id')->on('payments')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_fees_payments');
    }
}
