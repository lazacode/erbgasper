<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW invoice_view AS
 SELECT b.invoice_id AS id,
    c.name,
    a.number,
    a.user_id,
        CASE
            WHEN (a.status = 1) THEN 'Paid'::text
            WHEN (a.status = 0) THEN 'Unpaid'::text
            ELSE 'Partially Paid'::text
        END AS status,
    a.active,
    a.sync,
    a.return_message,
    a.push_status,
    sum(b.amount) AS amount,
    sum(d.paid_amount) AS paid_amount,
    a.amount_type,
    a.currency,
    a.payment_type,
    (a.created_at)::date AS created_at,
    (sum(b.amount) - sum(d.paid_amount)) AS unpaid_amount,
    'Event Fee'::text AS payment_for
   FROM (((invoices a
     JOIN invoice_fees b ON ((b.invoice_id = a.id)))
     LEFT JOIN invoice_fees_payments d ON ((d.invoice_fee_id = b.id)))
     JOIN users c ON ((c.id = a.user_id)))
  GROUP BY a.number, a.user_id, a.status, a.active, a.sync, a.return_message, a.push_status, b.invoice_id, c.name, a.amount_type, a.currency, a.payment_type, a.created_at");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW invoices_view");
    }
}
