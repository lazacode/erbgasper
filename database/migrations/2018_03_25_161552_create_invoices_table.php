<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number')->unique();
            $table->integer('user_id');
            $table->string('title')->nullable();
            $table->string('optional_name')->nullable();
            $table->timestampTz('date');
            $table->smallInteger('status')->nullable();
             $table->smallInteger('type')->default(0);
            $table->string('year')->nullable();
            $table->smallInteger('active')->default(1);
            $table->smallInteger('sync')->default(0);
            $table->string('return_message')->nullable();
            $table->string('push_status')->nullable()->comment('Final push status for such invoice. It can be invoice update or invoice submission ');
            $table->mediumText('note')->nullable();
            $table->string('amount_type')->default('FIXED');
            $table->string('currency')->default('TZS');
            $table->integer('payment_type')->default(901);
            $table->string('bil_id')->nullable();
            $table->string('invoice_type')->default('normal');

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('invoices');
    }

}
