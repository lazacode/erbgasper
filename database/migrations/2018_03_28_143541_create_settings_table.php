<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('website');
            $table->string('email');
            $table->string('box');
            $table->string('api_key');
            $table->string('api_secret');
            $table->string('institution_code');
            $table->string('create_invoice_url');
            $table->string('username');
            $table->string('password');
            $table->string('mno_number');
            $table->smallInteger('sms_type')->default(1);
            $table->smallInteger('site_mode')->default(1);
            $table->date('payment_deadline');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('settings');
    }

}
