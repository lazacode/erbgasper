<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceFeesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoice_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invoice_id');
            $table->integer('event_id');
            $table->integer('virtual');
            $table->integer('virtual_material')->nullable();
            $table->float('amount');
            $table->string('paymenttype')->nullable();
            $table->timestampTz('paid_date')->nullable();
            $table->smallInteger('status')->default(0)->comment('checking if amount is paid, 0-not paid,1-paid,2-partially paid');
            $table->string('note');
            $table->string('item_name');
            $table->timestamps();
            $table->foreign('invoice_id')->references('id')->on('invoices')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('event_id')->references('id')->on('events')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('invoice_fees');
    }

}
