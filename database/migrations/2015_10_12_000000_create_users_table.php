<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable()->unique();
            $table->string('password');
            $table->integer('user_type_id');
            $table->integer('role_id')->nullable();
            $table->integer('employer_id');
            $table->integer('is_employer')->default(0);
            $table->integer('profession_id')->nullable();
            $table->string('number')->nullable()->unique();
            $table->integer('created_by')->nullable();
            $table->smallInteger('site_visit')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
