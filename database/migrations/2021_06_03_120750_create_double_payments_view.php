<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoublePaymentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::statement("CREATE VIEW double_payments AS
 SELECT count(*) AS count,
    payments.invoice_id
   FROM payments
  GROUP BY payments.invoice_id
 HAVING (count(payments.invoice_id) > 1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('double_payments_view');
    }
}
