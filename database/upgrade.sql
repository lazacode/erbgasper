
/**
 * Author:  Ephraim
 * Created: 13 Jul 2020
 */

alter table erb_payment.fees add column virtual_amount numeric;
alter table erb_payment.fees add column virtual_material_amount numeric;
alter table erb_payment.users add column virtual smallint;
alter table erb_payment.users add column virtual_material smallint;
alter table erb_payment.settings add column max_attendee integer;
insert into erb_payment.financial_entity select * from erb_payment.financial_entity;
insert into erb_payment.api select * from erb_payment.api;
insert into erb_payment.events select * from erb_payment.events;
insert into erb_payment.fees select * from erb_payment.fees;
insert into erb_payment.employers select * from erb_payment.employers;
insert into erb_payment.permission_groups select * from erb_payment.permission_groups;
insert into erb_payment.permissions select * from erb_payment.permissions;
insert into erb_payment.professions select * from erb_payment.professions;
insert into erb_payment.roles select * from erb_payment.roles;
insert into erb_payment.role_permissions select * from erb_payment.role_permissions;
insert into erb_payment.settings select * from erb_payment.settings;
insert into erb_payment.user_roles select * from erb_payment.user_roles;
insert into erb_payment.user_types select * from erb_payment.user_types;
select public.reset_sequence();
ALTER SEQUENCE erb2020.user_types_id_seq RESTART WITH 200;