<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/create','ApiController@create');
Route::post('/accept_payment','PaymentController@store');
Route::post('/init','ApiController@api');
/**
 * ------------------------------------------------
 * scancode api requests
 */
Route::post('/scancode/count_applicants','ApiController@countApplicants');
Route::post('/scancode/list_applicants','ApiController@listApplicants');
Route::post('/scancode/applicant_detail','ApiController@applicantDetails');
/**
 * ------------------------------------------------
 */
Route::post('/webhook', 'MeetingController@webhook');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    Route::post('/createInvoice','ApiController@create');
    return $request->user();
});
