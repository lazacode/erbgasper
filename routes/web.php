<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes, Live New App
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/resentAllBarcodes', function () {
    $payments = \App\Model\Payment::all();
    $api = (new \App\Http\Controllers\ApiController());
    foreach ($payments as $payment) {
        $api->sendBarcodeForm($payment->id);
    }
});

Auth::routes();
Route::get('/testing', 'ApiController@sendBarcodeForm');

Route::group(['middleware' => 'web'], function() {
    Route::get('/find', 'HomeController@search');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/user/profile/{id}', 'UserController@profile');
    Route::get('/user/ticket/{id}', 'UserController@ticket');
    Route::get('/user/site/{id}', 'UserController@site');
    Route::get('/myticket/{id}', 'SettingController@downloadTicket');

    Route::get('/user/nametag/{id}', 'UserController@nametag');
    Route::any('/user/password', 'UserController@password');
    Route::get('/download/{id}', 'BookingController@downloadPdf');
    Route::get('/search', 'BookingController@search')->name('search');
    Route::get('/search_certificate', 'certificateController@search');
    Route::get('/invoice/feeDelete/{id}', 'InvoiceController@feeDelete');
    Route::post('/invoice/addUserFee/{id}', 'InvoiceController@addUserFee');
    Route::post('/user/getOrganization', 'UserController@getOrganization');
    Route::any('/booking/{id}/card', 'PaymentController@card');
    Route::get('/booking/getAmount', 'BookingController@getNewFeeAmount');
    Route::get('/create_new/{id}', 'BookingController@createNewInvoice');
    Route::post('/setting/getedit', 'SettingController@getEdits');
    Route::post('/certificate/ajax', 'certificateController@showData');
    Route::post('ajaxTable', 'HomeController@dataTable');
    Route::any('/payment/export', 'PaymentController@export');
    Route::any('/users/export/{id}/{event?}', 'UserController@export');
    
     Route::any('/booking/checkEmail', 'BookingController@checkEmail');
      Route::any('/booking/checkPhone', 'BookingController@checkPhone');


    Route::resource('booking', 'BookingController');
    Route::resource('invoice', 'InvoiceController');
    Route::resource('payment', 'PaymentController');
    Route::resource('user', 'UserController');
    Route::resource('inbox', 'InboxController');
    Route::resource('setting', 'SettingController');
    Route::resource('certificate', 'certificateController');


    Route::get('/user/applicants/{id?}/{pg?}', 'UserController@show');
    Route::resource('meeting', 'MeetingController');

        //test
    Route::get('/reports/{id}', 'MeetingController@get_report');
    Route::get('/getmeeting/{id}', 'MeetingController@get');
    Route::get('/participantsreports/{id}', 'MeetingController@get_participants_report');
    Route::get('/testzoom/{id}', 'MeetingController@add_participant');
    Route::get('/report/{id}/{type}', 'HomeController@report');
    Route::get('/getparticipants/{id}', 'MeetingController@getMeetingParticipants');
});

Route::get('/', 'BookingController@landing');
Route::post('/tinymce', function(){
    print_r(request()->all());
});
//Route::get('/create_billing', 'InvoiceController@createBilling');
Route::get('/update_billing', 'InvoiceController@updateBilling');
//Route::get('/update_control', 'InvoiceController@updateControl');

