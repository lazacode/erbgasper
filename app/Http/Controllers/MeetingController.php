<?php

namespace App\Http\Controllers;

//use App\Traits\ZoomMeetingTrait;

use App\Http\Controllers\ZoomMeetingTrait;
use App\Model\Event;
use App\Model\User;
use App\Model\UserMeeting;
use App\Model\ZoomMeeting;
use Illuminate\Http\Request;
use DB;

class MeetingController extends Controller {

    use ZoomMeetingTrait;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

//    function calc($da) {
//        $data['topic'] = 'Imekubali 2';
//        $data['start_time'] = now()+30;
//        $data['agenda'] = 'Hii ni kubwa Kuliko';
//        $data['host_video'] = 1;
//        $data['participant_video'] = 1;
//        return $this->create($data);
//    }

    public function index($id, $data) {
        $response = $this->create($data);
        DB::table('api_requests')->insert(['content' => json_encode($response)]);
        if (!empty($response)) {
            $response = $response['data'];
            ZoomMeeting::create([
                'uuid' => $response['uuid'],
                'meeting_id' => $response['id'],
                'meeting_url' => $response['join_url'],
                'topic' => $response['topic'],
                'event_id' => $id,
            ]);
        }
        return $this->generateZoomToken();
    }

    public function show($id) {
        $events = ZoomMeeting::all();
        $meeting = $this->get($id);

        if ($meeting['success'] == true) {
            $participants = $this->get_participants($id);
            $par = collect($participants['data']['registrants']);
            $thisMeeting = ZoomMeeting::where('meeting_id', $id)->first();
            $attended = UserMeeting::where(['zoom_meeting_id' => $thisMeeting->id, 'status' => 'Joined'])->get();
//        $participants_report = $this->get_participants_report($id);
//        if (count($participants_report) > 0) {
//            $report = $participants_report['data']['participants'];
//        } else {
//            $report = [];
//        }
//        return $report;
            //return $par->where('status','approved');
            return view('meeting.index')->with(['events' => $events, 'meeting' => $meeting, 'participants' => $par, 'attended' => $attended]);
        } else {
            return view('meeting.false_response')->with(['events' => $events]);
        }
    }

    public function store(Request $request) {
        $this->create($request->all());

        return redirect()->route('meetings.index');
    }

    public function update($meeting, Request $request) {
        $this->update($meeting->zoom_meeting_id, $request->all());

        return redirect()->route('meetings.index');
    }

    public function destroy(ZoomMeeting $meeting) {
        $this->delete($meeting->id);

        return $this->sendSuccess('Meeting deleted successfully.');
    }

    public function webhook(Request $request) {
        $event = $request->event;
        $payload = $request->payload;
        $object = $payload['object'];
        $meetingId = $object['id'];
        if ($event == "meeting.started") {
            //return $object['id'];
        } elseif ($event == "meeting.ended") {
            //return $object['id'];
        } elseif ($event == "meeting.participant_joined") {
            $participant = $object['participant'];
            $email = $participant['email'];
            $user = User::where('email', $email)->first();
            if (!empty($user) && (int) $meetingId > 0) {
                $meeting = ZoomMeeting::where('meeting_id', $meetingId)->first();
                $m = UserMeeting::where(['zoom_meeting_id' => $meeting->id, 'user_id' => $user->id])->first();
                if (!empty($m)) {
                    $m->update(['status' => 'Joined']);
                }
            }

            return $participant;
        } elseif ($event == "meeting.participant_left") {
            $participant = $object['participant'];
            return $participant->email;
        }
        return $request->event;
    }

}
