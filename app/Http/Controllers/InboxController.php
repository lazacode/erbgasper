<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Sms;
use \App\Model\Email;
use \App\Model\Sms_template;
use \App\Model\Schedule;
use \App\Model\User;
use \App\Http\Controllers\ApiController;

class InboxController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['sms'] = [];
        return view('inbox.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'template') {
            $this->data['templates'] = Sms_template::all();
            return view('inbox.template', $this->data);
        } else if ($id == 'getTemplate') {
            $template_id = request('template');
            $template = Sms_template::find($template_id);
            return !empty($template) ? $template->message : '';
        } else if ($id == 'schedule') {
            $this->data['schedules'] = Schedule::all();
            return view('inbox.schedule', $this->data);
        } else if ($id == 'resend') {
            request('type') == 'email' ? Email::find(request('id'))->update(['status' => 0]) :
                            Sms::find(request('id'))->update(['status' => 0]);
            return 1;
        } else if ($id == 'email') {
            $this->data['emails'] = [];
            return view('inbox.email', $this->data);
        } else if ($id == 'attachment') {
            $this->data['attachments'] = [];
            return view('inbox.attachment', $this->data);
        } else if ($id == 'resend_barcode') {
            $payment = \App\Model\Payment::find(request('id'));
            (new PaymentController())->sendBarcodeForm($payment->invoice);
            $payment->update(['receipt_sent' => 1]);
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    public function sendSms() {
        $message = request('message');
        $to = request('to');

        $event_id = request('event_id');

        $arr = ['virtual' => preg_match('/virtual/i', $to) ? 1 : 0];

        $invoicesIDs = \App\Model\Invoice_fee::where(array_merge(['event_id' => $event_id], $arr))->get('invoice_id');
        $usersList = \App\Model\InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
        if ($to == 'virtual') {
            $users = User::whereIn('id', $usersList)->get();
        } else if ($to == 'physical') {
            $users = User::where('is_employer', '<>', 1)->whereIn('id', $usersList)->get();
        } else if (preg_match('/virtual_sponsored/i', $to)) {
            $users = User::where('user_type_id', preg_replace('/[a-z_]/i', NULL, $to))->get();
        } else if (preg_match('/physical_sponsored/i', $to)) {
            $users = User::where('is_employer', '<>', 1)->whereIn('id', $usersList)->where('user_type_id', preg_replace('/[a-z_]/i', NULL, $to))->get();
        } else {

            $users = $to == 0 ? User::whereIn('id', $usersList)->get() :
                    User::where('user_type_id', $to)->whereIn('id', $usersList)->get();
        }
        if ($to == 'write') {
            $phones = explode(',', request('phone'));
            foreach ($phones as $phone) {
                $this->send_sms($phone, $message);
            }
            return false;
        }
        $zoom_meeting = \App\Model\ZoomMeeting::where('event_id', $event_id)->first();
        foreach ($users as $user) {
            $patterns = array(
                '/#name/i', '/#paid_amount/i', '/#zoom_meeting_id/i', '/#zoom_meeting_url/i', '/#event_name/i'
            );
            $replacements = array(
                $user->name,
                $user->payment()->sum('amount'),
                !empty($zoom_meeting) ? $zoom_meeting->meeting_id : '',
                !empty($zoom_meeting) ? $zoom_meeting->meeting_url : '',
                !empty($zoom_meeting) ? $zoom_meeting->event->name : ''
            );
            $sms = preg_replace($patterns, $replacements, $message);
            $this->send_sms($user->phone, $sms);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        request('t') == 'email' ? Email::find($id)->delete() : Sms::find($id)->delete();
        return redirect()->back()->with('success', 'Deleted');
    }

}
