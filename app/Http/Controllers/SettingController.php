<?php

namespace App\Http\Controllers;

use App\Model\Invoice;
use App\Model\Invoice_fee;
use App\Model\InvoiceUser;
use App\Model\ZoomMeeting;
use Illuminate\Http\Request;
use \App\Model\User_type;
use \App\Model\Role;
use \App\Model\Fee;
use \App\Model\Event;
use \App\Model\User;
use \App\Model\Setting;
use \App\Model\Profession;
use \App\Model\Sms_template;
use \App\Model\Zoom;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PDF;
use DB;

class SettingController extends Controller {

    public function __construct() {
        if (request('auth') == NULL) {
            $this->middleware('auth');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['setting'] = Setting::first();
        return view('setting.index', $this->data);
    }

    public function createBarCode($user_id) {
        $user = User::find($user_id);
        $barcode = new BarcodeGenerator();
        $barcode->setText($user->number);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        return $barcode->generate();
    }

    public function newCreateBarCode($user_id, $event_id) {
        $user = User::find($user_id);
        $barcode = new BarcodeGenerator();
        $barcode->setText($user->number);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        return $barcode->generate();
    }

    public function createTicketParam($user_id) {
        $this->data['barcode'] = $this->createBarCode($user_id);
        $this->data['event'] = Event::first();
        $this->data['user'] = User::find($user_id);
        $invoice = \App\Model\Invoice::where('user_id', $user_id)->first();
        if (!empty($invoice)) {
            $this->data['payment'] = $invoice->payment()->first();
        } else {
            $this->data['payment'] = [];
        }
        $this->data['padding_ticket'] = 1;
        $this->data['id'] = $user_id;
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return view('email.ticket_attachment', $this->data);
    }

    public function downloadTicket($user_id) {

        $this->data['barcode'] = $this->createBarCode($user_id);
        $userInvoices = InvoiceUser::where('user_id', $user_id)->orderBy('id', 'desc')->limit(1)->get('invoice_id');
        $invoices = Invoice::whereIn('id', $userInvoices)->get();
        $fee = Invoice_fee::where('virtual', 0)->whereIn('invoice_id', $userInvoices)->first();
        if (!empty($fee)) {
            $this->data['event'] = Event::find($fee->event_id);
            $this->data['user'] = User::find($user_id);
            $invoice = Invoice::where(['id' => $fee->invoice_id])->first();
            if (!empty($invoice)) {
                $this->data['payment'] = $invoice->payment()->first();
            } else {
                $this->data['payment'] = [];
            }

            PDF::setOptions(['defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('email.ticket_attachment', $this->data);
            return $pdf->download('receipt.pdf');
        }
    }

    public function getUserTicket($user_id = null) {
        $this->data['barcode'] = $this->createBarCode($user_id);
        $this->data['event'] = Event::first();
        $this->data['user'] = User::find($user_id);
        $invoice = \App\Model\Invoice::where('user_id', $user_id)->first();
        if (!empty($invoice)) {
            $this->data['payment'] = $invoice->payment()->first();
        } else {
            $this->data['payment'] = [];
        }
        $this->data['padding_ticket'] = 1;
        $this->data['id'] = $user_id;
        return view('email.ticket_attachment', $this->data);
    }

    public function getUserTicketNew($user_id = null) {
        $this->data['barcode'] = $this->createBarCode($user_id);
        $userInvoices = InvoiceUser::where('user_id', $user_id)->orderBy('id', 'desc')->limit(1)->get('invoice_id');
        $invoices = Invoice::whereIn('id', $userInvoices)->get();
        $fee = Invoice_fee::whereIn('invoice_id', $userInvoices)->first();
        if (!empty($fee)) {
            $this->data['event'] = Event::find($fee->event_id);
            $this->data['user'] = User::find($user_id);
            $invoice = Invoice::where(['id' => $fee->invoice_id])->first();
            if (!empty($invoice)) {
                $this->data['payment'] = $invoice->payment()->first();
            } else {
                $this->data['payment'] = [];
            }
            $this->data['padding_ticket'] = 1;
            $this->data['id'] = $user_id;
            return view('email.ticket_attachment', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function addPermission() {
        $permission_id = request('id');
        $role_id = request('role_id');
        $data = array(
            'role_id' => $role_id,
            'permission_id' => $permission_id
        );
        $insert_id = DB::table('role_permissions')->insertGetId($data, 'id');
        if ($insert_id > 0) {
            echo 'Success: Added';
        } else {
            echo 'Error: Please Refresh';
        }
    }

    public function removePermission() {
        $permission_id = request('id');
        $role_id = request('role_id');
        $data = array(
            'role_id' => $role_id,
            'permission_id' => $permission_id
        );
        DB::table('role_permissions')->where($data)->delete();
        echo 'Success : Removed';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (isset($request->tag)) {
            return $this->{$request->tag}();
        } else {
            $this->validate($request, [
                "name" => "required",
                "phone" => "required",
                "email" => "required|email",
                "box" => "required",
                "address" => "required"
            ]);
            $setting = Setting::first();
            if(request()->site_mode==3){
                Invoice::where('status',1)->update(['status'=>0]);
            }
            empty($setting) ? Setting::create($request->all()) : $setting->update($request->all());
            return redirect()->back()->with('success', ' Information added successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'role') {
            $this->data['roles'] = Role::all();
            return view('setting.role', $this->data);
        } else if ($id == 'fee') {
            $this->data['events'] = Event::where('status', 1)->get();
            $this->data['fees'] = Fee::all();
            return view('setting.fee', $this->data);
        } else if ($id == 'event') {
            $this->data['events'] = Event::all();
            return view('setting.event', $this->data);
        } else if ($id == 'profession') {
            $this->data['professions'] = Profession::all();
            return view('setting.profession', $this->data);
        } else if ($id == 'permission') {
            $this->data['roles'] = Role::all();
            $this->data['role_id'] = request('id');
            $this->data['groups'] = \App\Model\Permission_group::all();
            return view('setting.permission', $this->data);
        } else if ($id == 'report') {
            $this->data['reports'] = DB::select('select created_at::date, count(*) from nametag_printlogs group by created_at::date');
            return view('setting.reports', $this->data);
        } else if ($id == 'zoom') {
            $this->data['events'] = Event::where('status', 1)->get();
            $this->data['zoomlinks'] = ZoomMeeting::all();
            return view('setting.zoom', $this->data);
        } else {
            $this->data['users'] = User_type::all();
            return view('setting.usertype', $this->data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (request('user') == 'fee') {
            $validator = Validator::make(request()->all(), [
                        'event_id' => [
                            'required',
                            Rule::unique('fees')->ignore($id, 'id')
                        ],
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return redirect()->back()->with('error', $errors->first());
            }
            Fee::find($id)->update(request()->all());
        } else if (request('user') == 'event') {
            //return request()->all();
            $event = Event::find($id);
             
            if ($event->virtual == "on") {
                if (!empty(request()->virtual)) {
                    $values = array_merge(request()->all(), ['virtual' => 'on']);
                    Event::find($id)->update($values);

                    $zoom = ZoomMeeting::where('event_id', $id);
                    if ($zoom->count() == 0) {
                        $data['topic'] = $event->name;
                        $data['start_time'] = $event->date;
                        $data['duration'] = 100000;
                        $data['agenda'] = $event->theme;
                        $data['host_video'] = 1;
                        $data['participant_video'] = 1;
                        $createMeeting = (new \App\Http\Controllers\MeetingController())->index($event->id, $data);
                    }
                } else {
                    $virtual = 'off';
                    $values = array_merge(request()->all(), ['virtual' => 'off']);
                    Event::find($id)->update($values);
                }
            } else {
              
                if (!empty(request()->virtual)) {
                    $values = request()->all();
                    Event::find($id)->update($values);
                   
                    //check if meeting is available
                    $totalmeeting = ZoomMeeting::where('event_id', $id)->count();
                     
                    if ($totalmeeting == 0) {
                        $data['topic'] = request()->name;
                        $data['start_time'] = request()->date;
                        $data['duration'] = 100000;
                        $data['agenda'] = request()->theme;
                        $data['host_video'] = 1;
                        $data['participant_video'] = 1;
                        $createMeeting = (new \App\Http\Controllers\MeetingController())->index($id, $data);
                    }
                } else {
                    $virtual = 'off';
                  
                    $values = array_merge(request()->all(), ['virtual' => 'off']);
                    Event::find($id)->update($values);
                    $zoom = ZoomMeeting::where('event_id', $id);
                     
                    if ($zoom->count() > 0) {
                        $zoom->delete();
                    }
                }
            }
        } else if (request('user') == 'role') {
            Role::find($id)->update(request()->all());
        } else if (request('user') == 'user_type') {
            User_type::find($id)->update(request()->all());
        } else if (request('user') == 'user') {
              $validator = Validator::make(request()->all(), [
                        'email' => [
                            'required',
                            Rule::unique('users')->ignore($id, 'id')
                        ],
            ]);
            User::find($id)->update(request()->all());
        } else if (request('user') == 'profession') {
            Profession::find($id)->update(request()->all());
        } else if (request('user') == 'sms_template') {
            Sms_template::find($id)->update(request()->all());
        }
        return redirect()->back()->with('success', 'Information updated successfully.');
    }

    public function getEdits() {
        if (request('table') == 'event') {
            $edits = Event::find(request('id'))->toJson();
        } else if (request('table') == 'fee') {
            $edits = Fee::find(request('id'))->toJson();
        } else if (request('table') == 'role') {
            $edits = Role::find(request('id'))->toJson();
        } else if (request('table') == 'user_type') {
            $edits = User_type::find(request('id'))->toJson();
        } else if (request('table') == 'user') {
            $edits = User::find(request('id'))->toJson();
        } else if (request('table') == 'profession') {
            $edits = Profession::find(request('id'))->toJson();
        } else if (request('table') == 'sms_template') {
            $edits = Sms_template::find(request('id'))->toJson();
        } else if (request('table') == 'eventsfee') {
            if (request('event_id') == 0) {
                $amount = Invoice_fee::where(['invoice_id' => request('invoice_id')])->sum('amount');
                $users = InvoiceUser::where(['invoice_id' => request('invoice_id')])->count();
                return $totalamount = $amount * $users;
            } else {
                $amount = Invoice_fee::where(['event_id' => request('event_id'), 'invoice_id' => request('invoice_id')])->sum('amount');
                $users = InvoiceUser::where(['invoice_id' => request('invoice_id')])->count();
                return $totalamount = $amount * $users;
            }
        }
        return $edits;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
