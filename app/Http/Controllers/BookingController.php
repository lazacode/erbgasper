<?php

namespace App\Http\Controllers;

use App\Model\Fee;
use App\Model\UserEvent;
use App\Model\UserMeeting;
use App\Model\ZoomMeeting;
use Illuminate\Http\Request;
use \App\Model\User;
use \App\Model\Invoice;
use \App\Model\Invoice_fee;
use \App\Model\InvoiceUser;
use \App\Model\Setting;
 use\App\Model\Event;
use PDF;
use Auth;
use DB;

class BookingController extends Controller {

    public $setting;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['landing'] = 1;
        $fees = Fee::all('event_id');
        $this->data['events'] = Event::where('status', 1)->whereIn('id', $fees)->get();
        return view('auth.booking', $this->data);
    }

    public function getFeeAmount($type = null, $option = null) {
        $fee = \App\Model\Fee::find(1);
        $now = date('Y-m-d'); // or your date as well
        $your_date = date('Y-m-d', strtotime($fee->end_date));
        $date1 = strtotime($now);
        $date2 = strtotime($your_date);
        $days = $date1 - $date2;
        $event_type = $type == null ? request('virtual') : $type;
        $site_visit_price = 0;
        if ((int) $event_type <> 1) {
            //normal event
            $amount = $days < 0 ?
                    $fee->amount : $fee->amount + $fee->penalty_amount;
        } else {
            $amount = $days < 0 ?
                    $fee->virtual_amount : $fee->virtual_amount + $fee->penalty_amount;
            $site_visit_price = (int) request('option') > 0 || (int) $option == 1 ? $fee->virtual_material_amount : 0;
        }
        return $amount + $site_visit_price;
    }

    public function getNewFeeAmount($type = null, $option = null, $eventID = null) {
        $event_id = $eventID == null ? request('eventid') : $eventID;
        $fee = \App\Model\Fee::where('event_id', $event_id)->first();
        $now = date('Y-m-d'); // or your date as well
        $your_date = date('Y-m-d', strtotime($fee->end_date));
        $date1 = strtotime($now);
        $date2 = strtotime($your_date);
        $days = $date1 - $date2;
        $event_type = $type == null ? request('virtual') : $type;
        $site_visit_price = 0;
        if ((int) $event_type <> 1) {
            //normal event
            $amount = $days < 0 ?
                    $fee->amount : $fee->amount + $fee->penalty_amount;
        } else {
            $amount = $days < 0 ?
                    $fee->virtual_amount : $fee->virtual_amount + $fee->penalty_amount;
            $site_visit_price = (int) request('option') > 0 || (int) $option == 1 ? $fee->virtual_material_amount : 0;
        }
        return $amount + $site_visit_price;
    }

    public function getEventsFeeAmount($events) {

        $totalAmount = 0;
        if (empty($events)) {
            echo 'You must select at least one Event before you  proceed';
            exit;
        }
        foreach ($events as $event) {
            $fee = \App\Model\Fee::where('event_id', $event->eventId)->first();

            $now = date('Y-m-d'); // or your date as well
            $your_date = date('Y-m-d', strtotime($fee->end_date));
            $date1 = strtotime($now);
            $date2 = strtotime($your_date);
            $days = $date1 - $date2;
            $event_type = $event->eventState;
            $site_visit_price = 0;
            if ((int) $event_type <> 1) {
                //normal event
                $amount = $days < 0 ?
                        $fee->amount : $fee->amount + $fee->penalty_amount;
            } else {
                $amount = $days < 0 ?
                        $fee->virtual_amount : $fee->virtual_amount + $fee->penalty_amount;
                $site_visit_price = (int) $event->evnt_state_online > 0 || (int) $event->evnt_state_online == 1 ? $fee->virtual_material_amount : 0;
            }
            $eventAmount = $amount + $site_visit_price;

            $totalAmount += $eventAmount;
        }
        return $totalAmount;
    }

    public function userEventAllocate($user_id, $events,$user_type_id=null) {
        foreach ($events as $event) {
            $user_event = UserEvent::where('user_id', $user_id)->where('event_id', $event->eventId)->first();
            if (empty($user_event)) {
                UserEvent::create([
                    'user_id' => $user_id,
                    'event_id' => $event->eventId,
                ]);
            }
            
        }
        \App\Model\User::find($user_id)->update(['user_type_id'=>$user_type_id]);
    }

    public function landing() {

        if (Auth::check()) {
            return redirect('home');
        }
        $this->data['fees'] = \App\Model\Fee::whereIn('event_id', Event::where('status', 1)->get(['id']))->get();
        $this->data['events'] = Event::where('status', 1)->get();
        $this->data['landing'] = 1;

        if (empty($this->data['setting'])) {
            die('System is not well configured, kindly check with the system administrator to ensure proper registration first');
        }
        if ($this->data['setting']->site_mode == 3) {
            $page = 'certificate';
        } else if ($this->data['setting']->site_mode == 2 || $this->data['setting']->site_mode == 4) {
            $this->data['setting'] = $this->data['setting'];
            $page = 'inactive';
        } else {
            $page = 'landing';
        }
        return view('landing.' . $page, $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function validateBookig() {

        return $this->validate(request(), [
                    'name' => 'required|string',
                    'phone' => 'required|min:6|max:15',
                    'email' => 'required|email',
                    'profession_id' => 'required',
                    'employer_id' => 'required',
                    'events' => 'required'
        ]);
    }

    protected function getEventIds($events) {
        $event_ids = [];
        foreach ($events as $key => $value) {
            array_push($event_ids, $value->eventId);
        }
        return $event_ids;
    }

    public function updateInvoiceStatus($invoice, $events, $user) {
        //check if user is allocated in all invoice_fees

        foreach ($events as $event_request) {
            $event = Event::find($event_request->eventId);
            $invoice_fee = Invoice_fee::where('invoice_id', $invoice->id)
                    ->where('event_id', $event_request->eventId)
                    ->first();

            $amount = $this->getNewFeeAmount($event_request->eventState, $event_request->evnt_state_online, $event_request->eventId);

            $object = ['amount' => $amount,
                'item_name' => preg_replace('/[^A-Za-z0-9 ]/', '', $user->name),
                'note' => $event->name,
                'virtual' => $event_request->eventState,
                'virtual_material' => $event_request->evnt_state_online
            ];
            if (empty($invoice_fee)) {
                Invoice_fee::create(array_merge($object, [
                    'invoice_id' => $invoice->id,
                    'event_id' => $event_request->eventId,
                ]));
            } else {
                Invoice_fee::where('invoice_id', $invoice->id)
                        ->where('event_id', $event_request->eventId)->update($object);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     * //create booking here
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validateBookig();
        $phone = validate_phone_number(request('phone'));
        $events = json_decode($request->events);
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }

        $valid_phone = $phone[1];
        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => $request->email])->first();

        $user_type = \App\Model\User_type::where('name', 'non-sponsored')->first();

        if (empty($user_info)) {
            // $site_visit = isset($request->site_visit) && (int) $request->site_visit > 0 ? 1 : 0;
            $users = array_merge($request->except('_token', 'phone'), ['password' => 123456789, 'phone' => $valid_phone, 'user_type_id' => !empty($user_type) ? $user_type->id : NULL]);
            $user = User::create($users);
        } else {
            $user = $user_info;
//            $normal_invoice = $user->invoice()->orderBy('id', 'desc')->first();
//            $sponsored_invoice = $user->invoiceFee()->first();
//            if (!empty($normal_invoice)) {
//                $this->notifyUser($normal_invoice, $this->data['setting']);
//                return redirect(url('booking/' . $user->invoice()->orderBy('id', 'desc')->first()->id))->with('info', 'Invoice for this user has already being created');
//            } else if (!empty($sponsored_invoice)) {
//                return redirect()->back()->with('success', 'Invoice for this user has already being created');
//            }
        }
        $this->userEventAllocate($user->id, $events,9); //by default, if we have amount then its not a sponsored

        $amount = $this->getEventsFeeAmount($events);

        $invoice = Invoice::where('user_id', $user->id)->where('active', 1)->first();

        if (empty($invoice)) {
            //create an invoice first via booking method
            $booking = array_merge($request->all(), ['user_id' => $user->id,
                'amount' => $amount,
                'invoice_type' => 'normal',
                'username' => $this->data['setting']->username,
                'password' => $this->data['setting']->password
            ]);
            $this->createBooking($booking, false);
            $invoice = Invoice::where('user_id', $user->id)->where('active', 1)->first();
        }
        $this->updateInvoiceStatus($invoice, $events, $user);

        if ((int) $amount == 0) {
            // create a zoom meeting and redirect users if event can be online
            $eventID = $events[0]->eventId;
            $event_info = Event::findOrFail($eventID);
            if (strtolower($event_info->virtual) == 'on') {

                $meeting = ZoomMeeting::where(['event_id' => $eventID])->first();
                $meetingID = $meeting->id;

                UserMeeting::create([
                    'zoom_meeting_id' => $meetingID,
                    'user_id' => $user->id,
                    'date' => NOW()
                ]);
            }
            return redirect()->back()->with('success', 'Congrats, you are registered successfully ');
        }

        //$return = (new \App\Http\Controllers\InvoiceController())->storeInvoice($this->curlServer($booking, $setting->create_invoice_url), $user->id, $fee->id);
        if (!empty($invoice)) {
            //invoice created successfully
            $this->saveUserMeetings($invoice->id);
            //return redirect()->back()->with('success', 'Congrats, Invoice created succesful, Payment details sent to your email '.$user->emai);
            return redirect(url('invoice/' . $invoice->id));
        } else {
            return redirect()->back()->with('error', 'Invoice Failed to be Created. Please try again later');
        }
    }

    public function saveUserMeetings($invoice_id) {
        $eventFees = Invoice_fee::where(['invoice_id' => $invoice_id, 'virtual' => 1])->get();
        if (!empty($eventFees)) {
            $users = InvoiceUser::where('invoice_id', $invoice_id)->get();
            foreach ($eventFees as $eventFee) {
                $eventID = $eventFee->event_id;
                $meeting = ZoomMeeting::where(['event_id' => $eventID])->first();
                $meetingID = $meeting->id;
                foreach ($users as $user) {
                    $userProfile = User::find($user->user_id);
                    //$addUser = (new \App\Http\Controllers\MeetingController())->add_participant($userProfile,$meeting->meeting_id);
                    UserMeeting::create([
                        'zoom_meeting_id' => $meetingID,
                        'user_id' => $user->user_id,
                        'date' => NOW()
                    ]);
                }
            }
        }
    }

    public function saveUserMeetingsForSponsored($invoice_id) {
        $eventFees = Invoice_fee::where(['invoice_id' => $invoice_id, 'virtual' => 1])->get();
        if (!empty($eventFees)) {
            $users = InvoiceUser::where('invoice_id', $invoice_id)->get();
            foreach ($eventFees as $eventFee) {
                $eventID = $eventFee->event_id;
                $meeting = ZoomMeeting::where(['event_id' => $eventID])->first();
                $meetingID = $meeting->id;
                foreach ($users as $user) {
                    $userProfile = User::find($user->user_id);
                    $this->sendSmstoSponsored($userProfile);
                    $addUser = (new \App\Http\Controllers\MeetingController())->add_participant($userProfile, $meeting->meeting_id);

                    UserMeeting::create([
                        'zoom_meeting_id' => $meetingID,
                        'user_id' => $user->user_id,
                        'date' => NOW()
                    ]);
                }
            }
        }
    }

    public function sendSmstoSponsored($user) {
        $patterns = array(
            '/#name/i', '/#invoice/i', '/#email/i', '/#phone/i'
        );
        $replacements = array(
            $user->name, '', $user->email, $user->phone
        );
        $template = \App\Model\Sms_template::where('name', 'sponsored')->first();
        $sms = preg_replace($patterns, $replacements, $template->message);
        DB::table('sms')->insert(array('phone' => $user->phone, 'body' => $sms, 'user_id' => $user->id));
    }

    public function createBooking($book, $is_bulk = false) {
        $booking = (object) $book;
        $user = \App\Model\User::find($booking->user_id);

        $invoice_number = Invoice::orderBy('id', 'desc')->first();
        $number = 1;
        if (!empty($invoice_number)) {

            $number = (int) $invoice_number->id + 1;
        }

        if ($is_bulk == TRUE) {
            $ega_invoice = $this->createInvoiceNo($user, $booking->amount);
            if (isset($ega_invoice->billId)) {
                $invoice_param = [
                    'number' => $number,
                    'user_id' => $booking->user_id,
                    'invoice_type' => $booking->invoice_type,
                    'bil_id' => $ega_invoice->billId,
                    'title' => request('title'),
                    'optional_name' => request('optional_name'),
                    'date' => 'now()',
                    'active' => 1,
                    'type' => 1,
                    'year' => date('Y')];

                $invoice = Invoice::create($invoice_param);

                // $this->handle($invoice->id);
                $this->notifyUser($invoice, $this->data['setting']);

                foreach (json_decode($booking->events) as $event) {
                    $ev = Event::findOrFail($event->eventId);
                    $amount = $this->getNewFeeAmount($event->eventState, $event->evnt_state_online, $event->eventId);
                    $invoiceFee = Invoice_fee::create(['invoice_id' => $invoice->id, 'event_id' => $event->eventId, 'amount' => $amount, 'item_name' => $user->name, 'note' => $ev->name, 'virtual' => $event->eventState, 'virtual_material' => $event->evnt_state_online]);
                }

                return (object) ['invoice_id' => $invoice->id, 'amount' => $booking->amount];
            } else {
                die('GEPG error: system failed to return a valid control number');
            }
        } else {
            $ega_invoice = $this->createInvoiceNo($user, $booking->amount);

            if (isset($ega_invoice->billId)) {
                $invoice_param = [
                    'number' => $number,
                    'user_id' => $booking->user_id,
                    'invoice_type' => $booking->invoice_type,
                    'bil_id' => $ega_invoice->billId,
                    'title' => request('title'),
                    'optional_name' => request('optional_name'),
                    'date' => 'now()',
                    'active' => 1,
                    'type' => $is_bulk == FALSE ? 0 : 1,
                    'year' => date('Y')];

                $invoice = Invoice::create($invoice_param);
                $invoiceID = $invoice->id;
                $userID = $invoice->user_id;
                $invoiceUser = InvoiceUser::create(['invoice_id' => $invoiceID, 'user_id' => $userID]);

                // $this->handle($invoice->id);
                $this->notifyUser($invoice, $this->data['setting']);

                foreach (json_decode($booking->events) as $event) {
                    $ev = Event::findOrFail($event->eventId);
                    $amount = $this->getNewFeeAmount($event->eventState, $event->evnt_state_online, $event->eventId);
                    $invoiceFee = Invoice_fee::create(['invoice_id' => $invoice->id, 'event_id' => $event->eventId, 'amount' => $amount, 'item_name' => $booking->name, 'note' => $ev->name, 'virtual' => $event->eventState, 'virtual_material' => $event->evnt_state_online]);
                }
                return $invoiceFee;
            } else {
                die('GEPG error: system failed to return a valid control number');
            }
        }
    }

    public function createBookingForSponsored($book, $is_bulk = false) {
        $booking = (object) $book;
        $user = \App\Model\User::find($booking->user_id);
        $user->update(['user_type_id' => 7]);
        $invoice_number = Invoice::orderBy('id', 'desc')->first();
        $number = 1;
        if (!empty($invoice_number)) {

            $number = (int) $invoice_number->id + 1;
        }

        $invoice_param = [
            'number' => $number,
            'user_id' => $booking->user_id,
            'invoice_type' => $booking->invoice_type,
            'bil_id' => time(),
            'title' => request('title'),
            'optional_name' => request('optional_name'),
            'date' => 'now()',
            'type' => 1,
            'year' => date('Y')];

        $invoice = Invoice::create($invoice_param);

        // $this->handle($invoice->id);
        $this->notifyUser($invoice, $this->data['setting']); ///////

        foreach (json_decode($booking->events) as $event) {
            $ev = Event::findOrFail($event->eventId);
            $amount = $this->getNewFeeAmount($event->eventState, $event->evnt_state_online, $event->eventId);
            $invoiceFee = Invoice_fee::create(['invoice_id' => $invoice->id, 'event_id' => $event->eventId, 'amount' => $amount, 'item_name' => $booking->name, 'note' => $ev->name, 'virtual' => $event->eventState, 'virtual_material' => $event->evnt_state_online]);
        }

        return (object) ['invoice_id' => $invoice->id, 'amount' => $booking->amount];
    }

    public function createInvoiceNo($user, $amount) {
        $data = Invoice::where('user_id', $user->id)->first();
        if (empty($data)) {
            return $this->createControlNumber($user, $amount);
        } else {
            return false;
        }
    }

    public function createControlNumber($user, $amount) {
        $api = new \App\Http\Controllers\ApiController();
//        return (object) [
//                    "billId" => 36,
//                    "controlNumber" => null,
//                    "fullName" => "Othman Omary",
//                    "email" => "othman.omary@ega.go.tz",
//                    "phone" => "255710826932",
//                    "createDate" => "2019-07-18T14:49:17.261",
//                    "dueDate" => "2019-06-18T16:23:15.052",
//                    "checkSum" => "396884f5b5c67df630a307fdd66e2f800e7e667d"
//        ];
        //lets check if this no control no exists

        return $api->createControlNumber($user, $amount);
    }

    function notifyUser($normal_invoice, $setting) {
        $message = 'Your invoice has been created. Use the following information to make payments'
                . '<li>Payment Methods:'
                . '     <p>   <br/><b>Via Mobile Network Operators (MNO)</b><br/>
                         Enter to the respective USSD Menu of MNO,Select 4 (Make Payments) for Tigopesa and Mpesa and Select 5 for AirtelMoney,Select 5 (Government Payments),Enter control number,Enter password</p>
                                                               
                                                                
<p>   <br/><b>Kupitia Mitandao ya Simu</b><br/>
                         Enter to the respective USSD Menu of MNO,Select 4 (Make Payments) for Tigopesa and Mpesa and Select 5 for AirtelMoney,Select 5 (Government Payments),Enter control number,Enter password</p>
                                                                <p>
                                                                
<b>Kupitia Bank</b>
                                                            <br/>
  Fika tawi lolote au wakala wa benki ya NMB, CDRB, NBC ukiwa na namba yako ya  kumbukumbu kutoka Kwenye mfumo <br/>

</p> 

                                                         <p>
                                                                
<b>FOR BANKS</b>
                                                            <br/>
       Visit any branch or bank agent of NMB, CRDB, NBC with your control number obtained from the system
<br/>
<b>(You are advised to print this invoice and submit it to the bank along with the appreciate amount)</b>


</p>

</li></ul>'
                . '<p>All payments without this reference number will not be processed</p><br/>Thank You';
        $this->send_email($normal_invoice->user->email, 'ERB Invoice Number Reminder', $message);
        //dispatch(new \App\Jobs\SendEmailJob("Booking Details", $normal_invoice->user->email, $message));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //$invoice = Invoice::find($id);
        //return $invoice->invoiceFee;
        $this->data['event'] = Event::first();
        $this->data['invoice'] = Invoice::find($id);
        $this->data['fee'] = \App\Model\Fee::first();
        return view('invoice.show', $this->data);
    }

    public function downloadPdf($id) {
        $this->data['event'] = Event::first();
        $this->data['invoice'] = Invoice::find($id);
        $this->data['fee'] = \App\Model\Fee::first();
        //return view('invoice.download', $this->data);
        $pdf = PDF::loadView('invoice.download', $this->data);
        return $pdf->download('invoice.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function search() {
        $var = validate_phone_number(request('tag'));
        $phone = is_array($var) ? $var[1] : request('tag'); //force phone to be invalid to return a searched tag
        //$user_base_records = ['email' => trim(strtolower(request('tag'))), 'phone' => $phone == null ? 0 : $phone];
        $user_info = User::orWhere('email', 'ilike', trim(strtolower(request('tag'))))->orWhere('phone', 'ilike', '%' . $phone . '%')->first();

        if (empty($user_info)) {
            echo json_encode(['message' => 'Sorry: Information does not exists', 'alert_status' => 'alert-danger']);
        } else {
            $no_such_invoice = json_encode([
                'message' => 'Sorry: No invoice has been created under such email or phone',
                'alert_status' => 'alert-danger']);

            if ($user_info->role_id == NULL) {
                if ((int) $user_info->is_employer == 1) {
                    $user_bulk_invoice = Invoice::where('user_id', $user_info->id)->where('active', 1)->first();
                    if (!empty($user_bulk_invoice)) {
                        $setting = Setting::first();
                        $this->notifyUser($user_bulk_invoice, $setting);
                        echo json_encode(['message' => 'Success: Your Invoice/Reference number is ' . $user_bulk_invoice->number . '. <a href="' . url('invoice/' . $user_bulk_invoice->id) . '" id="link" class="badge badge-success">Click here to view your invoice</a> ', 'alert_status' => 'alert-success']);
                    } else {

                        echo $no_such_invoice;
                    }
                } else {
                    $user_invoice = Invoice::where('user_id', $user_info->id)->where('active', 1)->first();
                    $invoice_in_bulk = Invoice_fee::whereIn('invoice_id', Invoice::where('user_id', $user_info->id)->get(['id']))->first();
                    if (!empty($user_invoice)) {
                        echo json_encode(['message' => 'Success: Your Invoice/Reference number is ' . $user_invoice->number . '. <a href="' . url('invoice/' . $user_invoice->id) . '" id="link" class="badge badge-success">Click here to view your invoice</a> ', 'alert_status' => 'alert-success']);
                    } else if (!empty($invoice_in_bulk)) {
                        echo json_encode(['message' => 'Success: Your invoice has been created under sponsorship of ' . $invoice_in_bulk->invoice->user->name, 'alert_status' => 'alert-success']);
                    } else {
                        $amount = $this->getFeeAmount($user_info->virtual, $user_info->virtual_material);
                        $booking = ['user_id' => $user_info->id,
                            'name' => $user_info->name,
                            'amount' => $amount,
                            'username' => $this->data['setting']->username,
                            'password' => $this->data['setting']->password
                        ];
                        $fee = \App\Model\Fee::find(1);
                        $this->createBooking($booking, $fee->id);
                        return $this->search();
                    }
                }
            } else {
                echo $no_such_invoice;
            }
        }
    }

    public function createNewInvoice($user_id = null) {
        $user_info = \App\Model\User::find($user_id);
        $amount = $this->getFeeAmount($user_info->virtual, $user_info->virtual_material);
        $booking = ['user_id' => $user_info->id,
            'name' => $user_info->name,
            'amount' => $amount,
            'username' => $this->data['setting']->username,
            'password' => $this->data['setting']->password
        ];
        $fee = \App\Model\Fee::find(1);
        $this->createBooking($booking, $fee->id);
        return redirect()->back()->with('success', 'success');
    }

    public function uploadTinyMceFile() {
        print_r(request()->all());
    }

    /**
     * check if email exists or not and provide response on booking
     */
    public function checkEmail() {
        $email = strtolower(trim(request('email')));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            die('<div class="alert alert-danger">Error: Email ' . $email . ' is not valid</div>');
        }
        $user_info = User::where('email', $email)->first();
        if (!empty($user_info)) {
            die('<div class="alert alert-danger">This Email ' . $email . ' already exists by a name <b>' . $user_info->name . '</b>. If you submit, a control number will be created for <b>' . $user_info->name . '</b>. Kindly choose another email if you are not <b>' . $user_info->name . '</b> </div>');
        } else {
            return "";
        }
    }

    public function checkPhone() {
        $phone = validate_phone_number(request('phone'));

        if (!is_array($phone)) {
            die('<div class="alert alert-danger">error: Phone number ' . request('phone') . ' is not valid</div>');
        }

        $valid_phone = $phone[1];
        $user_info = User::where('phone', $valid_phone)->first();
        if (!empty($user_info)) {
            die('<div class="alert alert-danger">This phone ' . $valid_phone . ' already exists by a name <b>' . $user_info->name . '</b>. If you submit, a control number will be created for <b>' . $user_info->name . '</b>. Kindly choose another phone number if you are not <b>' . $user_info->name . '</b> </div>');
        } else {
            return "";
        }
    }

}
