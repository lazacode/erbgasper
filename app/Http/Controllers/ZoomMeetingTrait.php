<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * trait ZoomMeetingTrait
 */
trait ZoomMeetingTrait {

    public $client;
    public $jwt;
    public $headers;

    public function __construct() {
        $this->client = new Client();
        $this->jwt = $this->generateZoomToken();
        $this->headers = [
            'Authorization' => 'Bearer ' . $this->jwt,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    public function generateZoomToken() {
        $key = 'm3obC_JzSIeEpLcPduwOOA';
        $secret = 'cQ53tHMUvNsWimeVvhdwrub23Z9MJvuOpq5M';
        $payload = [
            'iss' => $key,
            'exp' => strtotime('+1 minute'),
        ];

        return \Firebase\JWT\JWT::encode($payload, $secret, 'HS256');
    }

    private function retrieveZoomUrl() {
        return 'https://api.zoom.us/v2/';
    }

    public function toZoomTimeFormat($dateTime) {
        try {
            $date = new \DateTime($dateTime);

            return $date->format('Y-m-d\TH:i:s');
        } catch (\Exception $e) {
            Log::error('ZoomJWT->toZoomTimeFormat : ' . $e->getMessage());

            return '';
        }
    }

    public function create($data) {
        $path = 'users/me/meetings';
        $url = $this->retrieveZoomUrl();

        $body = [
            'headers' => $this->headers,
            'body' => json_encode([
                'topic' => $data['topic'],
                'type' => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration' => $data['duration'],
                'agenda' => (!empty($data['agenda'])) ? $data['agenda'] : null,
                'timezone' => 'Africa/Nairobi',
                'settings' => [
                    'host_video' => ($data['host_video'] == "1") ? true : false,
                    'participant_video' => ($data['participant_video'] == "1") ? true : false,
                    'waiting_room' => true,
                    'registration_type' => 1,
                    'approval_type' => 0,
                ],
            ]),
        ];

        try {
            $response = $this->client->post($url . $path, $body);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return [
            'success' => $response->getStatusCode() === 201,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function add_participant($user, $meetingID) {
        //return $meetingID;
        $path = 'meetings/' . $meetingID . '/registrants';
        $url = $this->retrieveZoomUrl();

        $arr = explode(' ', $user->name);
        $num = count($arr);
        $first_name = $middle_name = $last_name = null;

        if ($num > 1) {
            $first_name = $arr[0];
            $last_name = $arr[1];
        } else {
            $first_name = $arr[0];
            $last_name = $arr[0];
        }

        $data = [
            "email" => $user->email,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "phone" => $user->phone,
        ];
        $body = [
            'headers' => $this->headers,
            'body' => json_encode($data),
        ];

        $response = $this->client->post($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 201,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function update($id, $data) {
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();

        $body = [
            'headers' => $this->headers,
            'body' => json_encode([
                'topic' => $data['topic'],
                'type' => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration' => $data['duration'],
                'agenda' => (!empty($data['agenda'])) ? $data['agenda'] : null,
                'timezone' => 'Africa/Nairobi',
                'settings' => [
                    'host_video' => ($data['host_video'] == "1") ? true : false,
                    'participant_video' => ($data['participant_video'] == "1") ? true : false,
                    'waiting_room' => true,
                ],
            ]),
        ];
        $response = $this->client->patch($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function get($id) {
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        $this->jwt = $this->generateZoomToken();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        $response = $this->client->get($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function get_participants($id) {
        ///meetings/{meetingId}/registrants
        $path = 'meetings/' . $id . '/registrants';
        $url = $this->retrieveZoomUrl();
        $this->jwt = $this->generateZoomToken();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        $response = $this->client->get($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function get_report($id) {
        $path = '/report/meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        $this->jwt = $this->generateZoomToken();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        $response = $this->client->get($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
            'data' => json_decode($response->getBody(), true),
        ];
    }

    public function get_participants_report($id) {
        $path = 'report/meetings/' . $id . '/participants';
        $url = $this->retrieveZoomUrl();
        $this->jwt = $this->generateZoomToken();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        try {
            // Validate the value...
            $response = $this->client->get($url . $path, $body);

            return [
                'success' => $response->getStatusCode() === 204,
                'data' => json_decode($response->getBody(), true),
            ];
        } catch (Throwable $e) {
            return [
                'success' => 400,
                'data' => [],
            ];
        }
    }

    /**
     * @param string $id
     *
     * @return bool[]
     */
    public function delete($id) {
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        $response = $this->client->delete($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
        ];
    }

}
