<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BookingController;
use App\Model\Employer;
use \App\Model\Invoice;
use App\Model\Invoice_fees_payment;
use App\Model\InvoiceUser;
use App\Model\UserEvent;
use \App\Model\User;
use \App\Model\Fee;
use \App\Model\Setting;
use \App\Model\Profession;
use \App\Model\Invoice_fee;
use \App\Model\Event;
use Illuminate\Http\Request;
use DB;
use Auth;

class InvoiceController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth')->except(['store', 'storeUserRecords', 'createInvoceByExcel']);
        $this->data['setting'] = \App\Model\Setting::first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $eventID = request('event');
        $totalAmount = 0;
        $paidAmount = 0;
        $unPaidAmount = 0;

        $dataa = [];
        $userIDs = User::where('is_employer', 0)
                ->whereIn('id', UserEvent::whereIn('event_id', Event::where('status', 1)->get(['id']))->get(['user_id']))
                ->get(['id']);

        if (!empty($eventID)) {
            $invoices = Invoice::where('invoice_type', 'normal')
                            ->whereIn('user_id', $userIDs)->get();
            foreach ($invoices as $invoice) {
                $users = InvoiceUser::where('invoice_id', $invoice->id)->count();
                if ($users < 2) {
                    array_push($dataa, $invoice->id);
                }
            }
            $invoiceFees = Invoice_fee::where('event_id', $eventID)->whereIn('invoice_id', $dataa)->get();
            $totalInvoices = $invoiceFees->count();
            foreach ($invoiceFees as $fee) {
                $amount = $fee->amount * $fee->users->count();
                $totalAmount += $amount;
                $paid = $fee->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            //'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount'
            $this->data['invoices'] = DB::table('invoices')
                            ->select(DB::raw('count(invoice_users) as totalusers'), 'invoices.id', 'invoices.number', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->join('invoice_fees', 'invoice_fees.invoice_id', '=', 'invoices.id')
                            ->join('invoice_users', 'invoice_users.invoice_id', '=', 'invoices.id')
                            ->where('invoice_fees.event_id', $eventID)
                            ->where('invoices.invoice_type', 'normal')
                            ->groupby('invoices.id', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->whereIn('invoices.user_id', $userIDs)->get();

//            return $this->data['invoices'] = DB::table('invoices')
//                ->select(DB::raw('count(invoice_users) as totalusers'),'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount','users.name')
//                ->join('invoice_fees','invoice_fees.invoice_id','=','invoices.id')
//                ->join('invoice_users','invoice_users.invoice_id','=','invoices.id')
//                ->join('users','users.id','=','invoices.user_id')
//                ->where('invoice_fees.event_id',$eventID)
//                ->groupby('invoices.id','invoice_fees.event_id','invoice_fees.amount')
//                ->whereIn('invoices.user_id', $userIDs)->get();


            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;

            return view('invoice.bulkwithevent', $this->data);
        } else {

            //$invoices = Invoice::whereIn('user_id', $userIDs)->get();
            //$invoices = Invoice::where('invoice_type','normal')->whereIn('user_id', $userIDs)->get('id');
            $invoices = Invoice::where('invoice_type', 'normal')
                            ->whereIn('user_id', $userIDs)->get();

            foreach ($invoices as $invoice) {
                $users = InvoiceUser::where('invoice_id', $invoice->id)->count();
                if ($users < 2) {
                    array_push($dataa, $invoice);
                }
            }
            $totalInvoices = count((array) $invoices);
            foreach ($dataa as $invoice) {
                $amount = $invoice->getAmount();
                $totalAmount += $amount;
                $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            $this->data['invoices'] = Invoice::whereIn('user_id', $userIDs)->whereIn('user_id', UserEvent::whereIn('event_id', Event::where('status', 1)->get(['id']))->get(['user_id']))->paginate(15);

            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;
            //return $this->data;
            $export = request('export');
            if ((int) $export == 1) {
                $this->data['file_name_export'] = time();
                $this->data['invoices'] = Invoice::whereIn('user_id', $userIDs)->get();
                echo view('invoice.export.user_invoice_export', $this->data);
                return response()->download('storage/app/' . $this->data['file_name_export'] . '.xls');
            }
            return view('invoice.index', $this->data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //Todo:Update code
        if (request()->bulk) {
            $fees = Fee::all('event_id');
            $this->data['events'] = Event::where('status', 1)->whereIn('id', $fees)->get();
            return view('invoice.bulkbooking', $this->data);
        } elseif (request()->sponsored) {
            $this->data['amount'] = (new BookingController())->getFeeAmount();
            $fees = Fee::all('event_id');
            $this->data['events'] = Event::where('status', 1)->whereIn('id', $fees)->get();
            return view('invoice.sponsoredbooking', $this->data);
        } else {
            $fees = Fee::all('event_id');
            $this->data['events'] = Event::where('status', 1)->whereIn('id', $fees)->get();
            return view('invoice.booking', $this->data);
        }

        //$this->data['amount'] = (new BookingController())->getFeeAmount();
    }

    public function storeInvoice($request, $user_id, $fee_id, $is_bulk = false) {
        $payment_invoice = json_decode($request);
        if (count($payment_invoice) == 1 && isset($payment_invoice->number)) {
            $invoice_param = [
                'number' => $payment_invoice->number,
                'user_id' => $user_id,
                'title' => request('title'),
                'optional_name' => request('optional_name'),
                'date' => 'now()',
                'active' => 1,
                'bil_id' => request('bilId'),
                'type' => $is_bulk == FALSE ? 0 : 1,
                'year' => date('Y')];
            $invoice = Invoice::create($invoice_param);
            return $is_bulk == false ?
                    Invoice_fee::create(['invoice_id' => $invoice->id, 'fee_id' => $fee_id, 'amount' => $payment_invoice->amount, 'item_name' => request('name'), 'note' => 'Engineers  Registration Board (ERB), Engineers Day Event']) :
                    (object) ['invoice_id' => $invoice->id, 'amount' => $payment_invoice->amount];
        } else {
            return NULL;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if ($request->file('file')) {
            $validator = $this->validate($request, [
                'invoice_type' => 'required',
                'file' => 'required|max:50000|mimes:xlsx,csv'
            ]);
            if ($request->invoice_type == "bulk") {

                $this->validate($request, [
                    'email' => 'required|email',
                    'phone' => 'required|numeric'
                ]);
                return $this->createBulkInvoice($request);
            } elseif ($request->invoice_type == "sponsored") {

                $this->validate($request, [
                    'user_id' => 'required',
                ]);
                return $this->createSponsoredInvoice($request);
            }
        } else if ((int) request('noexcel') == 1) {

            return $this->createInvoceByExcel($request, TRUE);
        } else {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|numeric|unique:users,phone',
                'employer_id' => 'required',
                'profession_id' => 'required'
            ]);
            return $this->createSingleUserInvoice($request);
        }
    }

    public function createSingleUserInvoice($request) {

        $phone = validate_phone_number(request('phone'));
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }
        $valid_phone = $phone[1];

        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => $request->email])->first();
        if (empty($user_info)) {
            $user_type = \App\Model\User_type::where('name', 'non-sponsored')->first();
            $user = User::create(array_merge($request->except('_token', 'phone'), ['password' => 123456789, 'phone' => $valid_phone, 'created_by' => Auth::user()->id, 'user_type_id' => !empty($user_type) ? $user_type->id : NULL]));
            $user_info = User::find($user->id);
        } else {
            return redirect()->back()->with('error', 'User with those information already exists with Invoice Number ');
        }
        $setting = Setting::first();
        $amount = (new BookingController())->getFeeAmount($user_info->virtual, $user_info->virtual_material);
        $booking = array_merge($request->all(), ['user_id' => $user_info->id,
            'amount' => $amount,
            'username' => $setting->username,
            'password' => $setting->password
        ]);
        $booking_control = new BookingController();
        $fee = \App\Model\Fee::find(1);
        $booking_control->createBooking($booking, $fee->id, false);
        return redirect(url('invoice'));
    }

    private function checkKeysExists($value) {
        $required = array('phone', 'name', 'email', 'profession');
        $data = array_shift($value);
        if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
            //All required keys exist! 
            $status = 1;
        } else {
            $missing = array_intersect_key(array_flip($required), $data);
            $data_miss = array_diff(array_flip($required), $missing);
            $status = ' <div class="alert alert-danger">Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file</div>';
        }
        return $status;
    }

    public function createBulkInvoice($request) {
        //////////
        ini_set('max_execution_time', 300);
        $employer = Employer::find($request->user_id);

        if (isset($request->email)) {

            $user_base_records = ['email' => strtolower($request->email), 'phone' => $request->phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['name' => $employer->name, 'password' => bcrypt('123456789'), 'phone' => $request->phone, 'is_employer' => 1, 'profession_id' => $request->profession, 'employer_id' => $request->user_id, 'user_type_id' => 120]));
            } else {
                $user_info->update(['password' => bcrypt('123456789'), 'employer_id' => $request->user_id, 'is_employer' => 1, 'user_type_id' => 120]);
                $user = $user_info;
            }
        } else {
            $dumb_phone = time();
            $user_base_records = ['email' => strtolower($employer->id) . '@erb.go.tz', 'phone' => $dumb_phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['name' => $employer->name, 'password' => bcrypt('123456789'), 'phone' => $dumb_phone, 'is_employer' => 1, 'employer_id' => $request->user_id, 'user_type_id' => 120, 'profession_id' => $request->profession]));
            } else {
                $user_info->update(['password' => bcrypt('123456789'), 'is_employer' => 1, 'employer_id' => $request->user_id]);
                $user = $user_info;
            }
        }

        $amount = (new BookingController())->getEventsFeeAmount(json_decode($request->events));
        $users = $this->uploadExcel();
     
        //We need to minus 1 because the excel contains HEADERS which actuall add those no
        $newAmount = $amount * (count((array) $users));
        $setting = Setting::first();
        $booking = array_merge($user_base_records, ['user_id' => $user->id,
            'amount' => $newAmount, 'phone' => time(),
            'invoice_type' => $request->invoice_type,
            'username' => $setting->username,
            'password' => $setting->password,
            'events' => $request->events
        ]);

        $invoice = Invoice::where('user_id', $user->id)->where('active', 1)->first();

        if (!empty($invoice)) {
            return redirect('invoice/' . $invoice->id)->with('info', 'Invoice for this user has already being created');
        }

        $booking_control = new BookingController();
        $return = $booking_control->createBooking($booking, 1);

        return $this->storeUserRecords($return, $users, $employer, false, json_decode($request->events));
    }

    public function createSponsoredInvoice($request) {
        ini_set('max_execution_time', 300);
        $employer = Employer::find($request->user_id);
        //check if we upload user with more details, then check

        $profession_id = 1;
        $profession = \collect(DB::select("select * from professions where lower(name) like '%" . $request->profession . "%' limit 1 "))->first();
        if (!empty($profession)) {
            $profession_id = $profession->id;
        }

        if (isset($request->email)) {

            $user_base_records = ['name' => $employer->name, 'email' => strtolower($request->email), 'phone' => $request->phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['password' => bcrypt('123456789'), 'phone' => $request->phone, 'is_employer' => 1, 'profession_id' => $profession_id, 'employer_id' => $request->user_id, 'user_type_id' => 120]));
            } else {
                User::where($user_base_records)->update(['password' => bcrypt('123456789'), 'employer_id' => $request->user_id, 'user_type_id' => 120]);
                $user = $user_info;
            }
        } else {
            // this is employer
            $dumb_phone = time();
            $user_base_records = ['name' => $employer->name, 'email' => strtolower($employer->id) . '@erb.go.tz', 'phone' => $dumb_phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['password' => bcrypt('123456789'), 'phone' => $dumb_phone, 'is_employer' => 1, 'employer_id' => $request->user_id, 'user_type_id' => 120, 'profession_id' => $profession_id]));
            } else {
                User::orWhere($user_base_records)->update(['password' => bcrypt('123456789'), 'is_employer' => 1, 'employer_id' => $request->user_id]);
                $user = $user_info;
            }
        }


        $amount = (new BookingController())->getEventsFeeAmount(json_decode($request->events));

        if ((float) $amount == 0) {
            return redirect()->back()->with('error', 'Invoice amount cannot be 0, kindly set FEE amount first before you upload participants');
        }
        $users = $this->uploadExcel();

        //We need to minus 1 because the excel contains HEADERS which actuall add those no 
        $newAmount = $amount * (count((array) $users));
        $setting = Setting::first();
        $booking = array_merge($user_base_records, ['user_id' => $user->id,
            'amount' => $newAmount,
            'phone' => time(),
            'invoice_type' => $request->invoice_type,
            'username' => $setting->username,
            'password' => $setting->password,
            'events' => $request->events
        ]);

        $invoice = Invoice::where('user_id', $user->id)->where('active', 1)->first();

        if (!empty($invoice)) {
            return redirect('invoice/' . $invoice->id)->with('info', 'Invoice for this user has already being created');
        }

        $booking_control = new BookingController();
        $return = $booking_control->createBookingForSponsored($booking, 1);
        $employer = Employer::find($request->user_id);

        return $this->storeUserRecordsForSponsored($return, $users, $employer, false, json_decode($request->events));
    }

    public function createInvoceByExcel($request, $noexcel = false) {
        ini_set('max_execution_time', 300);

        $employer = \App\Model\Employer::find($request->user_id);
        //check if we upload user with more details, then check

        $profession_id = 1;
        $profession = \collect(DB::select("select * from professions where lower(name) like '%" . $request->profession . "%' limit 1 "))->first();
        if (!empty($profession)) {
            $profession_id = $profession->id;
        }
        if (isset($request->email)) {

            $user_base_records = ['name' => $employer->name, 'email' => strtolower($request->email), 'phone' => $request->phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['password' => bcrypt('123456789'), 'phone' => $request->phone, 'is_employer' => 1, 'profession_id' => $profession_id, 'employer_id' => $request->user_id, 'user_type_id' => 120]));
            } else {
                User::where($user_base_records)->update(['password' => bcrypt('123456789'), 'profession_id' => $profession_id, 'employer_id' => $request->user_id, 'user_type_id' => 120]);
                $user = $user_info;
            }
        } else {

            // this is employer
            $dumb_phone = time();
            $user_base_records = ['name' => $employer->name, 'email' => strtolower($employer->id) . '@erb.go.tz', 'phone' => $dumb_phone];
            $user_info = User::orWhere($user_base_records)->first();

            if (empty($user_info)) {
                $user = User::create(array_merge($user_base_records, ['password' => bcrypt('123456789'), 'phone' => $dumb_phone, 'is_employer' => 1, 'employer_id' => $request->user_id, 'user_type_id' => 120, 'profession_id' => $profession_id]));
            } else {
                User::orWhere($user_base_records)->update(['password' => bcrypt('123456789'), 'is_employer' => 1, 'employer_id' => $request->user_id]);
                $user = $user_info;
            }
        }


        $amount = (new BookingController())->getEventsFeeAmount(json_decode($request->events));

        $users = $this->uploadExcel();

        //We need to minus 1 because the excel contains HEADERS which actuall add those no
        $newAmount = $amount * (count((array) $users));
        $setting = Setting::first();
        $booking = array_merge($user_base_records, ['user_id' => $user->id,
            'amount' => $newAmount, 'phone' => time(),
            'invoice_type' => $request->invoice_type,
            'username' => $setting->username,
            'password' => $setting->password,
            'events' => $request->events
        ]);
        $invoice = Invoice::where('user_id', $user->id)->where('active', 1)->first();

        if (!empty($invoice)) {
            return redirect('invoice/' . $invoice->id)->with('info', 'Invoice for this user has already being created');
        }

        $booking_control = new BookingController();
        $return = $booking_control->createBooking($booking, 1);

        return $this->storeUserRecords($return, $users, $employer, $noexcel, json_decode($request->events));
    }

    public function storeUserRecords($return, $users, $employer, $noexcel = false, $events = false) {
        $data = $users;
        $status = $noexcel == false ? $this->checkKeysExists($data) : 1;

        if ((int) $status == 1) {
            $status = '';

            foreach ($data as $value) {

                $valid_phone = validate_phone_number($value['phone']);
                if (!filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {

                    $status .= ' <div class="alert alert-danger">This email <b>' . $value['email'] . '</b>  is not valid. Record skipped </div>';
                }
                if (count($valid_phone) <> 2) {
                    $status .= ' <div class="alert alert-danger">This phone number <b>' . $value['phone'] . '</b>  is not valid. Record skipped </div>';
                }
                $prof = $value['profession'];
                $profession = Profession::where('name', $prof)->orWhere(DB::raw('lower(name)'), strtolower($prof))->first();
                if (!empty($profession)) {
                    $profession_id = $profession->id;
                } else {
                    $p = Profession::create(['name' => $prof]);
                    $profession_id = $p->id;
                }
                $user_data = [
                    'phone' => $valid_phone[1],
                    'email' => $value['email']
                ];

                $user_info = User::orWhere($user_data)->first();

                $user = (empty($user_info)) ?
                        User::create(array_merge($user_data, ['password' => bcrypt('user12345'),
                            'name' => $value['name'], 'profession_id' => $profession_id, 'employer_id' => $employer->id, 'is_employer' => 0, 'user_type_id' => 9])) :
                        $user_info;
                $invoiceUser = InvoiceUser::create(['invoice_id' => $return->invoice_id, 'user_id' => $user->id]);
                foreach ($events as $event) {
                    UserEvent::firstOrCreate([
                        'user_id' => $user->id,
                        'event_id' => $event->eventId,
                    ]);
                }
            }
        }

        $addToMeeting = (new BookingController())->saveUserMeetings($return->invoice_id);
        $this->data['status'] = $status;
        return redirect(url('invoice/' . $return->invoice_id))->with('info', $this->data['status']);
    }

    public function storeUserRecordsForSponsored($return, $users, $employer, $noexcel = false, $events = false) {
        $data = $users;
        $status = $noexcel == false ? $this->checkKeysExists($data) : 1;

        if ((int) $status == 1) {
            $status = '';

            foreach ($data as $value) {

                $valid_phone = validate_phone_number($value['phone']);
                if (!filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {

                    $status .= ' <div class="alert alert-danger">This email <b>' . $value['email'] . '</b>  is not valid. Record skipped </div>';
                }
                if (count($valid_phone) <> 2) {
                    $status .= ' <div class="alert alert-danger">This phone number <b>' . $value['phone'] . '</b>  is not valid. Record skipped </div>';
                }
                $prof = $value['profession'];
                $profession = Profession::where('name', $prof)->orWhere(DB::raw('lower(name)'), strtolower($prof))->first();
                if (!empty($profession)) {
                    $profession_id = $profession->id;
                } else {
                    $p = Profession::create(['name' => $prof]);
                    $profession_id = $p->id;
                }
                $user_data = [
                    'phone' => $valid_phone[1],
                    'email' => $value['email'],
                ];

                $user_info = User::orWhere($user_data)->first();

                $user = (empty($user_info)) ?
                        User::create(array_merge($user_data, ['password' => bcrypt('user12345'),
                            'name' => $value['name'], 'profession_id' => $profession_id, 'employer_id' => $employer->id, 'is_employer' => 0, 'user_type_id' => 7])) :
                        $user_info;
                $invoiceUser = InvoiceUser::create(['invoice_id' => $return->invoice_id, 'user_id' => $user->id]);

                foreach ($events as $event) {
                    UserEvent::firstOrCreate([
                        'user_id' => $user->id,
                        'event_id' => $event->eventId,
                    ]);
                }
                \App\Model\User::find($user->id)->update(['user_type_id'=>7]);
            }
        }

        $addToMeeting = (new BookingController())->saveUserMeetingsForSponsored($return->invoice_id);
        $this->data['status'] = $status;
        return redirect(url('invoice/' . $return->invoice_id))->with('info', $this->data['status']);
    }

    public function sendNotificationToUser($user, $sponsored = null) {
        $patterns = array(
            '/#name/i', '/#invoice/i', '/#email/i', '/#phone/i'
        );
        $replacements = array(
            $user->name, '', $user->email, $user->phone
        );
        if ($sponsored == 1) {
            $template = \App\Model\Sms_template::where('name', $sponsored == 1 ? 'sponsored' : 'welcome')->first();
            $sms = preg_replace($patterns, $replacements, $template->message);

            DB::table("emails")->insert(array('body' => $sms, 'subject' => 'Payment Accepted', 'email' => $user->email, 'user_id' => $user->id));
            DB::table('sms')->insert(array('phone' => $user->phone, 'body' => $sms, 'user_id' => $user->id));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ((int) $id > 0) {
            $this->data['invoice'] = Invoice::findOrFail($id);
            //$a = $invoice->invoiceFee()->get();
            //return $this->data['invoice']= $events->groupBy('item_name');
//            return $this->data['invoice']= $a->groupBy('event_id');
//            return $invoice_fee = $invoice->invoiceFee()->get();
//            return $invoice_fee = $invoice->invoiceFee()->get();
        } elseif ($id == 'bulk') {
            //bulk invoices
            return $this->bulkInvoice();
        } else {
            return $this->sponsoredInvoice();
        }

        //return $invoice_fee = $this->data['invoice']->invoiceFee()->get();
        return view('invoice.single', $this->data);
    }

    public function sponsoredInvoice() {
        $eventID = request('event');
        $totalAmount = 0;
        $paidAmount = 0;
        $unPaidAmount = 0;

        $userIDs = User::where('is_employer', 1)->get(['id']);
        if (!empty($eventID)) {
            $events = (int) $eventID > 0 ? [$eventID] : Event::where('status', 1)->get(['id']);
            $invoices = Invoice::where('invoice_type', 'sponsored')
                            ->whereIn('user_id', $userIDs)
                            ->whereIn('id', Invoice_fee::whereIn('event_id', $events)->get(['invoice_id']))->get('id');
            $invoiceFees = Invoice_fee::where('event_id', $eventID)->whereIn('invoice_id', $invoices)->get();
            $totalInvoices = $invoiceFees->count();
            foreach ($invoiceFees as $fee) {
                $amount = $fee->amount * $fee->users->count();
                $totalAmount += $amount;
                $paid = $fee->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            //'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount'
            $this->data['invoices'] = DB::table('invoices')
                            ->select(DB::raw('count(invoice_users) as totalusers'), 'invoices.id', 'invoices.number', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->join('invoice_fees', 'invoice_fees.invoice_id', '=', 'invoices.id')
                            ->join('invoice_users', 'invoice_users.invoice_id', '=', 'invoices.id')
                            ->where('invoice_fees.event_id', $eventID)
                            ->groupby('invoices.id', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->where('invoices.invoice_type', 'sponsored')
                            ->whereIn('invoices.user_id', $userIDs)
                            ->whereIn('invoices.id', Invoice_fee::whereIn('event_id', Event::where('status', 1)->get(['id']))->get(['invoice_id']))->get();
//            return $this->data['invoices'] = DB::table('invoices')
//                ->select(DB::raw('count(invoice_users) as totalusers'),'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount','users.name')
//                ->join('invoice_fees','invoice_fees.invoice_id','=','invoices.id')
//                ->join('invoice_users','invoice_users.invoice_id','=','invoices.id')
//                ->join('users','users.id','=','invoices.user_id')
//                ->where('invoice_fees.event_id',$eventID)
//                ->groupby('invoices.id','invoice_fees.event_id','invoice_fees.amount')
//                ->whereIn('invoices.user_id', $userIDs)->get();


            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;

            return view('invoice.bulkwithevent', $this->data);
        } else {

            $invoices = Invoice::where('invoice_type', 'sponsored')
                            ->whereIn('user_id', $userIDs)
                            ->whereIn('id', Invoice_fee::whereIn('event_id', Event::where('status', 1)->get(['id']))->get(['invoice_id']))->get('id');
            $totalInvoices = $invoices->count();
            foreach ($invoices as $invoice) {
                $amount = $invoice->getAmount();
                $totalAmount += $amount;
                $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            $this->data['invoices'] = Invoice::where('invoice_type', 'sponsored')
                            ->whereIn('user_id', $userIDs)
                            ->whereIn('id', Invoice_fee::whereIn('event_id', Event::where('status', 1)->get(['id']))->get(['invoice_id']))->get();
            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;
            //return $this->data;
            return view('invoice.sponsored', $this->data);
        }
    }

    public function bulkInvoice() {
        $eventID = request('event');
        $totalAmount = 0;
        $paidAmount = 0;
        $unPaidAmount = 0;
        // $events = (int) $eventID > 0 ? [$eventID] : Event::where('status', 1)->get(['id']);
        $userIDs = User::where('is_employer', 1)->get(['id']);
        $dataa = [];
//        $userIDs = User::where('is_employer', 1)
//                ->whereIn('id', UserEvent::whereIn('event_id', $events)->get(['user_id']))
//                ->get(['id']);

        if ((int) $eventID > 0) {
            $invoices = Invoice::where('invoice_type', 'bulk')
                            ->whereIn('user_id', $userIDs)->where('active', 1)->get();
            foreach ($invoices as $invoice) {
                $users = InvoiceUser::where('invoice_id', $invoice->id)->count();
                if ($users > 1) {
                    array_push($dataa, $invoice->id);
                }
            }
            $invoiceFees = Invoice_fee::where('event_id', $eventID)->whereIn('invoice_id', $dataa)->get();
            $totalInvoices = $invoiceFees->count();
            foreach ($invoiceFees as $fee) {
                $amount = $fee->amount * $fee->users->count();
                $totalAmount += $amount;
                $paid = $fee->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            //'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount'
            $this->data['invoices'] = DB::table('invoices')
                            ->select(DB::raw('count(invoice_users) as totalusers'), 'invoices.id', 'invoices.number', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->join('invoice_fees', 'invoice_fees.invoice_id', '=', 'invoices.id')
                            ->join('invoice_users', 'invoice_users.invoice_id', '=', 'invoices.id')
                            ->where('invoice_fees.event_id', $eventID)
                            ->groupby('invoices.id', 'invoice_fees.event_id', 'invoice_fees.amount')
                            ->where('invoices.invoice_type', 'bulk')
                            ->where('invoices.active', 1)
                            ->whereIn('invoices.user_id', $userIDs)->get();
//            return $this->data['invoices'] = DB::table('invoices')
//                ->select(DB::raw('count(invoice_users) as totalusers'),'invoices.id','invoices.number','invoice_fees.event_id','invoice_fees.amount','users.name')
//                ->join('invoice_fees','invoice_fees.invoice_id','=','invoices.id')
//                ->join('invoice_users','invoice_users.invoice_id','=','invoices.id')
//                ->join('users','users.id','=','invoices.user_id')
//                ->where('invoice_fees.event_id',$eventID)
//                ->groupby('invoices.id','invoice_fees.event_id','invoice_fees.amount')
//                ->whereIn('invoices.user_id', $userIDs)->get();


            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;

            if ((int) request('export') == 1) {
                $this->data['file_name_export'] = time();
                echo view('invoice.export.bulk_invoice_export', $this->data);
                return response()->download('storage/app/' . $this->data['file_name_export'] . '.xls');
            }
            return view('invoice.bulkwithevent', $this->data);
        } else {
            $invoices = Invoice::where('invoice_type', 'bulk')
                            ->whereIn('user_id', $userIDs)->where('active', 1)->get();
            foreach ($invoices as $invoice) {
                $users = InvoiceUser::where('invoice_id', $invoice->id)->count();
                if ($users > 1) {
                    array_push($dataa, $invoice);
                }
            }
            $totalInvoices = count($dataa);
            foreach ($dataa as $invoice) {
                $amount = $invoice->getAmount();
                $totalAmount += $amount;
                $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                $paidAmount += $paid;
                $unpaid = $amount - $paid;
                $unPaidAmount += $unpaid;
            }

            $this->data['invoices'] = $invoices;
            $this->data['events'] = Event::where(['status' => 1])->get();
            $this->data['total_invoices'] = $totalInvoices;
            $this->data['total_amount'] = $totalAmount;
            $this->data['paid_amount'] = $paidAmount;
            $this->data['unpaid_amount'] = $unPaidAmount;
            //return $this->data;
            if ((int) request('export') == 1) {
                $this->data['file_name_export'] = time();
                echo view('invoice.export.bulk_invoice_export', $this->data);
                return response()->download('storage/app/' . $this->data['file_name_export'] . '.xls');
            }
            return view('invoice.bulk', $this->data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->data['invoice'] = Invoice::find($id);
        return view('invoice.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    public function addUserFee($id) {
        $phone = validate_phone_number(request('phone'));
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }
        $valid_phone = $phone[1];

        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => request('email')])->first();

        $obj = array_merge(request()->except('_token', 'phone'), ['password' => bcrypt('user12345'), 'phone' => $valid_phone, 'created_by' => Auth::user()->id]);
        $user = (empty($user_info)) ?
                User::create($obj) : $user_info;
        $note = 'Engineers  Registration Board (ERB), Engineers Day Event';

        $amount = (new BookingController())->getFeeAmount($user->virtual, $user->virtual_material);
        $invoice_fee_data = [
            'invoice_id' => $id,
            'user_id' => $user->id,
        ];
        $invoice_fee_info = Invoice_fee::where($invoice_fee_data)->first();

        if (empty($invoice_fee_info)) {
            Invoice_fee::create(array_merge(['note' => $note, 'amount' => $amount, 'item_name' => $user->name], $invoice_fee_data));
            $this->sendNotificationToUser($user, 1);
            return redirect()->back()->with('success', 'Records added successful');
        } else {
            return redirect()->back()->with('warning', 'Records Exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $invoice = Invoice::find($id);
        $invoice->delete();
        //User::find($invoice->user_id)->delete();
        return redirect()->back()->with('success', 'Invoice Deleted');
    }

    public function feeDelete($id) {
        Invoice_fee::find($id)->delete();
        return redirect()->back()->with('success', 'Invoice Deleted');
    }

    public function createBilling() {
        //select all users with no invoice join all users with invoices but not paid
        //loop through to create billing
        //
//        $setting = \App\Model\Setting::first();
//        $amount = (new BookingController())->getFeeAmount();
//        $users = DB::select("select * from erb_new_payment.users where id not in (select user_id from erb_new_payment.invoices) and is_employer=0 and name not in ('BEATRICE MUNISHI','YARED PETER NGALABA','OMARY CHITAWALA','ENG. ANOLD A. KILEO','ISAKWISA AMBOKILE','ASHERY MWAIRWA KASEE') ");
//        foreach ($users as $user) {
//
//            $booking = ['user_id' => $user->id,
//                'amount' => $amount,
//                'username' => $setting->username,
//                'password' => $setting->password,
//                'name' => $user->name
//            ];
//            $booking_control = new BookingController();
//            $fee = \App\Model\Fee::find(1);
//            echo $booking_control->createBooking($booking, $fee->id, false);
//        }
    }

    public function updateBilling() {
        //select all users with no invoice join all users with invoices but not paid
        //loop through to create billing
        //
        $setting = \App\Model\Setting::first();

        // DB::statement('delete from erb_new_payment.invoices where id not in (select invoice_id from erb_new_payment.payments)');
        $users = DB::select("select * from erb_new_payment.users where id not in (select user_id from erb_new_payment.invoices ) and is_employer=1 and role_id is null");
        foreach ($users as $user) {
            $amount = (new BookingController())->getFeeAmount($user->virtual, $user->virtual_material);
            $booking = ['user_id' => $user->id,
                'amount' => $amount,
                'username' => $setting->username,
                'password' => $setting->password,
                'name' => $user->name
            ];
            $booking_control = new BookingController();
            $fee = \App\Model\Fee::find(1);
            $booking_control->createBooking($booking, $fee->id, 1);
            echo 'invoice for ' . $user->name . ' created successfully<br/>';
        }
    }

    public function updateControl() {

//        $requests = DB::select("select * from erb_new_payment.requests where content like '%controlNumber%' and created_at::date='2019-08-10' order by created_at desc");
//        foreach ($requests as $info) {
//
//            $request = (object) json_decode($info->content, true);
//
//            $user = \App\Model\User::where('email', $request->email)->first();
//            if (count($user) == 1) {
//                \App\Model\Invoice::where('user_id', $user->id)->update(['number' => $request->controlNumber]);
//                echo 'User ' . $user->name . ' update control no to ' . $request->controlNumber . '<br/>';
//            }
//        }
    }

}
