<?php

namespace App\Http\Controllers;

use App\Model\Event;
use App\Model\Fee;
use App\Model\Invoice_fee;
use App\Model\InvoiceUser;
use App\Model\User;
use App\Model\UserMeeting;
use \App\Model\Zoom;
use App\Model\ZoomMeeting;
use Illuminate\Http\Request;
use \App\Model\Invoice;
use \App\Model\Payment;
use \App\Model\Setting;
use \App\Model\Invoice_fees_payment;
use \App\Model\Receipt;
use DB;
use App\Http\Controllers\VPCPaymentConnection;
use Excel;

class PaymentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //return Payment::all();
//        $from = $this->data['from'] = request('from');
//        $to = $this->data['to'] = request('to');
//        $from_date = date('Y-m-d H:i:s', strtotime($from));
//        $to_date = date('Y-m-d H:i:s', strtotime($to. ' +1 day'));
//        $this->data['events'] = Event::all();
//        $event_id = request('event');
//        if (empty($event_id)) {
//            $this->data['payments'] = ($from != '' && $to != '') ?
//                Invoice_fees_payment::whereBetween('created_at', [$from_date, $to_date])->get() :
//                Invoice_fees_payment::all();
//        } else {
//            $fee = Invoice_fee::where('event_id', request('event'))->get('id');
//            $this->data['payments'] = ($from != '' && $to != '') ?
//                Invoice_fees_payment::whereBetween('created_at', [$from_date, $to_date])->whereIn('invoice_fee_id', $fee)->get() :
//                Invoice_fees_payment::whereIn('invoice_fee_id', $fee)->get();
//        }
        $event_id = request('event');
        $this->data['event_id'] = $event_id;

        $events = (int) $event_id > 0 ? [$event_id] : Event::where('status', 1)->get(['id']);
        $invoice_ids = Invoice::whereIn('user_id', \App\Model\UserEvent::whereIn('event_id',
                                $events)
                        ->get(['user_id']))
                ->get(['id']);

        //$this->data['payments'] = Payment::whereIn('invoice_id', $invoice_ids)->get();
        if ((int) $event_id == 0) {
            $this->data['payments'] = Payment::all();
        } else {
            $this->data['payments'] = Payment::whereIn('invoice_id', $invoice_ids)->get();
        }
        return view('payment.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        $invoice = Invoice::where("number", request('number'))->first();
        if (!empty($invoice)) {


// This is when a bank return payment status to us
//save it in the database
            $this->validate(request(), ['amount' => 'required|numeric', 'transaction_id' => 'required|unique:payments']);
            if (in_array(request('paymentType'), ['BANK', 'MPESA'])) {
                $payments = Payment::where('transaction_id', request('transaction_id'))->first();
                if (!empty($payments)) {
                    $data = array(
                        'status' => 1,
                        'success' => 0,
                        'reference' => $invoice->number,
                        'description' => 'Transaction ID has been used already to commit transaction'
                    );
                    die(json_encode($data));
                }
            }
            $mobile_transaction_id = request('mobile_transaction_id');
            if (request('amount') > $invoice->getAmount()) {
                return redirect()->back()->with('error', 'Payment not accepted. Amount paid is greater than amount required');
            }
            $payment = (new \App\Http\Controllers\ApiController())->acceptPayment(request('event'), request('amount'), $invoice->id, request('method'), request('transaction_id'), $mobile_transaction_id, request('name'), request('account_number'), request('transaction_time'), request('token'));
        }
        $this->sendBarcodeForm($invoice);
        $this->addZoomParticipant($invoice);

        //$this->dispatch(new \App\Jobs\syncPayment($query)); //push invoice in a que
        if (request('nametag') == 'on') {
            if ($invoice->user->is_employer == 1) {
                $user_ids = '';
                $ids = \App\Model\Invoice_fee::where('invoice_id', $invoice->id)->get(['user_id']);
                foreach ($ids as $id) {
                    $user_ids .= $id->user_id . ',';
                }
            } else {
                $user_ids = $invoice->user_id;
            }
            return redirect('user/bulknametag?single=1&ids=' . $user_ids);
        }
        $bulk = $invoice->type == 1 ? '/bulk' : '';
        return redirect('invoice' . $bulk)->with('success', json_decode($payment)->description);
    }

    public function sendNotification($invoice) {
        $patterns = array(
            '/#name/i', '/#invoice/i', '/#email/i', '/#phone/i'
        );
        $replacements = array(
            $invoice->user->name, $invoice->number, $invoice->user->email, $invoice->user->phone
        );
        //  $template = \App\Model\Sms_template::where('name', 'welcome')->first();
        // $sms = preg_replace($patterns, $replacements, $template->message);
        // DB::table("emails")->insert(array('body' => $sms, 'subject' => 'Payment Accepted', 'email' => $invoice->user->email, 'user_id' => $invoice->user->id));
        // DB::table('sms')->insert(array('phone' => $invoice->user->phone, 'body' => $sms, 'user_id' => $invoice->user->id));
    }

    public function sendZoomLink($invoice) {
        $zoomDetails = Zoom::find(1);
        $message = "Payment Received: \n Use the following details to attend the meeting,          
            \n Meeting Link: " . $zoomDetails->link . "
            \n Meeting ID: " . $zoomDetails->meetingid . "
            \n Passcode: " . $zoomDetails->password;

        $sendSMS = (new \App\Http\Controllers\ApiController())->send_sms($invoice->user->phone, $message, 1);
        $sendSMS = (new \App\Http\Controllers\ApiController())->send_email($invoice->user->email, 'ERB Zoom Meeting Details', $message, null);
    }

    public function addZoomParticipant($invoice) {
        $invoice_id = $invoice->id;
        $eventFees = Invoice_fee::where(['invoice_id' => $invoice_id, 'virtual' => 1])->get();
        if (!empty($eventFees)) {
            $users = InvoiceUser::where('invoice_id', $invoice_id)->get();
            foreach ($eventFees as $eventFee) {
                $eventID = $eventFee->event_id;
                $meeting = ZoomMeeting::where(['event_id' => $eventID])->first();
                $meetingID = $meeting->id;
                foreach ($users as $user) {
                    $userProfile = User::find($user->user_id);
                    $addUser = (new \App\Http\Controllers\MeetingController())->add_participant($userProfile, $meeting->meeting_id);
                }
            }
        }
    }

    public function sendBarcodeForm($invoice) {
        $invoice_id = $invoice->id;
        $eventFees = Invoice_fee::where(['invoice_id' => $invoice_id, 'virtual' => 0])->get();
        if (!empty($eventFees)) {
            $users = InvoiceUser::where('invoice_id', $invoice_id)->get();
            foreach ($eventFees as $eventFee) {
                foreach ($users as $user) {
                    $content = 'Please Click the link below to download/print your barcode form'
                            . '<br/>'
                            . '<br/>'
                            . '<a href="http://engineersday.co.tz/user/ticket/' . $user->user->id . '?auth=' . encrypt($user->user->id) . '" style="display: inline-block; margin-bottom: 0; font-weight: 40px; text-align: center;
    vertical-align: middle; cursor: pointer; background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 12px 24px; font-size: 14px; line-height: 1.428571429; border-radius: 4px;color: #fff; background-color: #5cb85c; border-color: #4cae4c;">EVENT BARCODE</a>';
                    $this->send_email($user->user->email, 'ERB Payment Accepted - Event Barcode ticket', $content);
                    //dispatch(new \App\Jobs\SendEmailJob("Payment Accepted", $userProfile->email, $content));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        if ($id == 'api') {
            $this->data['requests'] = \App\Model\Request::all();
        } else if ($id == 'summary') {
            $this->data['requests'] = \App\Model\Request::all();
        } else if ($id == 'receipts') {

            $event_id = (int) request('event') == 0 ?
                    Event::where('status', 1)
                            ->get(['id']) : [request('event')];

            $this->data['receipts'] = Payment::whereIn('invoice_id',
                            Invoice::whereIn('user_id', \App\Model\UserEvent::whereIn('event_id',
                                                    $event_id)
                                            ->get(['user_id']))->get(['id']))->get();

            // $this->data['receipts'] = Payment::all();
            //return Receipt::all();
//            $this->data['events'] = Event::where(['status' => 1])->whereIn('id',Fee::all('event_id'))->get();
//            if (empty(request()->event)) {
//                $this->data['receipts'] = Receipt::all();
//            } else {
//                $this->data['receipts'] = Receipt::where('event_id', request()->event)->get();
//            }
        } else if ($id == 'receiptpage') {
            return $this->showReceipt();
        } else if ($id == 'add') {
            return $this->addPayment();
        }
        return view('payment.' . $id, $this->data);
    }

    public function addPayment() {
        $invoice_id = request('id');
        $events = Invoice_fee::where(['invoice_id' => $invoice_id])->get('event_id');
        $this->data['events'] = Event::whereIn('id', $events)->get();
        $this->data['invoice'] = Invoice::find($invoice_id);
        return view('payment.add', $this->data);
    }

    public function showReceipt() {
        $this->data['receipt'] = Receipt::find(request('p'));

        return view('payment.receipt_page', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $payment = Payment::find($id);
        $payment->invoice->update(['status' => 0]);
        Invoice_fees_payment::where('payment_id', $id)->delete();
        $payment->delete();
        return redirect()->back()->with('sucess', 'Invoice Deleted');
    }

    public function card() {
        $id = request()->segment(2);
        $this->data['invoice'] = Invoice::find($id);
        $this->data['fee'] = \App\Model\Fee::first();
        if ($_POST) {
            return $this->cardProcessor();
        }
        return view('payment.card', $this->data);
    }

    private function cardProcessor() {
        $conn = new VPCPaymentConnection();

// This is secret for encoding the SHA256 hash
// This secret will vary from merchant to merchant

        $secureSecret = "";

// Set the Secure Hash Secret used by the VPC connection object
        $conn->setSecureSecret($secureSecret);

// Set the error flag to false
        $errorExists = false;

// *******************************************
// START OF MAIN PROGRAM
// *******************************************
// This is the title for display
// Add VPC post data to the Digital Order
        foreach ($_GET as $key => $value) {
            if (($key != "vpc_SecureHash") && ($key != "vpc_SecureHashType") && ((substr($key, 0, 4) == "vpc_") || (substr($key, 0, 5) == "user_"))) {
                $conn->addDigitalOrderField($key, $value);
            }
        }

// Obtain a one-way hash of the Digital Order data and
// check this against what was received.
        $serverSecureHash = array_key_exists("vpc_SecureHash", $_GET) ? $_GET["vpc_SecureHash"] : "";
        $secureHash = $conn->hashAllFields();
        if ($secureHash == $serverSecureHash) {
            $this->data['hashValidated'] = "<font color='#00AA00'><strong>CORRECT</strong></font>";
        } else {
            $this->data['hashValidated'] = "<font color='#FF0066'><strong>INVALID HASH</strong></font>";
            $errorsExist = true;
        }


        $Title = array_key_exists("Title", $_GET) ? $_GET["Title"] : "";
        $this->data['againLink'] = array_key_exists("AgainLink", $_GET) ? $_GET["AgainLink"] : "";
        $this->data['amount'] = array_key_exists("vpc_Amount", $_GET) ? $_GET["vpc_Amount"] : "";
        $this->data['locale'] = array_key_exists("vpc_Locale", $_GET) ? $_GET["vpc_Locale"] : "";
        $this->data['batchNo'] = array_key_exists("vpc_BatchNo", $_GET) ? $_GET["vpc_BatchNo"] : "";
        $this->data['command'] = array_key_exists("vpc_Command", $_GET) ? $_GET["vpc_Command"] : "";
        $this->data['message'] = array_key_exists("vpc_Message", $_GET) ? $_GET["vpc_Message"] : "";
        $this->data['version'] = array_key_exists("vpc_Version", $_GET) ? $_GET["vpc_Version"] : "";
        $this->data['cardType'] = array_key_exists("vpc_Card", $_GET) ? $_GET["vpc_Card"] : "";
        $this->data['orderInfo'] = array_key_exists("vpc_OrderInfo", $_GET) ? $_GET["vpc_OrderInfo"] : "";
        $this->data['receiptNo'] = array_key_exists("vpc_ReceiptNo", $_GET) ? $_GET["vpc_ReceiptNo"] : "";
        $this->data['merchantID'] = array_key_exists("vpc_Merchant", $_GET) ? $_GET["vpc_Merchant"] : "";
        $this->data['merchTxnRef'] = array_key_exists("vpc_MerchTxnRef", $_GET) ? $_GET["vpc_MerchTxnRef"] : "";
        $this->data['authorizeID'] = array_key_exists("vpc_AuthorizeId", $_GET) ? $_GET["vpc_AuthorizeId"] : "";
        $this->data['transactionNo'] = array_key_exists("vpc_TransactionNo", $_GET) ? $_GET["vpc_TransactionNo"] : "";
        $this->data['acqResponseCode'] = array_key_exists("vpc_AcqResponseCode", $_GET) ? $_GET["vpc_AcqResponseCode"] : "";
        $this->data['txnResponseCode'] = array_key_exists("vpc_TxnResponseCode", $_GET) ? $_GET["vpc_TxnResponseCode"] : "";
        $this->data['riskOverallResult'] = array_key_exists("vpc_RiskOverallResult", $_GET) ? $_GET["vpc_RiskOverallResult"] : "";

// Obtain the 3DS response
        $this->data['vpc_3DSECI'] = array_key_exists("vpc_3DSECI", $_GET) ? $_GET["vpc_3DSECI"] : "";
        $this->data['vpc_3DSXID'] = array_key_exists("vpc_3DSXID", $_GET) ? $_GET["vpc_3DSXID"] : "";
        $this->data['vpc_3DSenrolled'] = array_key_exists("vpc_3DSenrolled", $_GET) ? $_GET["vpc_3DSenrolled"] : "";
        $this->data['vpc_3DSstatus'] = array_key_exists("vpc_3DSstatus", $_GET) ? $_GET["vpc_3DSstatus"] : "";
        $this->data['vpc_VerToken'] = array_key_exists("vpc_VerToken", $_GET) ? $_GET["vpc_VerToken"] : "";
        $this->data['vpc_VerType'] = array_key_exists("vpc_VerType", $_GET) ? $_GET["vpc_VerType"] : "";
        $this->data['vpc_VerStatus'] = array_key_exists("vpc_VerStatus", $_GET) ? $_GET["vpc_VerStatus"] : "";
        $this->data['vpc_VerSecurityLevel'] = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $_GET["vpc_VerSecurityLevel"] : "";

// CSC Receipt Data
        $this->data['cscResultCode'] = array_key_exists("vpc_CSCResultCode", $_GET) ? $_GET["vpc_CSCResultCode"] : "";
        $this->data['ACQCSCRespCode'] = array_key_exists("vpc_AcqCSCRespCode", $_GET) ? $_GET["vpc_AcqCSCRespCode"] : "";

// Get the descriptions behind the QSI, CSC and AVS Response Codes
// Only get the descriptions if the string returned is not equal to "No Value Returned".

        $this->data['txnResponseCodeDesc'] = "";
        $this->data['cscResultCodeDesc'] = "";
        $avsResultCodeDesc = "";

        if ($this->data['txnResponseCode'] != "No Value Returned") {
            $this->data['txnResponseCodeDesc'] = getResultDescription($this->data['txnResponseCode']);
        }

        if ($this->data['cscResultCode'] != "No Value Returned") {
            $this->data['cscResultCodeDesc'] = getCSCResultDescription($this->data['cscResultCode']);
        }


        $this->data['error'] = "";
// Show this page as an error page if error condition
        if ($this->data['txnResponseCode'] == "7" || $this->data['txnResponseCode'] == "No Value Returned" || $errorExists) {
            $this->data['error'] = "Error ";
        }
        $this->data['title'] = $Title;
        return view('payment.card_processor', $this->data);
    }

    public function export() {
        $users = DB::select('SELECT  c.name, c.phone,c.email, e.name as employer_name, b.number, a.amount,d.name as channel, a.method,a.transaction_id,a.mobile_transaction_id,a.account_number, a.created_at::date as created_at from payments a join invoices b on a.invoice_id=b.id join users c on c.id=b.user_id join financial_entity d on d.id=a.financial_entity_id join employers e on e.id=c.employer_id');
        $title = ['name', 'phone number', 'email', 'employer name', 'invoice number', 'amount', 'channel', 'method', 'transaction ID', 'Mobile Transaction ID', 'Account Number', 'Time'];
        return $this->exportExcel($users, $title, 'payments');
    }

}
