<?php

namespace App\Http\Controllers;

use App\Model\Invoice_fee;
use App\Model\InvoiceUser;
use Illuminate\Http\Request;
use \App\Model\User;
use \App\Model\Role;
use \App\Model\Fee;
use \App\Model\User_type;
use \App\Model\Event;
use \App\Model\Zoom;
use \App\Model\Employer;
use \App\Model\Payment;
use \App\Model\Invoice;
use \App\Model\Profession;
use \App\Model\Financial_entity;
use \App\Model\Sms_template;
use DB;
use PDF;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\PaymentController;

class UserController extends Controller {

    public function __construct() {
        if (request('auth') == NULL) {
            $this->middleware('auth');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //profile
    }

    public function profile($id) {
        $this->data['user'] = User::findOrFail($id);
        return view('user.profile', $this->data);
    }

    public function ticket($id = null) {
        $this->data['padding_ticket'] = 1;
        $this->data['token'] = request('auth');
        if (strlen(request('auth')) > 2) {
            $auth_token = decrypt($this->data['token']);
            if ($auth_token != $id) {
                die('Request is not valid. Please click the link as supplied in your email address');
            }
        }
        $this->data['id'] = $id;
        return view('user.ticket', $this->data);
    }

    public function nametag($id = null) {
        //    $this->data['user'] = User::find($id);
//        $this->data['event'] = Event::first();
//        $this->data['barcode'] = (new SettingController())->createBarCode($id);
//        $this->data['setting'] = \App\Model\Setting::first();
//        return redirect('user/bulknametag/?single=1&ids=' . $id);
        //     return view('user.nametag', $this->data);
//         $ids = explode(',', trim(request('ids'), ','));
        $this->data['users'] = User::where('id', $id)->where('is_employer', '<>', 1)->get();
        $this->data['event'] = Event::where('status', 1)->first();
        $this->data['setting'] = \App\Model\Setting::first();
        $this->storePrintEvent([$id], $this->data['event']);
//        if (request('single') == 1) {
        // return view('user.bulknametag', $this->data);
        PDF::setOptions(['dpi' => 10, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('user.bulknametag', $this->data);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream('pdf_nametag.pdf');
//        }
//        PDF::setOptions(['dpi' => -10, 'defaultFont' => 'sans-serif']);
//        $pdf = PDF::loadView('user.bulknametag', $this->data);
//        $pdf->setPaper('A4', 'landscape');
//        return $pdf->stream('pdf_nametag.pdf');
    }

    public function storePrintEvent($ids, $event) {
        $ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
        $staff_id = Auth::user()->id;
        foreach ($ids as $id) {
            \App\Model\Nametag_printlog::create([
                'user_id' => $id,
                'staff_id' => $staff_id,
                'event_id' => $event->id,
                'user_agent' => json_encode(['printer' => request('single') == 1 ? 'EPSON' : 'HP', 'IP' => $ip])
            ]);
        }
    }

    public function bulkNameTag() {
        $ids = explode(',', trim(request('ids'), ','));
        $this->data['users'] = User::whereIn('id', array_filter($ids))->where('is_employer', '<>', 1)->get();
        $this->data['event'] = Event::where('status', 1)->first();
        $this->data['setting'] = \App\Model\Setting::first();
        $this->storePrintEvent($ids, $this->data['event']);
        if (request('single') == 1) {
            // return view('user.bulknametag', $this->data);
            PDF::setOptions(['dpi' => 10, 'defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('user.bulknametag', $this->data);
            $pdf->setPaper('A4', 'portrait');
            return $pdf->stream('pdf_nametag.pdf');
        }
        PDF::setOptions(['dpi' => -10, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('user.bulknametag', $this->data);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('pdf_nametag.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ($request->user == 'entity') {
            Financial_entity::create($request->all());
        } else if ($request->user == 'user_type') {
            User_type::create($request->all());
        } else if ($request->user == 'role') {
            Role::create($request->all());
        } else if ($request->user == 'user') {
            $this->validate(request(), ['phone' => 'required|unique:users,phone',
                'email' => 'required|email|unique:users,email']);
            $role_id = request('role_id');
            $pass = 'Erb@12_Africa' . rand(43434, 4343434);
            User::create(array_merge($request->all(), ['user_type_id' => 1, 'employer_id' => 1, 'password' => bcrypt($pass)]));
            if ((int) $role_id > 0) {
                $message = 'Your Account has been created'
                        . '<br/>'
                        . 'Login email: ' . $request->email
                        . '<br/>'
                        . 'Login password: ' . $pass;
                $this->send_email($request->email, 'User Account', $message);
                //dispatch(new \App\Jobs\SendEmailJob("Staff Credential", $request->email, $message));
            }
        } else if ($request->user == 'fee') {
            $checkEventFee = Fee::where('event_id', $request->event_id)->count();
            if ($checkEventFee) {
                return redirect()->back()->with('error', 'Fees exist.');
            } else {
                Fee::create($request->all());
            }
        } else if ($request->user == 'event') {
            if (isset($request->virtual)) {
                $data['topic'] = $request->name;
                $data['start_time'] = $request->date;
                $data['duration'] = 100000;
                $data['agenda'] = $request->theme;
                $data['host_video'] = 1;
                $data['participant_video'] = 1;
                $event = Event::create($request->all());
                $createMeeting = (new \App\Http\Controllers\MeetingController())->index($event->id, $data);
            } else {
                $event = Event::create($request->all());
            }
        } else if ($request->user == 'zoom') {
            $checkZoomLink = Zoom::where('event_id', $request->event_id)->count();
            if ($checkZoomLink) {
                return redirect()->back()->with('error', 'Zoom Link exist.');
            } else {
                Zoom::create($request->all());
            }
        } else if ($request->user == 'employer') {
            $employer = Employer::create($request->all());
            $user = User::create(array_merge($request->all(), ['password' => 123456789, 'is_employer' => 1, 'user_type_id' => 120, 'employer_id' => $employer->id, 'email' => time() . 'jkdjs@engineers.co.tz']));
            $user->number = 'ERB' . $user->id;
            $user->save();
        } else if ($request->user == 'profession') {
            Profession::create($request->all());
        } else if ($request->user == 'sms_template') {
            Sms_template::create($request->all());
        } else if ($request->user == 'sms') {
            (new \App\Http\Controllers\InboxController())->sendSms();
        } else if ($request->user == 'schedule') {
            \App\Model\Schedule::create(array_merge($request->except('days', 'time'), ['days' => implode(',', $request->days), 'time' => date('Y-m-d h:i', strtotime($request->time))]));
        } else if ($request->user == 'payment') {
            return (new PaymentController())->store();
        }
        return redirect()->back()->with('success', 'success');
    }

    public function saveUser($basic_param, $other) {
        $user = User::orWhere($basic_param)->first();
        if (empty($user)) {
            $record = User::create(array_merge($basic_param, array('user_type_id' => $other['id'])));
        } else {
            $record = $user;
            $user->update(array_merge($basic_param, array('user_type_id' => $other['id'])));
        }
        return $record;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'entity') {
            $this->data['entities'] = Financial_entity::all();
            return view('user.entity', $this->data);
        } else if ($id == 'applicants') {
            $this->data['events'] = Event::where('status', 1)->get();
            $event_id = request('event');
            $this->data['event_id_selected'] = (int) $event_id > 0 ? [$event_id] : Event::where('status', 1)->get(['id']);

            if (!empty(request()->paid)) {
                  
                if (empty(request()->event)) {
                    $invoiceID = Payment::get('invoice_id');

                    if (!empty($invoiceID)) {
                        $invoice = Invoice::whereIn('id', $invoiceID)->get();
                        $totalPayments = Payment::whereIn('invoice_id', $invoiceID)->sum('amount');
                        $userIDs = InvoiceUser::whereIn('invoice_id', $invoiceID)->get('user_id');
                   
                        $users = User::whereIn('id', $userIDs)->get();
                        $this->data['users'] = $users;
                        $this->data['totalusers'] = count((array)$users);
                        $this->data['paidamounts'] = $totalPayments;
                    }
                     return view('user.applicants', $this->data);
                } else {
                    
                    $inv = array();
                    $amount = 0;
                    $invoiceFeesIds = Invoice_fee::where('event_id', request()->event)->get('invoice_id');
                    $invoices = Invoice::whereIn('id', $invoiceFeesIds)->get('id');
                    $payU = Payment::whereIn('invoice_id', $invoices)->get('invoice_id');
                    $invoiceIds = Invoice::whereIn('id', $payU)->get('id');
                    $userIDs = InvoiceUser::whereIn('invoice_id', $invoiceIds)->get('user_id');
                    $totalPayments = Payment::whereIn('invoice_id', $invoiceIds)->sum('amount');
                    $users = User::whereIn('id', $userIDs)->get();
                    $this->data['users'] = $users;
                    $this->data['totalusers'] = count($users);
                    $this->data['paidamounts'] = $totalPayments;
                }

                return view('user.applicants', $this->data);
            } else {
                if (empty(request()->event)) {
                    $userType = User_type::find(request()->user_type);
                    $this->data['usertypedata'] = $userType;

                    if (request()->user_type == 0) {
                        $userIds = \App\Model\UserEvent::whereIn('event_id', $this->data['event_id_selected'])->get(['user_id']);
                        $this->data['users'] = User::where('user_type_id', '!=', 120)
                                        ->whereIn('id', \App\Model\UserEvent::whereIn('event_id', $this->data['event_id_selected'])->get(['user_id']))->get();
                        $this->data['totalusers'] = count($this->data['users']);
                      
                        $paids = Invoice::whereIn('user_id', $userIds)->get();
                        $amount = 0;
                        foreach ($paids as $paid) {
                            $payments = $paid->payment;
                            if (!empty($payments)) {
                                foreach ($payments as $payment) {
                                    $amount = $amount + $payment->amount;
                                }
                            }
                        }
                        $this->data['paidamounts'] = $amount;
                     
                    } else {
                        $users = User::where('user_type_id', $userType->id)
                                ->whereIn('id', \App\Model\UserEvent::whereIn('event_id', $this->data['event_id_selected'])->get(['user_id']))
                                ->get();
                        $userIds = User::where('user_type_id', $userType->id)->whereIn('id', \App\Model\UserEvent::whereIn('event_id', $this->data['event_id_selected'])->get(['user_id']))->get('id');
                        $this->data['users'] = $users;
                        $this->data['totalusers'] = count($users);
                        $paids = Invoice::whereIn('user_id', $userIds)->get();
                        $amount = 0;
                        foreach ($paids as $paid) {
                            $payments = $paid->payment;
                            if (!empty($payments)) {
                                foreach ($payments as $payment) {
                                    $amount = $amount + $payment->amount;
                                }
                            }
                        }
                        $this->data['paidamounts'] = $amount;
                    }
                
                    return view('user.applicants', $this->data);
                } else {
                    $this->data['eventdata'] = Event::find(request()->event);
//                $this->data['usertypedata'] = User_type::find(request()->user_type);
//                return view('user.applicants', $this->data);
                    $userType = User_type::find(request()->user_type);

                    $this->data['usertypedata'] = $userType;
                    if (request()->user_type == 0) {

                        $getInvoiceIDsBasedOnEvent = Invoice_fee::where('event_id', request()->event)->get('invoice_id');
                        $getUserIds = InvoiceUser::whereIn('invoice_id', $getInvoiceIDsBasedOnEvent)->get('user_id');
                        $users = User::where('user_type_id', '!=', 120)->whereIn('id', $getUserIds)->get();
                        $userIds = User::whereIn('id', $getUserIds)->get('id');
                        $this->data['users'] = $users;
                        $this->data['totalusers'] = count($users);

//                    $userIds = User::get('id');
//                    $this->data['users'] = User::all();
//                    $this->data['totalusers'] = count(User::all());
                        $invoiceIDs = Invoice::whereIn('user_id', $userIds)->get('id');
                        $fees = Invoice_fee::where('event_id', request()->event)->whereIn('invoice_id', $invoiceIDs)->get();
                        //invoiceFeesPayment
                        $amount = 0;
                        foreach ($fees as $fees) {
                            $payments = $fees->invoiceFeesPayment;
                            if (!empty($payments)) {
                                foreach ($payments as $payment) {
                                    $amount = $amount + $payment->paid_amount;
                                }
                            }
                        }
                        $this->data['paidamounts'] = $amount;
                    } else {
                        $getInvoiceIDsBasedOnEvent = Invoice_fee::where('event_id', request()->event)->get('invoice_id');
                        $getUserIds = InvoiceUser::whereIn('invoice_id', $getInvoiceIDsBasedOnEvent)->get('user_id');
                        $users = User::where('user_type_id', $userType->id)->whereIn('id', $getUserIds)->get();
                        $userIds = User::where('user_type_id', $userType->id)->whereIn('id', $getUserIds)->get('id');
                        $this->data['users'] = $users;
                        $this->data['totalusers'] = count($users);
                        $invoiceIDs = Invoice::whereIn('user_id', $userIds)->get('id');
                        $fees = Invoice_fee::where('event_id', request()->event)->whereIn('invoice_id', $invoiceIDs)->get();
                        //invoiceFeesPayment
                        $amount = 0;
                        foreach ($fees as $fees) {
                            $payments = $fees->invoiceFeesPayment;
                            if (!empty($payments)) {
                                foreach ($payments as $payment) {
                                    $amount = $amount + $payment->paid_amount;
                                }
                            }
                        }
                        $this->data['paidamounts'] = $amount;
                    }
                    return view('user.applicants', $this->data);
                }
            }
        } elseif ($id == 'organizations') {
            // $this->data['organizations'] = \App\Model\Employer::orderBy('id', 'desc')->get();
            return view('user.organizations', $this->data);
        } else if ($id == 'bulknametag') {
            return $this->bulkNameTag();
        } else if ($id == 'getApplicants') {
            return $this->getApplicants();
        } else if ($id == 'attendance') {
            return $this->attendance();
        } else if ($id == 'addAttendance') {
            return $this->addAttendance();
        } else if ($id == 'addBulkAttendance') {
            return $this->AddBulkAttendance();
        } else if ($id == 'barcodeAttendance') {
            return $this->barcodeAttendance();
        } else if ($id == 'invite') {
            return $this->invite();
        } else {
            $this->data['users'] = User::whereNotNull('role_id')->paginate(200);
            return view('user.staff', $this->data);
        }
    }

    public function site() {
        $id = request()->segment(3);
        $this->data['users'] = $id == 1 ? User::where('virtual', 1)->get() :
                User::where('virtual', '<>', 1)->where('is_employer', '<>', 1)->get();
        return view('user.user_record', $this->data);
    }

    public function invite() {
        $this->data['applicants'] = User::whereNull('role_id')->where('user_type_id', 13)->get();
        return view('user.invite', $this->data);
    }

    public function barcodeAttendance() {
        $number = request('s');
        $user = User::where(DB::raw('lower(number)'), strtolower(trim($number)))->first();
        if (!empty($user)) {
            $day = request('date');
            $add = $this->addSingleUserAttendance($user->id, $day, 1);
            echo $add == true ? ('<span class="label label-success">success</span>') :
                    '<span class="label label-info">updated</span>';
        } else {
            echo '<span class="label label-danger">Error: User not found</span>';
        }
    }

    public function addBulkAttendance() {
        $type = request('type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        if ($type == null || $type == 0) {
            $users = User::whereNull('role_id')->get();
        } else {
            $users = $type == 120 ? User::where('is_employer', 1)->get() :
                    User::whereNull('role_id')->whereIn('user_type_id', $user_types)->get();
        }
        foreach ($users as $user) {
            $present = request('status') == 'false' ? 0 : 1;
            $this->addSingleUserAttendance($user->id, request('date'), $present);
        }
        echo 'success';
    }

    function addAttendance() {
        $eventid = request('event_id');
        $id = request('user_id');
        $day = request('date');
        if ((int) $id) {
            $present = request('status') == 'false' ? 0 : 1;
            $add = $this->addUserAttendance($id, $day, $present, $eventid);
            echo $add == true ? ('success') : 'updated';
        }
    }

    public function addSingleUserAttendance($user_id, $day, $present) {
        $where = ['user_id' => $user_id, 'date' => $day];
        $found = \App\Model\Attendance::where($where);
        if (!empty($found->first())) {
            //update              
            $data = array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]);
            $found->update($data);
            return false;
        } else {
            \App\Model\Attendance::create(array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]));
            return TRUE;
        }
    }

    public function addUserAttendance($user_id, $day, $present, $event_id) {
        $where = ['user_id' => $user_id, 'date' => $day, 'event_id' => $event_id];
        $found = \App\Model\Attendance::where($where);
        if (!empty($found->first())) {
            //update
            $data = array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]);
            $found->update($data);
            return false;
        } else {
            \App\Model\Attendance::create(array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]));
            return TRUE;
        }
    }

    public function attendance() {
        $eventID = request()->event;
        if (!empty($eventID)) {
            $invoiceIDs = Invoice_fee::where('event_id', $eventID)->get('invoice_id');
            $userIDs = InvoiceUser::whereIn('invoice_id', $invoiceIDs)->get('user_id');
            $this->data['events'] = Event::where('status', 1)->get();
            //$user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
            $this->data['event'] = Event::find($eventID);
            $this->data['thisevent'] = Event::find($eventID);
            $this->data['applicants'] = User::whereIn('id', $userIDs)->get();
        } else {
            $this->data['events'] = Event::where('status', 1)->get();
            $this->data['thisevent'] = \App\Model\Event::first();
//            $type = request('user_type');
//            $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
//            $this->data['event'] = \App\Model\Event::first();
//            $this->data['applicants'] = User::where('is_employer', 0)->get();
        }


        return view('user.attendance', $this->data);
    }

    public function getApplicants() {
        $type = request('type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        if ((int) request('employer_id') > 0) {
            $other_return = [];
            if ((int) request('user_id')) {
                $invoice = \App\Model\Invoice::where('user_id', request('user_id'))->first();
                $other_return = $invoice->invoiceFee()->get(['user_id as id']);
            }
            $obj = User::where('employer_id', request('employer_id'))->where('is_employer', '<>', 1)->get(['id']);
            $returns = $obj->merge($other_return);
        } else {
            $returns = ($type == null || $type == 0) ?
                    User::whereNull('role_id')->get(['id']) :
                    User::whereIn('user_type_id', $user_types)->get();
        }

        foreach ($returns as $return) {
            echo $return->id . ',';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        User::find($id)->update($request->all());
        \App\Model\Email::where('user_id', $id)->update(['email' => request('email')]);
        \App\Model\Sms::where('user_id', $id)->update(['phone' => request('phone')]);
        return redirect()->back()->with('success', 'User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (request('type') == 'user') {
            User::find($id)->delete();
        } else if (request('type') == 'user_type') {
            User_type::find($id)->delete();
        } else if (request('type') == 'role') {
            Role::find($id)->delete();
        } else if (request('type') == 'fee') {
            Fee::find($id)->delete();
        } else if (request('type') == 'event') {
            Event::find($id)->delete();
        } else if (request('type') == 'profession') {
            Profession::find($id)->delete();
        } else if (request('type') == 'organization') {
            Employer::find($id)->delete();
        } else if (request('type') == 'sms_template') {
            Sms_template::find($id)->delete();
        } else if (request('type') == 'schedule') {
            \App\Model\Schedule::find($id)->delete();
        } else {
            Financial_entity::find($id)->delete();
        }
        return redirect()->back()->with('Deleted Successiful', 'success');
    }

    public function password() {
        if ($_POST) {
            $current = request('current');
            $user = User::find(Auth::user()->id);
            if (Auth::attempt(['email' => $user->email, 'password' => $current])) {
                $new1 = request('new1');
                $new2 = request('new2');
                if ($new1 != $new2) {
                    return redirect()->back()->with('error', 'New password and confirmed one  do not matchs');
                }

                $this->validate(request(), [
                    'new2' => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
                        ], ['Password must be 8–30 characters, and include a number, a symbol, a lower and a upper case letter']);
                $user->update(['password' => Hash::make($new1)]);
                return redirect()->back()->with('success', 'Password changed successfully');
            } else {

                return redirect()->back()->with('error', 'Current Password is not valid');
            }
        }
        return view('auth.passwords.change', $this->data);
    }

    public function export() {
        if (request()->segment(3) == 90) {
            $where = (int) request()->segment(4) == 0 ? '' : ' where e.event_id=' . request()->segment(4);
            $sql = 'select distinct a.name, a.phone, a.email, b.name as employer, e.amount, '
                    . ' c.name as type, d.name as specialization  from users a left '
                    . ' join employers b on b.id=a.employer_id left join user_types c '
                    . ' on c.id=a.user_type_id left join professions d on d.id=a.profession_id '
                    . ' join payment_balance e on e.user_id=a.id' . $where;
        } else if (request()->segment(3) == 151) {
            $userIDs = User::where('is_employer', 0)->get(['id']);
            $invoices = Invoice::where('invoice_type', 'normal')->whereIn('user_id', $userIDs)->get();
            $sql = 'select users.name, users.phone, users.email,employers.name as employer, user_types.name as type, professions.name as specialization, x.amount from users left join user_types on users.user_type_id=user_types.id left join employers on employers.id=users.employer_id  left join professions on professions.id=users.profession_id  left join (select i.user_id, sum(p.amount) as amount from payments p join invoices i on p.invoice_id=i.id  group by i.user_id)  x on x.user_id=users.id  where' . $paid_only;
            $users = DB::select($sql);
            $title = ['name', 'phone number', 'email', 'employer name', 'type', 'specialization', 'amount'];
            return $this->exportExcel($users, $title, 'users');
        } else {
            $user_type = request()->segment(3) == 0 ? '' : ' where user_type_id=' . request()->segment(3);
            $sql = 'select users.name, users.phone,users.email, employers.name as employer, user_types.name as type, professions.name as specialization from users left join user_types on users.user_type_id=user_types.id left join employers on employers.id=users.employer_id  left join professions on professions.id=users.profession_id  ' . $user_type;
        }

        $users = DB::select($sql);
        $title = ['name', 'phone number', 'email', 'employer name', 'amount', 'type', 'specialization'];

        return $this->exportExcel($users, $title, 'users');
    }

    public function exportreport() {
        if (request()->segment(3) == 90) {
            $paid_only = '  users.id in (select user_id from invoices where id in (select invoice_id from payments)) ';
            $sql = 'select users.name, users.phone, users.email,employers.name as employer, user_types.name as type, professions.name as specialization from users left join user_types on users.user_type_id=user_types.id left join employers on employers.id=users.employer_id  left join professions on professions.id=users.profession_id  left join (select i.user_id, sum(p.amount) from payments p join invoices i on p.invoice_id=i.id  group by i.user_id)  x on x.user_id=users.id  where' . $paid_only;
        } else {
            $user_type = request()->segment(3) == 0 ? '' : ' where user_type_id=' . request()->segment(3);
            $sql = 'select users.name, users.phone,users.email, employers.name as employer, user_types.name as type, professions.name as specialization from users left join user_types on users.user_type_id=user_types.id left join employers on employers.id=users.employer_id  left join professions on professions.id=users.profession_id  ' . $user_type;
        }

        $users = DB::select($sql);
        $title = ['name', 'phone number', 'email', 'employer name', 'type', 'specialization'];
        return $this->exportExcel($users, $title, 'users');
    }

}
