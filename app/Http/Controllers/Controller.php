<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public $data = [];

    public function __construct() {
        $this->data['setting'] = \App\Model\Setting::first();
        $this->data['events'] = \App\Model\Event::where('status', 1)->get();
        // $this->logRequest();
    }

    public function user() {
        return Auth::user();
    }

    public function logRequest() {
        
    }

    public function segment($id) {
        $enc = mb_detect_encoding(trim(request()->segment($id)), "UTF-8,ISO-8859-1");
        return iconv($enc, "UTF-8", trim(request()->segment($id)));
    }

    /**
     * 
     * @param type $fields : Array  values to push , 
      @param $url : Url to fetch or push records
     */
    public function curlServer($fields, $url, $header = null) {
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);
        $pheader = $header == null ? array(
            'application/x-www-form-urlencoded'
                ) : $header;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $pheader);

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function send_email($email, $subject, $message, $attachment = null) {

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = \App\Model\User::where('email', $email)->first();
            $obj = array('body' => is_array($message) ? $message['message'] : $message, 'subject' => $subject, 'email' => $email, 'attachment' => $attachment);
            if (!empty($user)) {
                $_array = array('user_id' => $user->id);
                DB::table('emails')->insert(array_merge($_array, $obj));
            } else {
                DB::table('emails')->insert($obj);
            }
        }
        return TRUE;
    }

    public function send_sms($phone_number, $message, $message_type = 1) {
        if ((strlen($phone_number) > 6 && strlen($phone_number) < 20) && $message != '') {
            $user = \App\Model\User::where('phone', $phone_number)->first();
            if (!empty($user)) {
                DB::table('sms')->insert(array('phone' => $phone_number,
                    'body' => $message,
                    'user_id' => $user->id,
                    'type' => 1));
            } else {
                DB::table('sms')->insert(array('phone' => $phone_number, 'body' => $message, 'type' => $message_type));
            }
        }
        return TRUE;
    }

    public function uploadExcel() {

        try {
            // it will be your file name that you are posting with a form or c
            $folder = "storage/";
            is_dir($folder) ? '' : mkdir($folder, 0777, True);

            $file = request()->file('file');

            $name = time() . rand(4343, 3243434) . '.' . $file->guessClientExtension();
            $move = $file->move($folder, $name);

            $path = $folder . $name;

            if (!$move) {
                die('upload Error');
            } else {
                $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            }
        } catch (Exception $e) {
            $this->resp->success = FALSE;
            $this->resp->msg = 'Error Uploading file';
            echo json_encode($this->resp);
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = 'D';
        //$highestColumn = $sheet->getHighestColumn();
        $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);

        $data = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData[0] = array_combine($headings[0], $rowData[0]);
         
            if (!empty($rowData[0][$headings[0][0]])) {
                $data = array_merge_recursive($data, $rowData);
            }
        }
        unlink($path);
        return $data;
    }

    public function exportExcel($data_array, $title, $file_name = 'file') {

        $table = '<table><thead><tr>';
        foreach ($title as $key) {
            $table .= '<th>' . $key . '</th>';
        }
        $table .= '</tr></thead><tbody>';
        foreach ($data_array as $key1 => $_value) {
            $table .= '<tr>';
            foreach ($_value as $key => $value) {
                $table .= '<td>' . str_replace('&', ' AND ', $value) . '</td>';
            }
            $table .= '</tr>';
        }
        $table .= '</tbody></table>';
        //echo $table; exit;
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();

        $spreadsheet = $reader->loadFromString($table);

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename = "' . $file_name . '.xls' . '"');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        //$writer->save($file_name . '.xls');
        $writer->save('php://output');
    }

    public function ajaxTable($table, $columns, $custom_sql = null) {
        ## Read value
        if (isset($_POST) && request()->ajax() == true) {
            $draw = $_POST['draw'];
            $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value']; // Search value
## Search 
            $searchQuery = " ";
            if ($searchValue != '') {
                $searchQuery = " and ( ";
                $list = '';
                foreach ($columns as $column) {
                    $list .= 'lower(' . $column . "::text) like '%" . strtolower($searchValue) . "%' or ";
                }
                $searchQuery = $searchQuery . rtrim($list, 'or ') . ' )';
            }

## Total number of records without filtering
            // $sel = DB::select("select count(*) as allcount from employee");
## Total number of record with filtering
## Fetch records

            if (strlen($custom_sql) < 2) {
                $sel = DB::select("select * from " . $table . " WHERE true " . $searchQuery);

                $empQuery = "select * from " . $table . " WHERE true " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
            } else {
                $empQuery = $custom_sql . " " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
                $sel = DB::select($custom_sql);
            }
            $empRecords = DB::select($empQuery);

## Response
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => count($sel),
                "iTotalDisplayRecords" => count($sel),
                "aaData" => $empRecords
            );

            return json_encode($response);
        }
    }

}
