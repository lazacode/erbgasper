<?php

namespace App\Http\Controllers;

use App\Model\InvoiceUser;
use Illuminate\Http\Request;
use \App\Model\Invoice;
use \App\Model\Payment;
use \App\Model\Invoice_fee;
use \App\Model\User;
use \App\Model\Event;
use DB;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function handle() {

        $payments = (array) \collect(DB::select('select a.number as booking_number,d.name, b.amount as amount_paid, d.phone as phone_number, b.method as description, b.transaction_id  as reference_number,  c.name as agent, \'dsdlksdslkjdlkssds\' as api_password FROM erb_payment.invoices a join erb_payment.payments b on b.invoice_id=a.id join erb_payment.financial_entity c on c.id=b.financial_entity_id join erb_payment.users d on d.id=a.user_id '))->first();
        // return $this->curlServer($payments, 'http://localhost/erb/erb_api.php');
    }

    function pushSMS($message, $phone_number) {
        $api_key = '5e0b7f1a911dd411';
        $secret_key = 'MDI2ZGVlMWExN2NlNzlkYzUyYWE2NTlhOGE0MjgyMDRmMjFlMDFjODkwYjU2NjA4OTY4NzZlY2Y3NGZjY2Y0Yw==';

        $postData = array(
            'source_addr' => 'INFO',
            'encoding' => 0,
            'schedule_time' => '',
            'message' => $message,
            'recipients' => [array('recipient_id' => '1',
            'dest_addr' => $phone_number)]
        );

        $Url = 'https://apisms.beem.africa/v1/send';

        $ch = curl_init($Url);
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization:Basic ' . base64_encode("$api_key:$secret_key"),
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        $response = curl_exec($ch);

        if ($response === FALSE) {
            echo $response;

            die(curl_error($ch));
        }
        return $response;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      
        $this->data['total_invoices'] = Invoice::count();
        $this->data['amount_tobe_collected'] = Invoice_fee::sum('amount');
        $this->data['total_money_collected'] = Payment::sum('amount');
        $this->data['total_users'] = \collect(DB::select('select count(distinct user_id) as count from user_events where event_id in (select id from events where status=1)'))->first()->count;

        //$this->data['virtual'] = User::where('virtual', 1)->count();
        //$this->data['nosite_visit'] = User::where('virtual', '<>', 1)->orWhereNull('virtual')->where('is_employer', '<>', 1)->count();
        $this->data['organizations'] = \App\Model\Employer::count();
        $this->data['applicants'] = User::where('is_employer', '<>', 1)->count();
        $this->data['events'] = Event::where('status', 1)->count();
        $this->data['erb_staff'] = User::whereNotNull('role_id')->count();
        $this->data['sms'] = \App\Model\Sms::count();
        $this->data['email'] = \App\Model\Email::count();
        $this->data['sms_pending'] = \App\Model\Sms::where('status', 0)->count();
        $this->data['email_pending'] = \App\Model\Email::where('status', 0)->count();

        $this->data['sms_status'] = json_decode(\karibusms::statistics());

        $this->data['allevents'] = Event::where('status', 1)->get();
        return view('home', $this->data);
    }

    public function search() {
        $q = request('s');
        $sq = explode(' ', $q);
        if (strlen($q) > 1) { //prevent empty search which load all results
            $users = \App\Model\User::leftJoin('employers', 'employers.id', 'users.employer_id')->leftJoin('invoices', 'invoices.user_id', 'users.id');

            foreach ($sq as $q) {
                $users->orWhere('employers.name', 'ilike', "%{$q}%");
                $users->orWhere('email', 'ilike', "%{$q}%");
                $users->orWhere('users.name', 'ilike', "%{$q}%");
                $users->orWhere('abbreviation', 'ilike', "%{$q}%");
                //   ->orWhere('invoices.number', 'ilike', "%{$q}%")
                $users->orWhere('users.number', $q);
                $users->orWhere('phone', 'ilike', "%{$q}%")->select('users.*');
            }


            $employers = \App\Model\Employer::where('name', 'ilike', "%{$q}%")
                            ->orWhere('abbreviation', 'ilike', "%{$q}%")
                            ->orWhere('location', 'ilike', "%{$q}%")->get();

            strlen(request('user_id_tags')) > 1 ?
                            $users->orWhereIn('users.id', explode(',', trim(request('user_id_tags'), ','))) : NULL;
            $this->data['users'] = $users->get();
            $this->data['employers'] = $employers;
            $invoices = \App\Model\Invoice::where('number', 'ilike', "%{$q}%")->get();
            $this->data['invoices'] = $invoices;
            return request('type') == 1 ?
                    view('layouts.search_focus', $this->data) : view('layouts.search', $this->data);
        }
    }

    public function getTotalApplicants($event_id) {
        $invoicesIDs = Invoice_fee::where('event_id', $event_id)->get('invoice_id');
        $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
        return $users = User::whereIn('id', $usersList)->count();
    }

    public function getTotalPhysicalApplicants($event_id) {
        $invoicesIDs = Invoice_fee::where(['event_id' => $event_id, 'virtual' => 0])->get('invoice_id');
        $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
        return $users = User::where('user_type_id', '!=', 7)->whereIn('id', $usersList)->count();
    }

    public function getTotalVirtualApplicants($event_id) {
        $invoicesIDs = Invoice_fee::where(['event_id' => $event_id, 'virtual' => 1])->get('invoice_id');
        $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
        return $users = User::where('user_type_id', '!=', 7)->whereIn('id', $usersList)->count();
    }

    public function getTotalSponsoredApplicants($event_id) {
        $invoicesIDs = Invoice_fee::where(['event_id' => $event_id])->get('invoice_id');
        $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
        return $users = User::where('user_type_id', 7)->whereIn('id', $usersList)->count();
    }

    public function report($id, $type) {
        if ($type == "all") {
            $invoicesIDs = Invoice_fee::where('event_id', $id)->get('invoice_id');
            $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
            $this->data['users'] = User::whereIn('id', $usersList)->get();
        } else if ($type == "physical") {
            $invoicesIDs = Invoice_fee::where(['event_id' => $id, 'virtual' => 0])->get('invoice_id');
            $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
            $this->data['users'] = User::where('user_type_id', '!=', 7)->whereIn('id', $usersList)->get();
        } else if ($type == "virtual") {
            $invoicesIDs = Invoice_fee::where(['event_id' => $id, 'virtual' => 1])->get('invoice_id');
            $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
            $this->data['users'] = User::where('user_type_id', '!=', 7)->whereIn('id', $usersList)->get();
        } else if ($type == "sponsored") {
            $invoicesIDs = Invoice_fee::where(['event_id' => $id])->get('invoice_id');
            $usersList = InvoiceUser::whereIn('invoice_id', $invoicesIDs)->get('user_id');
            $this->data['users'] = User::where('user_type_id', 7)->whereIn('id', $usersList)->get();
        } else {
            
        }
        if (request('export') == 1) {
            $title = ['name', 'phone number', 'email', 'employer name', 'amount', 'type'];
            $objects = [];
            foreach ($this->data['users'] as $user) {
                $array = ['name' => $user->name,
                    'phone' => $user->phone,
                    'email' => $user->email,
                    'employer' => $user->employer->name,
                    'amount' => $user->payment()->sum('amount'),
                    'type' => $user->usertype->name
                ];
                array_push($objects, $array);
            }
            return $this->exportExcel($objects, $title, 'time' . time());
        }
        return view('user.report', $this->data);
    }

    public function dataTable() {
        $page = request('page');
        switch ($page) {
            case 'users':
                $user_type = request('type') == 0 ? '' : ' AND user_type_id=' . request('type') . ' ';
                $where = strlen($user_type) > 5 ? ' and ' : ' where ';
                $paid_only = request('paid') == 1 ?  ' and users.id in (select user_id from invoices where id in (select invoice_id from payments)) ' : '';
                $sql = 'select RANK () OVER ( ORDER BY users.id ) as row_id , users.id, users.name,users.email, users.phone, employers.name as employer, user_types.name as type, professions.name as specialization, (select count(*) from nametag_printlogs where user_id=users.id) as printed, x.sum from users left join user_types on users.user_type_id=user_types.id left join employers on employers.id=users.employer_id  left join professions on professions.id=users.profession_id  left join (select i.user_id, sum(p.amount) from payments p join invoices i on p.invoice_id=i.id  group by i.user_id)  x on x.user_id=users.id  where users.id in (select user_id from user_events where event_id in (select event_id from events where status=1))  ' . $paid_only . $user_type;
                return $this->ajaxTable('users', ['users.name', 'users.email', 'users.phone', 'employers.name', 'user_types.name', 'professions.name'], $sql);
                break;

            case 'receipt':
                $sql = 'SELECT RANK () OVER ( ORDER BY a.id ) as row_id, a.id,a.code, a.number, d.name, b.created_at,b.method, b.method,c.number as reference from receipts a join payments b on a.payment_id=b.id join invoices c on c.id=b.invoice_id join users d on d.id=c.user_id';
                return $this->ajaxTable('receipt', ['a.code', 'a.number', 'd.name', 'b.created_at', 'b.method', 'c.number'], $sql);
            case 'invoice':
                $sql = 'SELECT RANK () OVER ( ORDER BY id ) as row_id, id, name,number,created_at, amount, payment_for, paid_amount, unpaid_amount,status from invoice_view where true';
                return $this->ajaxTable('invoice_view', ['name', 'number', 'created_at', 'amount', 'payment_for', 'paid_amount', 'unpaid_amount', 'status'], $sql);
                break;

            case 'sms':
                $sql = 'SELECT RANK () OVER ( ORDER BY a.id ) as row_id,a.id ,a.body, b.name, a.phone, a.status, a.created_at from sms a join users b on a.user_id=b.id';
                return $this->ajaxTable('receipt', ['a.body', 'b.name', 'a.phone', 'a.created_at'], $sql);
                break;
            case 'email':
                $sql = 'SELECT RANK () OVER ( ORDER BY a.id ) as row_id,a.id ,a.body, b.name, a.email,a.subject, a.status, a.created_at from emails a join users b on a.user_id=b.id';
                return $this->ajaxTable('receipt', ['a.body', 'b.name', 'a.email', 'a.subject', 'b.phone', 'a.created_at'], $sql);
                break;
            case 'organization':
                $sql = '';
                return $this->ajaxTable('employers', ['a.abbreviation', 'a.name', 'a.created_at', 'a.id'], $sql);
                break;
            case 'payments':
                $filter_by_event = (int) request('event_id') > 0 ? ' and f.id=' . request('event_id') . ' ' : '';
                $sql = 'select a.id, c.name, b.number as reference,a.payment_type as channel, a.amount, a.method, a.transaction_id, a.transaction_time from payments a join invoices b on b.id=a.invoice_id JOIN users c on c.id=b.user_id join user_events d on d.user_id=c.id join events f on f.id=d.event_id where f.status=1 ' . $filter_by_event;
                return $this->ajaxTable('payments', ['c.name', 'a.amount', 'a.id'], $sql);
                break;
            default:
                $sql = 'SELECT RANK () OVER ( ORDER BY a.id ) as row_id,a.id , c.name, b.number, a.amount,d.name as channel, a.method,a.transaction_id,a.mobile_transaction_id,a.account_number, a.created_at::date as created_at, a.status from payments a join invoices b on a.invoice_id=b.id join users c on c.id=b.user_id join financial_entity d on d.id=a.financial_entity_id';
                return $this->ajaxTable('payments', ['a.amount', 'a.method', 'a.currency', 'c.name', 'd.name', 'a.transaction_id', 'a.created_at', 'b.number', 'a.account_number'], $sql);
                break;
        }
    }

}
