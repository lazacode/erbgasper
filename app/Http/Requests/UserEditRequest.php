<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|exists:users',
            'phone'         => 'required|unique:users,phone',
            'gender'        => Rule::in(['male', 'female']),
            'birthday'      => [
                'required',
                'date',
                'before:'. Carbon::now()->subYears(18),
            ],
            'role'          => 'required',
        ];
    }
}
