<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|unique:users',
            'phone'         => 'required|unique:users',
            'gender'        => Rule::in(['male', 'female']),
            'birthday'      => [
                'required',
                'date',
                'before:'. Carbon::now()->subYears(18),
            ],
            'role'          => 'required',
        ];
    }
}
