<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice_fees_payment extends Model
{
   protected $guarded = ['id'];

   public function invoiceFee()
   {
       return $this->belongsTo('\App\Model\Invoice_fee');
   }

   public function payment()
   {
       return $this->belongsTo('\App\Model\Payment');
   }
   
}
