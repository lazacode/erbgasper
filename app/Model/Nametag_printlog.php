<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nametag_printlog extends Model
{
     protected $fillable = [
        'user_id', 'staff_id', 'event_id', 'user_agent'
    ];
}
