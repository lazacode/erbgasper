<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role_permission extends Model {

    public function permission() {
        return $this->belongsTo('\App\Model\Permission');
    }

    public function role() {
        return $this->belongsTo('\App\Model\Role');
    }

}
