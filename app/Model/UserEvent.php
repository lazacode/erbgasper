<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model {

    public $table = 'user_events';
    protected $fillable = ['event_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];

    public function event() {
        return $this->belongsTo('\App\Model\Event');
    }

    public function user() {
        return $this->belongsTo('\App\Model\User');
    }

}
