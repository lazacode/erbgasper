<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission_group extends Model
{
    //
    public function permission() {
        return $this->hasMany('\App\Model\Permission');
    }
}
