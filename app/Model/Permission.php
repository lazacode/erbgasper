<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
   
     public function rolePermission() {
        return $this->hasMany('\App\Model\Role_permission');
    }
}
