<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Zoom extends Model
{
    protected $fillable = ['link','meetingid','password','event_id'];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
