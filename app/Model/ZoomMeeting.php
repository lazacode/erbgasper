<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ZoomMeeting extends Model {

    protected $fillable = ['uuid', 'meeting_id', 'meeting_url', 'topic', 'event_id'];

    public function users() {
        return $this->hasMany('App\Model\UserMeeting');
    }

    public function event() {
        return $this->belongsTo('App\Model\Event');
    }

}
