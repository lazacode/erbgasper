<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserMeeting extends Model
{
    protected $fillable = ['user_id', 'zoom_meeting_id', 'date', 'status'];
    
    public function user() {
         return $this->belongsTo('\App\Model\User');
    }
}
