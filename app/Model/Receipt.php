<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = ['payment_id','code','event_id'];

    public function payment() {
        return $this->belongsTo('\App\Model\Payment');
    }
}
