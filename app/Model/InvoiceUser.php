<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceUser extends Model {

    public $table = 'invoice_users';
    protected $fillable = ['invoice_id', 'user_id', 'created_at', 'updated_at'];

    public function event() {
        return $this->belongsTo('\App\Model\Event');
    }

    public function user() {
        return $this->belongsTo('\App\Model\User');
    }

}
