<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
      protected $fillable = [
        'name', 'amount', 'start_date','end_date','penalty_amount','virtual_amount','virtual_material_amount','event_id'
    ];
      
    public function event() {
        return $this->belongsTo(\App\Model\Event::class);
    }
}
