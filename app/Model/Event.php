<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     protected $fillable = [
        'name', 'location', 'theme','description','status','date','max_attendee','virtual'
    ];

     public function fees()
     {
         return $this->hasOne(Fee::class);
     }
     
     public function userEvent() {
         return $this->hasMany(\App\Model\UserEvent::class);
     }
}
