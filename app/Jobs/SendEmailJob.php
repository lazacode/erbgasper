<?php

namespace App\Jobs;

use App\Mail\SendEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $emailType;
    protected $emailAddress;
    protected $emailBody;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailType, $emailAddress,$emailBody)
    {
        $this->emailAddress = $emailAddress;
        $this->emailType = $emailType;
        $this->emailBody = $emailBody;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendEmail($this->emailType,$this->emailAddress, $this->emailBody);
        Mail::to($this->emailAddress)->send($email);
    }
}
