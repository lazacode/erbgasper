<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use \App\Http\Controllers\BookingController;
use \App\Http\Controllers\ApiController;
use \App\Model\Payment;
use \App\Model\Invoice;
use DB;
use Aws\Common\Aws;
use Aws\Ses\SesClient;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            // \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->call(function () {
            //$this->run();
            $this->sendEmails();
            $this->sendSms();
        })->everyMinute();

        $schedule->call(function () {
            //$this->sendPaymentReminder();
        })->fridays();
    }

    public function run() {
        $this->checkSchedule();
    }

    function pushSMS($message, $phone_number) {
        $api_key = '5e0b7f1a911dd411';
        $secret_key = 'MDI2ZGVlMWExN2NlNzlkYzUyYWE2NTlhOGE0MjgyMDRmMjFlMDFjODkwYjU2NjA4OTY4NzZlY2Y3NGZjY2Y0Yw==';

        $postData = array(
            'source_addr' => 'INFO',
            'encoding' => 0,
            'schedule_time' => '',
            'message' => $message,
            'recipients' => [array('recipient_id' => '1', 'dest_addr' => $phone_number)]
        );

        $Url = 'https://apisms.beem.africa/v1/send';

        $ch = curl_init($Url);
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization:Basic ' . base64_encode("$api_key:$secret_key"),
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        $response = curl_exec($ch);

        if ($response === FALSE) {
            echo $response;

            die(curl_error($ch));
        }
        var_dump($response);
    }

    public function sendSms() {
        $messages = \App\Model\Sms::where('status', 0)->limit(8)->get();
        if (count($messages) > 0) {
            foreach ($messages as $sms) {

                $phone = validate_phone_number($sms->phone);
                $result = is_array($phone) ?
                        (object) json_decode($this->pushSMS($sms->body, $phone[1])) : ['message'=>'Wrong phone number '.$sms->phone];
                $sms->update(['status' => 1,
                    'return_code' => json_encode($result), 'updated_at' => 'now()']);
            }
        }
        return true;
    }

    public function sendEmails() {
        $emails = \App\Model\Email::where('status', 0)->limit(15)->get();

        if (count($emails) > 0) {
            foreach ($emails as $message) {
                if (filter_var($message->email, FILTER_VALIDATE_EMAIL)) {
                    try {
                        $data = ['content' => $message->body, 'link' => 'www.engineersday.co.tz', 'name' => $message->user->name];

                        \Mail::send('email.page', $data, function ($m) use ($message) {
                            $m->from('noreply@lazacode.com', 'Engineers Registration Board -AED2020');
//                            if (preg_match('/Barcode/', $message->subject)) {
//                                $m->bcc('engineersday2017@gmail.com');
//                            }
                            $m->to($message->email)->subject($message->subject);
                        });
                        if (count(\Mail::failures()) > 0) {
                            $message->update(['status' => 0]);
                        } else {
                            $message->update(['status' => 1]);
                        }

                        $message->email == 'inetscompany@gmail.com' ? $message->delete() : '';
                    } catch (\Exception $e) {
                        // error occur\
                        DB::table('emails')->insert(['body' => 'email error' . $e->getMessage(), 'status' => 0, 'email' => 'inetscompany@gmail.com', 'user_id' => 1, 'subject' => 'Occur occurs in ERB payment system']);
                    }
                } else {
                    //skip all invalid emails and update the status=
                    $message->update(['status' => 1]);
                }
            }
        }
        return $this;
    }

    public function checkSchedule() {
        $schedules = \App\Model\Schedule::all();
        foreach ($schedules as $schedule) {
            $days = explode(',', $schedule->days);
            if (in_array(date('l'), $days) && date('H:i') == date('H:i', strtotime($schedule->time))) {
                //execute command
                $template = DB::table('sms_templates')->where('id', $schedule->sms_template_id)->first();
                $numbers = explode(',', $template->phone_numbers);

                //sort by all active events
                $events = DB::select('select * from events where date >CURRENT_DATE ');
                foreach ($events as $event) {

                    $where_payments = \App\Model\Invoice::whereIn('id', \App\Model\Invoice_fee::where('event_id', $event->id)
                                    ->get(['invoice_id']))
                            ->get(['id']);

                    $paid_applicants = \App\Model\User::whereNull('role_id')->where('user_type_id', 9)
                                    ->whereIn('id', \App\Model\Invoice::whereIn('id', \App\Model\Payment::whereIn('invoice_id', $where_payments)->get(['invoice_id']))->get(['user_id']))->count();

                    $patterns = array('/#amount/i', '/#paid_applicants/i', '/#total_applicants/i');

                    $amount = \App\Model\Payment::whereIn('invoice_id', $where_payments)->sum('amount');
                    $replacements = array(
                        $amount, $paid_applicants, (new \App\Http\Controllers\HomeController())->getTotalApplicants($event->id)
                    );
                    $body = preg_replace($patterns, $replacements, $template->message);

                    foreach ($numbers as $number) {
                        DB::table('sms')->insert(array('phone' => $number, 'body' => $body, 'user_id' => 1, 'type' => 1));
                    }
                }
            }
        }
        // return $this->syncInvoice();
    }

    public function sendPaymentReminder() {
        exit;
        $invoices = Invoice::whereNotIn('id', Payment::get(['invoice_id']))->get();
        $setting = \App\Model\Setting::first();
        foreach ($invoices as $invoice) {
            $message = 'You are reminded to make payment for your invoice'
                    . '<ul><li>Reference Number: <b>' . $invoice->number . '</b></li>'
                    . '<li>Payment Methods:'
                    . '     <p>   <br/><b>FOR MOBILE</b><br/>
                                                            Use <b>' . $setting->mno_number . '</b> as the Business number and use ' . $invoice->number . ' as the reference number to make payments in the selected Mobile Company.</p>
                                                                <p><b>FOR BANKS</b>
                                                            <br/>
Use the Reference NUMBER to make payments in the Bank selected, thereafter a confirmation SMS & email will be sent to the mobile number and email you used during the Booking.
<br/>
<b>(You are advised to print this invoice and submit it to the bank along with the appreciate amount)</b>


</p> </li></ul>'
                    . '<p>All payments without this reference number will not be processed</p><br/>Thank You';
            $subject = '';
            $obj = array('body' => $message,
                'subject' => $subject, 'email' => $invoice->user->email, 'user_id' => $invoice->user->id);
            DB::table('emails')->insert($obj);
        }
    }

    public function sendBarcodeTicket() {
        $api = new ApiController();
        $payments = Payment::where('receipt_sent', 0)->get();
        foreach ($payments as $payment) {
            $api->sendBarcodeForm($payment->id);
            $payment->update(['receipt_sent' => 1]);
        }
        return $this;
    }

    function syncInvoice() {
        // $booking = new BookingController();
        //$booking->handle();
        return $this->sendSms();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
