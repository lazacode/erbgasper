<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $emailType;
    protected $email;
    protected $body;
    public function __construct($emailType, $email, $body)
    {
        $this->emailType = $emailType;
        $this->email = $email;
        $this->body = $body;
    }

    /**
     * Build the message$this->subject('EMS LOGIN INFORMATION')
     *
     * @return $this
     */
    public function build()
    {
        if ($this->emailType == "Staff Credential") {
            return $this->subject('User Account')
                ->view('inbox.staff_email')->with(['email'=> $this->email, 'body' => $this->body]);
        } elseif ($this->emailType == "Booking Details") {
            return $this->subject('ERB Booking Details')
                ->view('inbox.staff_email')->with(['email'=> $this->email, 'body' => $this->body]);
        }elseif ($this->emailType == "Payment Accepted") {
            return $this->subject('ERB Payment Accepted - Event Barcode ticket')
                ->view('inbox.staff_email')->with(['email'=> $this->email, 'body' => $this->body]);
        }
    }
}
