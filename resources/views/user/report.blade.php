@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Applicants - <b>REPORTS</b>
            </header>
            <div class="panel-body">
                <div class="container">
                    <div class="row" id="search_checked_table" style="display:none">
                        <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name=""
                           class="btn btn-sm btn-danger link"><i class="fa fa-cloud"></i> Print NameTag (HP Printer)</a>
                        <a type="button" target="_blank" tags='' id="nametag_link2" href="" value="" name=""
                           class="btn btn-sm btn-primary link"><i class="fa fa-cloud"></i> Print single NameTag (EPSON
                            Printer) </a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Number</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="search_checked"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <?php
                $user_type_id = (int) request('user_type') == 0 ? 90 : request('user_type');
                ?>
                <br><br>

                <br/>
                <section id="unseen">

                    <p><a href="<?= url()->current() . '?export=1' ?>" class="btn btn-primary">Export to
                            Excel</a></p>
                    <table class="table table-bordered table-striped table-condensed dataTable"
                           style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>type</th>
                                <th>employer</th>
                                <th>Invoice Amount</th>
                                <th>Paid Amount</th>
                                <th>Action</th>
                                <th>Print Count</th>
                                <th><input type='checkbox' name='all' id='toggle_all'/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($users as $user)
                            <tr id="row<?= $user->id ?>">
                                <td>{{ $i++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->usertype->name }}</td>
                                <td>{{ $user->employer->name }}</td>
                                <td data-title="">
                                    <?php
                                    $invoice = $user->invoice()->where('active', 1)->first();
                                    if (!empty($invoice)) {
                                        $am = $invoice->invoiceFee()->sum('amount');
                                    } else {
                                        $am = 0;
                                    }
                                    echo money($am);
                                    ?>
                                </td>
                                <td>{{ $user->payment()->sum('amount') }}</td>
                                <td>
                                    <a href="{{ url('user/profile/'.$user->id) }} " class="btn btn-xs btn-primary">View</a>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('{{ $user->id }}','{{ url('/setting/'.$user->id.'/edit') }}')" class="btn btn-xs btn-info">Edit</a>

                                    <?php
                                    $invU = \App\Model\InvoiceUser::where('user_id', $user->id)->get('invoice_id');
                                    $payU = \App\Model\Payment::whereIn('invoice_id', $invU)->get('invoice_id');
                                    $tes = \App\Model\Invoice_fee::where('virtual', 0)->whereIn('invoice_id', $payU)->get();
                                    ?>
                                    @if($user->user_type_id != '7' AND $user->user_type_id != '120')
                                    @if(count($tes) > 0)
                                    <a href="{{url('user/ticket/'.$user->id)}}" class="btn btn-xs btn-warning">Barcode</a>
                                    <a href="{{ url('user/nametag/'.$user->id) }}" class="btn btn-xs btn-default">Tag</a>
                                    @endif
                                    @endif

                                </td>
                                <td>{{ \App\Model\Nametag_printlog::where('user_id', $user->id)->count() }}</td>
                                <td>
                                    <input type="checkbox" class="check" name="select[]" value="{{$user->id}}"/>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>type</th>
                                <th>employer</th>
                                <th>Amount</th>
                                <th>Action</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </section>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post"
                              action="<?= url('invoice') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                    <h4 class="modal-title" id="title_page">Add New User</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="employer_id" id="employer_id">
                                                    <option value=""></option>
                                                    <?php $userype = \App\Model\Employer::orderBy('name')->get() ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach
                                                    ;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'employer_id'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Name
                                                (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="name" name="name" minlength="2"
                                                       type="text" required="" value="{{old('name')}}"
                                                       pattern="[a-zA-Z\. ]{5,}"
                                                       onblur="this.value = this.value.toUpperCase()">
                                            </div>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="cemail" class="control-label col-lg-3">E-Mail
                                                (required)</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="email" type="email" name="email"
                                                       required="" value="<?= old('email') ?>"
                                                       onblur="this.value = this.value.toLowerCase()">
                                            </div>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="phone" type="text" name="phone"
                                                       value="<?= old('phone') ?>">
                                            </div>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3">Specialization
                                                (Required)</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="profession_id"
                                                        id="profession_id">
                                                    <option value=""></option>
                                                    <?php $professions = App\Model\Profession::where('invitee', 0)->orderBy('name')->get(); ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach
                                                    ;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>


                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="user"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close
                                    </button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Jquery datatable js -->
<script type="text/javascript" src="{{ asset('public/datatables/jquery.dataTables.min.js')}}"></script>
<!-- Datatable js -->
<script type="text/javascript" src="{{ asset('public/datatables/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/jszip.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('public/datatables/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.html5.min.js')}}"></script>


<link href="{{ asset('public/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
<!-- page end-->
<script type="text/javascript">
                                                           $('.dataTable').DataTable();
                                                           $(document).ready(function () {
                                                           var table = $('#applicant_ajax').DataTable({
                                                           "processing": true,
                                                                   "serverSide": true,
                                                                   'serverMethod': 'post',
                                                                   'ajax': {
                                                                   'url': "<?= url('ajaxTable/?page=users&paid=' . request('paid') . '&type=' . request('user_type')) ?>"
                                                                   },
                                                                   "columns": [
                                                                   {"data": "row_id"},
                                                                   {"data": "name"},
                                                                   {"data": "phone"},
                                                                   {"data": "email"},
                                                                   {"data": "type"},
                                                                   {"data": "employer"},
                                                                   {"data": "specialization"},
                                                                   {"data": ""},
                                                                   {"data": "printed"},
                                                                   {"data": ""}
                                                                   ],
                                                                   "columnDefs": [{
                                                                   "targets": 7,
                                                                           "data": null,
                                                                           "render": function (data, type, row, meta) {
                                                                           var edit = '';
                                                                           var view = '<a href="<?= url('user/profile/') ?>/' + row.id + '" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;';
<?php if (can_access('edit_users')) { ?>
                                                                               var edit_url = '<?= url('/setting') ?>/' + row.id + '/edit';
                                                                               edit = '<a data-toggle="modal" href="#myModal" onmousedown="open_edit_model(\'' + row.id + '\', \'' + edit_url + '\')" class="btn btn-xs btn-info">Edit</a> ';
<?php } ?>

                                                                           var barcode = row.sum > 0 ? ' <a href="<?= url('user/ticket/') ?>/' + row.id + '" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;' : '';
                                                                           var tag = row.sum > 0 ? '<a href="<?= url('user/nametag/') ?>/' + row.id + '" class="btn btn-xs btn-default">Tag</a>' : '';
                                                                           return view + edit + barcode + tag;
                                                                           }

                                                                   },
                                                                   {
                                                                   "targets": 9,
                                                                           "data": null,
                                                                           "render": function (data, type, row, meta) {
                                                                           return '<input type="checkbox" class="check" name="select[]" value="' + row.id + '"/>';
                                                                           }

                                                                   }],
                                                                   rowCallback: function (row, data) {
                                                                   //$(row).addClass('selectRow');
                                                                   $(row).attr('id', 'row' + data.id);
                                                                   }

                                                           });
                                                           $('#applicant_ajax tbody').on('click', '.check', function () {
                                                           var value = $(this).val();
                                                           var status = $(this).is(':checked');
                                                           if (status === true) {
                                                           var text = $('#row' + value).html();
                                                           $('#search_checked_table').show();
                                                           $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                                                           var ex = $('#nametag_link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var param = ex.split(",");
                                                           param.push(value);
                                                           $('#nametag_link').attr('tags', param.join(","));
                                                           $('#nametag_link').attr('href', url + param.join(","));
                                                           var ex = $('.link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                                                           var param = ex.split(",");
                                                           param.push(value);
                                                           $('#nametag_link').attr('tags', param.join(","));
                                                           $('#nametag_link').attr('href', url + param.join(","));
                                                           $('#nametag_link2').attr('tags', param.join(","));
                                                           $('#nametag_link2').attr('href', url2 + param.join(","));
                                                           console.log(param);
                                                           } else {
                                                           var ex = $('#nametag_link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var param = ex.split(",");
                                                           param = jQuery.grep(param, function (val) {
                                                           return val != value;
                                                           });
                                                           var arr = param;
                                                           var result = arr.filter(function (elem) {
                                                           return elem != value;
                                                           });
                                                           console.log(result);
                                                           var ex = $('.link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                                                           var param = ex.split(",");
                                                           param.push(value);
                                                           $('#nametag_link').attr('tags', param.join(","));
                                                           $('#nametag_link').attr('href', url + param.join(","));
                                                           $('#nametag_link2').attr('tags', param.join(","));
                                                           $('#nametag_link2').attr('href', url2 + param.join(","));
                                                           $('#nametag_link').attr('tags', result.join(","));
                                                           $('#nametag_link').attr('href', url + result.join(","));
                                                           $('#s_table' + value).remove();
                                                           }
                                                           });
                                                           });
                                                           ajax_barcode_search = function () {
                                                           document.addEventListener('keydown', function (event) {
                                                           if (event.keyCode == 17 || event.keyCode == 74)
                                                                   event.preventDefault();
                                                           });
                                                           $('#search_input_tag').keyup(function (e) {
                                                           if (e.keyCode == 13) {
                                                           var pasteData = $(this).val();
                                                           $.ajax({
                                                           type: 'GET',
                                                                   url: "<?= url('find') ?>",
                                                                   data: {
                                                                   "s": pasteData,
                                                                           'type': 1,
                                                                           user_id_tags: $('#user_id_tags').attr('content')
                                                                   },
                                                                   dataType: "html ",
                                                                   beforeSend: function (xhr) {
                                                                   $('#search_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                                                                   },
                                                                   complete: function (xhr, status) {
                                                                   $('#search_input_tag').val('');
                                                                   $('#search_loader').html('');
                                                                   },
                                                                   success: function (data) {
                                                                   var val = $('#search_page_result').text();
                                                                   if (val == '') {
                                                                   $('#search_page_result').html(data);
                                                                   } else {
                                                                   $('#search_page_result').html(data);
                                                                   }
                                                                   }
                                                           });
                                                           }
                                                           });
                                                           }

                                                           $(document).ready(ajax_barcode_search);
                                                           open_edit_model = function (a, b) {
                                                           $.ajax({
                                                           type: 'POST',
                                                                   url: "<?= url('setting/getedit') ?>",
                                                                   data: {
                                                                   "id": a,
                                                                           "table": "user"
                                                                   },
                                                                   dataType: "json",
                                                                   success: function (data) {
                                                                   $('#title_page').html('Edit user type');
                                                                   $('#commentForm').attr('action', b);
                                                                   $("#commentForm").attr("method", "get");
                                                                   $.each(data, function (i, item) {
                                                                   $('#' + i).val(item);
                                                                   });
                                                                   }
                                                           });
                                                           }
                                                           reset_form = function () {
                                                           $('#title_page').html('Add New User Type');
                                                           $('#commentForm').attr('action', '<?= url('user') ?>');
                                                           $("#commentForm").attr("method", "post");
                                                           $("input:not(:hidden)").val('');
                                                           $('.delete').html('Delete');
                                                           }
                                                           sort_user = function () {
                                                           $('#sort_user').change(function () {
                                                           var type = $(this).val();
                                                           window.location.href = '<?= url()->current() ?>/?user_type=' + type;
                                                           });
                                                           }
                                                           sort_event = function () {
                                                           $('#sort_event').change(function () {
                                                           var type = $(this).val();
                                                           var currentUrl = '<?= url()->full() ?>';
                                                           var url = new URL(currentUrl);
                                                           url.searchParams.set("event", type); // setting your param
                                                           var newUrl = url.href;
                                                           window.location.href = newUrl;
                                                           });
                                                           }

                                                           search_checked = function () {
                                                           $('.check').click(function () {
                                                           var value = $(this).val();
                                                           var status = $(this).is(':checked');
                                                           if (status === true) {
                                                           var text = $('#row' + value).html();
                                                           $('#search_checked_table').show();
                                                           $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                                                           var ex = $('.link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                                                           var param = ex.split(",");
                                                           param.push(value);
                                                           $('#nametag_link').attr('tags', param.join(","));
                                                           $('#nametag_link').attr('href', url + param.join(","));
                                                           $('#nametag_link2').attr('tags', param.join(","));
                                                           $('#nametag_link2').attr('href', url2 + param.join(","));
                                                           console.log(param);
                                                           } else {
                                                           var ex = $('.link').attr('tags');
                                                           var url = '<?= url('user/bulknametag?ids=') ?>';
                                                           var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                                                           var param = ex.split(",");
                                                           param = jQuery.grep(param, function (val) {
                                                           return val != value;
                                                           });
                                                           var arr = param;
                                                           var result = arr.filter(function (elem) {
                                                           return elem != value;
                                                           });
                                                           console.log(result);
                                                           $('#nametag_link').attr('tags', result.join(","));
                                                           $('#nametag_link').attr('href', url + result.join(","));
                                                           $('#nametag_link2').attr('tags', param.join(","));
                                                           $('#nametag_link2').attr('href', url2 + param.join(","));
                                                           $('#s_table' + value).remove();
                                                           }
                                                           });
                                                           }
                                                           toggle_all = function () {
                                                           $('#toggle_all').click(function () {
                                                           var status = $(this).is(':checked');
                                                           if ($("#toggle_all").prop('checked')) {
                                                           $('.check').prop("checked", true);
                                                           } else {
                                                           $('.check').prop("checked", false);
                                                           }
                                                           if (status === true) {
                                                           //select all

                                                           $.ajax({
                                                           type: 'GET',
                                                                   url: "<?= url('user/getApplicants') ?>",
                                                                   data: {
                                                                   "type": '<?= request('type') ?>',
                                                                   },
                                                                   dataType: "html",
                                                                   success: function (data) {
                                                                   console.log(data);
                                                                   $('#search_checked_table').show();
                                                                   var ex = data;
                                                                   var url = '<?= url('user/bulknametag?ids=') ?>';
                                                                   var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                                                                   var param = ex.split(",");
                                                                   $('#nametag_link').attr('tags', param.join(","));
                                                                   $('#nametag_link').attr('href', url + param.join(","));
                                                                   $('#nametag_link2').attr('tags', param.join(","));
                                                                   $('#nametag_link2').attr('href', url2 + param.join(","));
                                                                   console.log(param);
                                                                   }
                                                           });
                                                           } else {
                                                           //diselect all
                                                           $('#nametag_link').attr('tags', '');
                                                           $('#nametag_link').attr('href', '');
                                                           $('#nametag_link2').attr('tags', '');
                                                           $('#nametag_link2').attr('href', '');
                                                           $('#search_checked_table').hide();
                                                           }
                                                           });
                                                           };
                                                           //    $('#search_input_tag2').on('paste', function (e) {
                                                           //        var pasteData = e.originalEvent.clipboardData.getData('text');
                                                           //        $.ajax({
                                                           //            type: 'GET',
                                                           //            url: "<?= url('find') ?>",
                                                           //            data: {
                                                           //                "s": pasteData,
                                                           //                'type': 1,
                                                           //                user_id_tags: $('#user_id_tags').attr('content')
                                                           //            },
                                                           //            dataType: "html ",
                                                           //            beforeSend: function (xhr) {
                                                           //                $('#search_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                                                           //            },
                                                           //            complete: function (xhr, status) {
                                                           //                $('#search_input_tag').val('');
                                                           //                $('#search_loader').html('');
                                                           //            },
                                                           //            success: function (data) {
                                                           //                var val = $('#search_page_result').text();
                                                           //                if (val == '') {
                                                           //                    $('#search_page_result').html(data);
                                                           //                } else {
                                                           //                    $('#search_page_result').html(data);
                                                           //                }
                                                           //            }});
                                                           //    });
                                                           $(document).ready(toggle_all);
                                                           $(document).ready(search_checked);
                                                           $(document).ready(sort_user);
                                                           $(document).ready(sort_event);
</script>
@endsection