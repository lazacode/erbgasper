@extends('layouts.app')

@section('content')
<!-- page start-->
<?php $url=url('public/jtable').'/'; ?>
<link href="<?=$url?>themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
<link href="<?=$url?>scripts/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />

<script src="<?=$url?>scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="<?=$url?>scripts/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<script src="<?=$url?>Scripts/jtable/jquery.jtable.js" type="text/javascript"></script>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Organizations

            </header>
        <div id="PeopleTableContainer" style="width: 600px;"></div>

        </section>
    </div>
</div>

<!-- page end-->
<script type="text/javascript">

    $(document).ready(function () {

        //Prepare jTable
        $('#PeopleTableContainer').jtable({
            title: 'Table of people',
            paging: true,
             pageSize: 10,
            sorting: true,
            defaultSorting: 'Name ASC',
            actions: {
                listAction: '<?=url('user/getOrganization')?>?action=list',
//                createAction: 'PersonActionsPagedSorted.php?action=create',
//                updateAction: 'PersonActionsPagedSorted.php?action=update',
//                deleteAction: 'PersonActionsPagedSorted.php?action=delete'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'name',
                    width: '40%'
                },
                abbreviation: {
                    title: 'abbreviation',
                    width: '20%'
                },
                created_at: {
                    title: 'created_at',
                    width: '30%',
                    type: 'date',
                    create: false,
                    edit: false
                }
            }
        });

        //Load person list from server
        $('#PeopleTableContainer').jtable('load');
$('.jtable').addClass('dataTable');
    });

</script>
@endsection