@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Organizations
            
            </header>
            <?php if (can_access('add_users')) { ?>
             <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModalEmployer" onmousedown="reset_form()">
                    Add  New Entity
                </a></p>
            <?php }?>
            <div class="panel-body">
                <section id="unseen">
                    <table  id="organization_ajax_table" class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Abbreviation</th>
                                <th class="numeric">Office</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                       
                    </table>
                </section>
            </div>
        </section>
    </div>
</div>
 <div class="modal fade" id="myModalEmployer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?=url('user')?>">

                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add New Entity</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <div class=" form">
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Abbreviation</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="abbreviation"  type="text" required=""  onblur="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                    <div class="form-group " style="display: none;">
                                        <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                        <div class="col-lg-6">
                                            <input class="form-control " id="cemail" type="email" name="email" required="" value="<?=time().'@erbtesting.or.tz'?>"  onblur="this.value = this.value.toLowerCase()">
                                        </div>
                                    </div>
                   <!--                    <div class="form-group ">
                                        <label for="curl" class="control-label col-lg-3">Phone (required)</label>
                                        <div class="col-lg-6">
                                            <input class="form-control " id="curl" type="text" name="phone">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="ccomment" class="control-label col-lg-3">Location(required)</label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control " id="ccomment" name="location" required=""></textarea>
                                        </div>
                                    </div>-->

                                </div>

                            </div>


                        </div>
                        <div class="modal-footer">
                           <?= csrf_field() ?>
                            <input type="hidden" name="user" value="employer"/>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
<!-- page end-->
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#organization_ajax_table').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('ajaxTable/?page=organization&type=sms') ?>"
            },
            "columns": [
                {"data": "id"},
                {"data": "abbreviation"},
                {"data": "name"},
                {"data": ""}
            ],
            "columnDefs": [{
                    "targets": 3,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        var del = '';
                        var resend = '';
<?php if (can_access('delete_settings')) { ?>

                            del = '<form method="POST" action="<?= url('user/') ?>/' + row.id + '" accept-charset="UTF-8" class=""><?= csrf_field() ?><input name="_method" type="hidden" value="DELETE"><input name="type" type="hidden" value="organization"><input class="btn btn-xs btn-danger" type="submit" value="Delete"></form>';
<?php } ?>

                        return  del;
                    }

                }],
            rowCallback: function (row, data) {

                //$(row).addClass('selectRow');
                $(row).attr('id', 'row' + data.id);
            }
        });

    });
    </script>
@endsection