@extends('layouts.app')
@section('content')

<?php if (can_access('view_dashboard')) { ?>
    <!--main content start-->
    <!--mini statistics start-->
    <script src="https://code.highcharts.com/highcharts.js"></script>

    <div class="row">
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon orange"><i class="fa fa-users"></i></span>
                <div class="mini-stat-info">
                    <span><?= $total_users ?></span>
                    Total Users

                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
                <div class="mini-stat-info">
                    <span><?= $organizations ?></span>
                    Organizations
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
                <div class="mini-stat-info">
                    <span><?= $events ?></span>
                    Active Events 
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon green"><i class="fa fa-users"></i></span>
                <div class="mini-stat-info">
                    <span><?= $erb_staff ?></span>
                    ERB staff
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">

        </div>


    </div>

    <!--mini statistics end-->

    <div class="row">
        <!--   <div class="col-md-3">
               <section class="panel">
                   <div class="panel-body">
                       <div class="top-stats-panel">
                           <div class="daily-visit">
                               <h4 class="widget-h">Total Invoices</h4>
                               <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                                   <br/><br/>
                               {{$total_invoices}}
                           </div>
                              <ul class="chart-meta clearfix">
                                   <li class="pull-left visit-chart-value"></li>
                                   <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('invoice')}}">View</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </section>
           </div>
           <div class="col-md-3">
               <section class="panel">
                   <div class="panel-body">
                       <div class="top-stats-panel">
                           <div class="daily-visit">
                               <h4 class="widget-h">
                                   Amount to be Collected</h4>
                               <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                                   <br/><br/>
                                 Tsh  {{number_format($amount_tobe_collected)}}
                               </div>
                               <ul class="chart-meta clearfix">
                                   <li class="pull-left visit-chart-value"></li>
                                   <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('invoice')}}">View</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </section>
           </div>
           <div class="col-md-3">
               <section class="panel">
                   <div class="panel-body">
                       <div class="top-stats-panel">
                           <div class="daily-visit">
                           <h4 class="widget-h">Amount Collected</h4>
                           <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                               <br/><br/>
                              Tsh {{number_format($total_money_collected)}}
                           </div>
                           <ul class="chart-meta clearfix">
                               <li class="pull-left visit-chart-value"></li>
                               <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('payment')}}">View</a></li>
                           </ul>
                           </div>
                       </div>
                   </div>
               </section>
           </div>-->
        <div class="col-md-9">

            <div id="eventtypes"></div>
            <!--mini statistics end-->
            <br/>
            <h3>Other Reports Based on Events</h3>
            @foreach($allevents as $event)
            <div class="row">
                <div class="col-md-12">
                    <h6>{{ $event->name }}</h6>
                </div>

            </div>
            <div class="row">
                <a href="{{ url('report/'.$event->id.'/all') }}">
                    <div class="col-md-3">
                        <div class="mini-stat clearfix">
                            <span class="mini-stat-icon orange"><i class="fa fa-align-justify"></i></span>
                            <div class="mini-stat-info">
                                <span>{{ (new \App\Http\Controllers\HomeController())->getTotalApplicants($event->id) }}</span>
                                Total Applicants
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ url('report/'.$event->id.'/physical') }}">
                    <div class="col-md-3">
                        <div class="mini-stat clearfix">
                            <span class="mini-stat-icon tar"><i class="fa fa-user"></i></span>
                            <div class="mini-stat-info">
                                <span>{{ (new \App\Http\Controllers\HomeController())->getTotalPhysicalApplicants($event->id) }}</span>
                                Physical Applicants
                            </div>
                        </div>
                    </div></a>
                <a href="{{ url('report/'.$event->id.'/virtual') }}">
                    <div class="col-md-3">
                        <div class="mini-stat clearfix">
                            <span class="mini-stat-icon orange"><i class="fa fa-users"></i></span>
                            <div class="mini-stat-info">
                                <span>{{ (new \App\Http\Controllers\HomeController())->getTotalVirtualApplicants($event->id) }}</span>
                                Virtual Applicants
                            </div>

                        </div>
                    </div></a>
                <a href="{{ url('report/'.$event->id.'/sponsored') }}">
                    <div class="col-md-3">
                        <div class="mini-stat clearfix">
                            <span class="mini-stat-icon tar"><i class="fa fa-user"></i></span>
                            <div class="mini-stat-info">
                                <span>{{ (new \App\Http\Controllers\HomeController())->getTotalSponsoredApplicants($event->id) }}</span>
                                Sponsored Applicants
                            </div>
                        </div>
                    </div></a>
            </div>
            @endforeach
        </div>

        <div class="col-md-3">
            <section class="panel">
                <div class="panel-body">
                    <div class="top-stats-panel">
                        <h4 class="widget-h">Pending & Tasks</h4>
                        <div class="bar-stats">
                            <h4>Pending</h4>
                            <ul class="bar-legend">
                                <li><span class="bar-legend-pointer pink"></span> SMS : {{$sms_pending}}</li>
                                <li><span class="bar-legend-pointer green"></span> Emails : {{$email_pending}}</li>
                            </ul>
                            <br/>
                            <div class="daily-visit">
                                <div class="daily-sales-info">
                                    <span class="sales-count">{{$sms-$sms_pending}} </span> 
                                    <span class="sales-label">SMS Sent,</span>
                                    <span class="sales-label"><span class="sales-count">
                                            {{isset($sms_status->sms_remain) ?$sms_status->sms_remain:''}}</span> SMS Remains</span>
                                </div>
                                <br/>
                                <div class="daily-sales-info">
                                    <span class="sales-count">{{$email-$email_pending}} </span> 
                                    <span class="sales-label">Emails Sent</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
    <script>

    Highcharts.chart('eventtypes', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Number of Users Per Event'
    },
    subtitle: {
        text: 'Source: engineersday.co.tz'
    },
    xAxis: {
        categories: [
            ' Total Applicants',
            ' Physical Applicants',
            ' Virtual Applicants',
            ' Sponsored Applicants'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Users'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} users</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    <?php
    foreach ($allevents as $event) {
        ?>
            {
                name: '<?= $event->name ?>',
                data: [
        <?= (new \App\Http\Controllers\HomeController())->getTotalApplicants($event->id) ?>,
        <?= (new \App\Http\Controllers\HomeController())->getTotalPhysicalApplicants($event->id) ?>,
        <?= (new \App\Http\Controllers\HomeController())->getTotalVirtualApplicants($event->id) ?>,
        <?= (new \App\Http\Controllers\HomeController())->getTotalSponsoredApplicants($event->id) ?>
                ]

            },
    <?php } ?>

    ]
    });

    </script>
    <!--right sidebar end-->
<?php } ?>
<!--mini statistics end-->
@endsection
