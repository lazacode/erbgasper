@extends('layouts.app')

@section('content')

<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Zoom Links
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                        <tr>
                            <th>Meeting ID</th>
                            <th>Topic</th>
                            <th>Meeting Link</th>
                            <th>Actions</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($zoomlinks as $zoom)
                            <tr>
                                <td>{{$zoom->meeting_id}}</td>
                                <td>{{$zoom->topic}}</td>

                                <td>
                                    <a href="{{ $zoom->meeting_url }}"> {{ $zoom->meeting_url }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('meeting.show',$zoom->meeting_id) }}" class="btn btn-xs btn-info">MORE DETAILS</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Meeting Participants</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <table class="table table-bordered">
                                            <th width="40%">USERNAME</th>
                                            <th width="50%">EMAIL</th>
                                            <th width="10%">STATUS</th>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user_type"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    console.log(i);
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
</script>
@endsection