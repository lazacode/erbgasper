@extends('layouts.app')
@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Name Tags Printing Reports
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                  
                </span>
            </header>

            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th class="numeric">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($reports as $report)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$report->created_at}}</td>
                                <td class="numeric">{{$report->count}}</td>

                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>

            </div>
    </div>
</section>
</div>
</div>
<!-- page end-->
@endsection