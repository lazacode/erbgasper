@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Configuration Settings

            </header>
            <div class="panel-body">

                <div class=" form">
                    <?php
                    if (session('return_message')) {
                        ?>
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <?= session('return_message') ?>

                        </div>
                    <?php } ?>
                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="{{url('setting')}}">
                        <div class="form-group ">
                            <label for="cname" class="control-label col-lg-3">Institution Name (required)</label>
                            <div class="col-lg-6">
                                <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="" value="{{old('name',!empty($setting) ? $setting->name :'')}}">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="cphone" class="control-label col-lg-3">Phone (required)</label>
                            <div class="col-lg-6">
                                <input class=" form-control" id="cphone" name="phone" minlength="2" type="text" required="" value="{{old('phone',!empty($setting) ? $setting->phone :'')}}">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                            <div class="col-lg-6">
                                <input class="form-control " id="cemail" type="email" name="email" required="" value="{{old('email',!empty($setting) ? $setting->email :'')}}">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="curl" class="control-label col-lg-3">Website (optional)</label>
                            <div class="col-lg-6">
                                <input class="form-control " id="curl" type="url" name="website" value="{{old('website',!empty($setting) ? $setting->website :'')}}">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="cbox" class="control-label col-lg-3">P.o Box (required)</label>
                            <div class="col-lg-6">
                                <input class=" form-control" id="cbox" name="box" minlength="2" type="text" required="" value="{{old('box',!empty($setting) ? $setting->box :'')}}">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="caddress" class="control-label col-lg-3">Address (required)</label>
                            <div class="col-lg-6">
                                <textarea class="form-control " id="caddress" name="address" required="">{{!empty($setting) ? $setting->address :''}}</textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="caddress" class="control-label col-lg-3">Site Mode</label>
                            <div class="col-lg-6">
                                <select name="site_mode" class="form-control" id="site_mode">
                                    <option value="1" <?= $setting->site_mode == 1 ? 'selected' : '' ?>>Active</option>
                                    <option value="2" <?= $setting->site_mode == 2 ? 'selected' : '' ?>>Not Active</option>
                                    <option value="3" <?= $setting->site_mode == 3 ? 'selected' : '' ?>>Certificate</option>
                                    <option value="4" <?= $setting->site_mode == 4 ? 'selected' : '' ?>>Disabled</option>

                                </select>
                            </div>
                        </div>
                           <div class="form-group ">
                            <label for="caddress" class="control-label col-lg-3">Bulk Booking Option</label>
                            <div class="col-lg-6">
                                <select name="bulk_booking" class="form-control" id="site_mode">
                                    <option value="1" <?= $setting->bulk_booking == 1 ? 'selected' : '' ?>>Enabled</option>
                                    <option value="2" <?= $setting->bulk_booking == 2 ? 'selected' : '' ?>>Disabled</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group " <?= $setting->site_mode == 4 ? '' : 'style="display: none"' ?>  id="site_mode_comment">
                            <label for="cbox" class="control-label col-lg-3">Disabled Reason</label>
                            <div class="col-lg-6">
                                <textarea class=" form-control calender" id="cbox" name="comment" minlength="2" type="date"  novalidate="true" value="">{{old('comment',!empty($setting) && $setting->site_mode == 4  ? $setting->comment :'')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group " >
                            <label for="cbox" class="control-label col-lg-3">Control Number Deadline</label>
                            <div class="col-lg-6">
                                <input class=" form-control calender" id="cbox" name="payment_deadline" minlength="2" type="date" required="" value="{{old('payment_deadline',!empty($setting) ? $setting->payment_deadline :'')}}">
                            </div>
                        </div>
                        <div class="form-group " <?= $setting->site_mode == 2 ? 'style="display: box"' : 'style="display: none"' ?> id="inactive_message_box">
                            <label for="inactive_mode" class="control-label col-lg-3">In Active Message</label>
                            <div class="col-lg-6">
                                <textarea class=" form-control" id="inactive_message" name="inactive_message" minlength="2"  value="{{old('inactive_message',!empty($setting) ? $setting->inactive_message :'')}}"></textarea>
                            </div>
                        </div>
                        <!-- <div class="form-group " >
                            <label for="cbox" class="control-label col-lg-3">Virtual Maximum number of attendee </label>
                            <div class="col-lg-6">
                                <input class=" form-control calender" id="max_attendee" name="max_attendee" minlength="2" type="number" required="" value="{{old('max_attendee',!empty($setting)? $setting->max_attendee :'')}}">
                            </div>
                        </div> -->
                        <?php if (can_access('edit_settings')) { ?>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                        <?php } ?>
                        <?= csrf_field() ?>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/invalid-origin/tinymce/5.4.2-90/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">

wywg = function () {
    tinymce.init({
        selector: "#inactive_message",
        //theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        images_upload_url: '<?= url('upload.php') ?>',
        //images_upload_credentials: true,
        images_upload_base_path: '/storage/images/',
        images_upload_handler: function (blobInfo, success, failure, progress) {
            var xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;

            xhr.open('POST', '<?= url('tinymce.php') ?>');

            xhr.upload.onprogress = function (e) {
                progress(e.loaded / e.total * 100);
            };

            xhr.onload = function () {
                var json;

                if (xhr.status < 200 || xhr.status >= 300) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                console.log(xhr);
                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success('<?= url('/') ?>/' + json.location);
            };

            xhr.onerror = function () {
                failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());

            xhr.send(formData);
        }

    });

}

$(document).ready(wywg);
content_for = function () {
    $('#permission_group').change(function () {
        var group_id = $(this).val();
        if (group_id === '0') {
            $('#content_for').val(0);
        } else {
            $.ajax({
                type: 'get',
                url: "<?= url('customer/getPermission/null') ?>",
                data: "group_id=" + group_id,
                dataType: "html",
                success: function (data) {
                    $('#content_for').html(data);
                }
            });
        }
    });
}
$(document).ready(content_for);
</script>
<script type="text/javascript">
    // $('#commentForm input,#commentForm select, #commentForm textarea').prop('disabled', true);
    site_mode = function () {
        $('#site_mode').change(function () {
            var val = $(this).val();
            if (val == 4) {
                $('#site_mode_comment').show();
            } else if (val == 2) {
                $('#inactive_message_box').show();
            } else {
                $('#site_mode_comment').hide();
            }
        });
    }
    $(document).ready(site_mode);
</script>

@endsection