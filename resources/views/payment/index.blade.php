@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Payments Report
                <a href="<?= url('payment/export') ?>" class="btn btn-primary pull-right">Export to Excel</a>
                <br><br>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <form action="">
                        <div class="form-group">
                            <label for="sortEvent">FILTER APPLICANTS BY EVENT(s)</label>
                            <select name="event" id="sort_event" class="form-control">
                                <option></option>
                                <option value="">All Active Events</option>
                                @foreach($events as $event)
                                <option value="{{$event->id}}">{{ $event->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <p></p>
                <table class="table table-bordered table-striped table-condensed" id="payment_ajax_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th>Invoice No</th>
                            <th>Paid Amount</th>
                            <th>Method</th>
                            <th>Channel</th>
                            <th>Transaction ID</th>
                            <th>Time</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th>Invoice No</th>
                            <th>Paid Amount</th>
                            <th>Method</th>
                            <th>Channel</th>
                            <th>Transaction ID</th>
                            <th>Time</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#payment_ajax_table').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('ajaxTable/?page=payments&type=payments&event_id='.$event_id) ?>"
            },
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "reference"},
                {"data": "amount"},
                {"data": "method"},
                {"data": "channel"},
                {"data": "transaction_id"},
                {"data": "transaction_time"},
                {"data": ""}
            ],
            "columnDefs": [{
                    "targets": 8,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        var del = '';
                       var status = '<span  class="badge  badge-success">paid</span>';
                        return status;
                    }

                }],
            rowCallback: function (row, data) {

                //$(row).addClass('selectRow');
                $(row).attr('id', 'row' + data.id);
            }
        });

    });
    sort_event = function () {
        $('#sort_event').change(function () {
            var type = $(this).val();
            var currentUrl = '<?= url()->full() ?>';
            var url = new URL(currentUrl);
            url.searchParams.set("event", type); // setting your param
            var newUrl = url.href;
            window.location.href = newUrl;
        });
    }
    $(document).ready(sort_event);
</script>
@endsection