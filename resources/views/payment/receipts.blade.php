@extends('layouts.app')

@section('content')
<!-- page start-->
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Payment Receipts

            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <form action="">
                        <div class="form-group">
                            <label for="sortEvent">FILTER APPLICANTS BY EVENT(s)</label>
                            <select name="event" id="sort_event" class="form-control">
                                <option></option>
                                <option value="">All Active Events</option>
                                @foreach($events as $event)
                                <option value="{{$event->id}}">{{ $event->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <?php if ((int) request('use') == 0) { ?>
                    <div class="position-center">
    <!--                        <form class="form-inline" role="form" action="<?= url('invoice/bulk') ?>" method="get">
                            <div class="form-group">
                                <label class="from" for="From">From Date</label>
                                <input type="date" class="form-control" id="from" name="from" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label class="to" for="To">To Date</label>
                                <input type="date" class="form-control" id="to" name="to" placeholder="" value="">
                            </div>

                            <button type="submit" class="btn btn-success">Search</button>
                        </form>-->

                    </div>
                    <p></p>
                    <section id="unseen">
                        <table id="receipt_ajax" class="table table-bordered table-striped table-condensed dataTable" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="numeric">Invoice Number</th>
                                    <th>Payer Name</th>
                                    <th class="numeric">Payment Date</th>
                                    <th>Payment Method</th>
                                    <th class="numeric">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($receipts as $receipt)
                                <tr>
                                    <th>{{ $i++ }}</th>
                                    <td>{{ $receipt->invoice->number }}</td>
                                    <td>{{ $receipt->invoice->user->name }}</td>
                                    <td>{{ $receipt->transaction_time }}</td>
                                    <td>{{ $receipt->method }}</td>
                                    <td>
                                        <a href="{{ url('payment/receipts/?use='.$receipt->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-file-o"></i> Receipt</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th class="numeric">Invoice Number</th>
                                    <th>Payer Name</th>
                                    <th class="numeric">Payment Date</th>
                                    <th>Payment Method</th>
                                    <th class="numeric">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </section>

                <?php } else { ?>
                    @include('payment.receipt_template')
                <?php } ?>

            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    $('#sortEvent').change(function () {
        var type = $(this).val();
        window.location.href = '<?= url()->current() ?>/?event=' + type;
    });
   
    $(document).ready(function () {
    $('.dataTable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            {
                text: 'PDF',
                extend: 'pdfHtml5',
                message: '',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function (doc) {
                    doc.pageMargins = [10, 10, 10, 10];
                    doc.defaultStyle.fontSize = 7;
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.title.fontSize = 9;
                    // Remove spaces around page title
                    doc.content[0].text = doc.content[0].text.trim();
                    // Create a footer
                    doc['footer'] = (function (page, pages) {
                        return {
                            columns: [
                                'www.shulesoft.com',
                                {
                                    // This is the right column
                                    alignment: 'right',
                                    text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                }
                            ],
                            margin: [10, 0]
                        }
                    });
                    // Styling the table: create style object
                    var objLayout = {};
                    // Horizontal line thickness
                    objLayout['hLineWidth'] = function (i) {
                        return .5;
                    };
                    // Vertikal line thickness
                    objLayout['vLineWidth'] = function (i) {
                        return .5;
                    };
                    // Horizontal line color
                    objLayout['hLineColor'] = function (i) {
                        return '#aaa';
                    };
                    // Vertical line color
                    objLayout['vLineColor'] = function (i) {
                        return '#aaa';
                    };
                    // Left padding of the cell
                    objLayout['paddingLeft'] = function (i) {
                        return 4;
                    };
                    // Right padding of the cell
                    objLayout['paddingRight'] = function (i) {
                        return 4;
                    };
                    // Inject the object in the document
                    doc.content[1].layout = objLayout;
                }
            },
            {extend: 'copyHtml5', footer: true},
            {extend: 'excelHtml5', footer: true},
            {extend: 'csvHtml5', customize: function (csv) {
                    return "ShuleSoft" + csv + "ShuleSoft";
                }},
            {extend: 'print', footer: true}

        ]
    });
    });


    </script>

<script type="text/javascript">
//    {{--$(document).ready(function () {--}}
//    {{--    var table = $('#receipt_ajax').DataTable({--}}
//    {{--        "processing": true,--}}
//    {{--        "serverSide": true,--}}
//    {{--        'serverMethod': 'post',--}}
//    {{--        'ajax': {--}}
//    {{--            'url': "<?= url('ajaxTable/?page=receipt') ?>"--}}
//    {{--        },--}}
//    {{--        "columns": [--}}
//    {{--            {"data": "row_id"},--}}
//    {{--            {"data": "code"},--}}
//    {{--            {"data": "number"},--}}
//    {{--            {"data": "name"},--}}
//    {{--            {"data": "created_at"},--}}
//    {{--            {"data": "method"},--}}
//    {{--            {"data": "reference"},--}}
//    {{--            {"data": ""}--}}
//    {{--        ],--}}
//    {{--        "columnDefs": [--}}
//    {{--            {--}}
//    {{--                "targets": 7,--}}
//    {{--                "data": null,--}}
//    {{--                "render": function (data, type, row, meta) {--}}
//    {{--                    return '<a href="<?= url('payment/receipts/?use=') ?>' + row.id + '" class="btn btn-xs btn-warning"><i class="fa fa-file-o"></i> Receipt</a>';--}}
//    {{--                }--}}
//
//    {{--            }],--}}
//
//    {{--        rowCallback: function (row, data) {--}}
//    {{--            //$(row).addClass('selectRow');--}}
//    {{--            $(row).attr('id', 'row' + data.id);--}}
//    {{--        }--}}
//
//    {{--    });--}}
//    {{--});--}}
    sort_event = function () {
        $('#sort_event').change(function () {
            var type = $(this).val();
            var currentUrl = '<?= url()->full() ?>';
            var url = new URL(currentUrl);
            url.searchParams.set("event", type); // setting your param
            var newUrl = url.href;
            window.location.href = newUrl;
        });
    }
    $(document).ready(sort_event);
</script>
@endsection