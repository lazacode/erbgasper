@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Certificates

            </header>
            <div class="panel-body">

                <br/>
                <section id="unseen">
                    <table id="data_ajax_example" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>email</th>
                                <th>number</th>
                                <th>phone</th>
                                <th>is_employer</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>email</th>
                                <th>number</th>
                                <th>phone</th>
                                <th>is_employer</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#data_ajax_example').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('certificate/ajax') ?>"
            },
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "email"},
                {"data": "number"},
                {"data": "phone"},
                {"data": "is_employer"},
                {"data": "action"}
            ],
            "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        return '<a class="btn btn-xs btn-primary" href="<?=url('certificate')?>/' + row.id + '">View Certificate</a>';
                    }
                
                }]
        });
        $('#data_ajax_example tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();
            alert(data[0] + "'s salary is: " + data[ 5 ]);
        });

    });
    sort_user = function () {
        $('#sort_user').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?user_type=' + type;
        });
    }

    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('certificate/printall?ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));
                console.log(param);

            } else {
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('certificate/printall?ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));
                $('#s_table' + value).remove();
            }
        });
    }
    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            if (status === true) {
                //select all

                $.ajax({
                    type: 'GET',
                    url: "<?= url('user/getApplicants') ?>",
                    data: {
                        "type": '<?= request('type') ?>',
                    },
                    dataType: "html",
                    success: function (data) {
                        console.log(data);
                        $('#search_checked_table').show();
                        var ex = data;
                        var url = '<?= url('certificate/printall?ids=') ?>';
                        var param = ex.split(",");
                        $('#nametag_link').attr('tags', param.join(","));
                        $('#nametag_link').attr('href', url + param.join(","));
                        console.log(param);

                    }
                });

            } else {
                //diselect all
                $('#search_checked_table').hide();
            }
        });
    };
    $(document).ready(toggle_all);
    $(document).ready(search_checked);
    $(document).ready(sort_user);
</script>
@endsection