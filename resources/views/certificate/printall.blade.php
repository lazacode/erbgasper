<style type="text/css" media="print">
    .page
    {
        -webkit-transform: rotate(-90deg); 
        -moz-transform:rotate(-90deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
</style>  <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ERB">
<link rel="shortcut icon">
<!--Core CSS -->
<link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">
<link href="{{ asset('public/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/clndr.css') }}?v=1" rel="stylesheet">
<!--clock css-->
<link href="{{ asset('public/js/css3clock/css/style.css') }}?v=1" rel="stylesheet">
<!--Morris Chart CSS -->
<link rel="stylesheet" href="{{ asset('public/js/morris-chart/morris.css') }}?v=1">
<!-- Custom styles for this template -->
<link href="{{ asset('public/css/style.css') }}?v=2" rel="stylesheet">
<link href="{{ asset('public/css/style-responsive.css') }}?v=1" rel="stylesheet"/>

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>
<script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<title>{{ config('app.name', 'ERB') }}</title>

<!-- Scripts -->

<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
<script src="{{ asset('public/js/jquery.js') }}"></script>
<style>
    h4{
        font-size: 1.2vw; color: black
    }
</style>
<div class="row">
    <div class="panel">

        <!--widget start-->
        <?php
        foreach ($users as $user) {
            ?>
            <div class="col-lg-offset-3">
                <div style="width:800px; height:600px; padding:20px; text-align:center; border: 10px solid #787878">
                    <div style="width:750px; height:550px; padding:20px; text-align:center; border: 5px solid #787878">
                        <span style="font-size:50px; font-weight:bold">Certificate of Attendance</span>
                        <br><br>
                        <span style="font-size:25px"><i>This is to certify that</i></span>
                        <br><br>
                        <span style="font-size:30px"><b>{{$user->name}}</b></span><br/><br/>
                        <span style="font-size:25px"><i>Participated in </i></span> <br/><br/>
                        <span style="font-size:30px"><?= $event->name ?></span> <br/><br/>
                        <span style="font-size:20px">and earned  points  <b>3</b></span> <br/><br/><br/><br/>
                        <span style="font-size:25px"><i>Dated</i></span><br>

                        <span style="font-size:30px"><?= $event->description ?></span>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>

</div>
<script>
causeRepaintsOn = $("h4");

$(window).resize(function () {
    causeRepaintsOn.css("z-index", 1);
});</script>

