@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Certificates

            </header>
            <div class="panel-body">
                <div class="row"  id="search_checked_table" style="display:none">
                    <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger"><i class="fa fa-cloud"></i> Print Certificate</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr  id="search_checked"></tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">  
                    <!--                    <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3 text-right">Sort</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="sort_user" name="user_type">
                                                    <option value=""></option> 
                                                    <option value="0">All</option> 
                                                    <option value="120">Employers</option> 
                    <?php $user_types = \App\Model\User_type::all(); ?>
                                                    @foreach ($user_types as $user_type)
                                                    <option value="{{$user_type->id}}">{{$user_type->name}}</option>                                                  @endforeach;
                                                </select>
                    
                                            </div>
                                        </div>-->
                </div>
                <a class="btn btn-xs btn-success" onclick="return confirm('you are about to send certificates to all participants. This cannot be undone. are you sure? ')" href="<?= url('certificate/send') ?>">Send Certificates</a>
                <br/>
                <br/>

                <section id="unseen">
                    <table id="data_ajax_example" class="table table-bordered table-striped table-condensed" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>type</th>
                                <th>employer</th>
                                <th>Specialization</th>
                                <th>Action</th>
                                <th><input type='checkbox' name='all' id='toggle_all'/></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>type</th>
                                <th>employer</th>
                                <th>Specialization</th>
                                <th>Action</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </section>


            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#data_ajax_example').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('certificate/ajax') ?>"
            },
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "phone"},
                {"data": "email"},
                {"data": "type"},
                {"data": "employer"},
                {"data": "specialization"},
                {"data": "action"},
                {"data": ""}
            ],
            "columnDefs": [{
                    "targets": 7,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        return '<a class="btn btn-xs btn-primary" href="<?= url('certificate') ?>/' + row.id + '?type=100">View Certificate</a>';
                    }

                },
                {
                    "targets": 8,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        return '<input type="checkbox" class="check" name="select[]" value="' + row.id + '"/>';
                    }

                }],
            rowCallback: function (row, data) {

                //$(row).addClass('selectRow');
                $(row).attr('id', 'row' + data.id);
            }
        });
        $('#data_ajax_example tbody').on('click', '.check', function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('certificate/printall?ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));
                console.log(param);

            } else {
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('certificate/printall?ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));
                $('#s_table' + value).remove();
            }
        });

    });
    sort_user = function () {
        $('#sort_user').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?user_type=' + type;
        });
    }

    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            if (status === true) {
                //select all

                $.ajax({
                    type: 'GET',
                    url: "<?= url('user/getApplicants') ?>",
                    data: {
                        "type": '<?= request('type') ?>',
                    },
                    dataType: "html",
                    success: function (data) {
                        console.log(data);
                        $('#search_checked_table').show();
                        var ex = data;
                        var url = '<?= url('certificate/printall?ids=') ?>';
                        var param = ex.split(",");
                        $('#nametag_link').attr('tags', param.join(","));
                        $('#nametag_link').attr('href', url + param.join(","));
                        console.log(param);

                    }
                });

            } else {
                //diselect all
                $('#search_checked_table').hide();
            }
        });
    };
    $(document).ready(toggle_all);
    $(document).ready(sort_user);
</script>
@endsection