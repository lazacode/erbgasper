@extends('layouts.app')

@section('content')
<style>
    @media print {
        a[href]:after {
            content: none !important;
        }

        #myTab {
            display: block !important;
            opacity: 1 !important;
        }

        table, thead, tbody, tr, td, th {
            border: 1px solid black !important;
        }

        #invoice_name {
            font-size: 15px !important;
            font-weight: bolder
        }
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Invoice
                <span class=" pull-right">
                    <div class="tab-pane">
                        <div style="">
                            @if(Auth::check())
                            <?php if ($invoice->status <> 1) { ?>  <a
                                    href="<?= url('payment/add?id=' . $invoice->id) ?>" class="btn btn-danger btn-sm"><i
                                        class="fa fa-money"></i> Add Payment </a>
                                <?php } ?>

                            @endif
                            <a href="#" onmousedown="print_page()" class="btn btn-primary btn-sm"><i
                                    class="fa fa-print"></i> Print </a>
                        </div>
                    </div>
                </span>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="panel-body invoice">
                            <div class="invoice-header">
                                <div class="invoice-title col-md-3 col-xs-2">
                                    <h1 id="invoice_name">invoice</h1>
                                </div>
                                <div class="invoice-info col-md-9 col-xs-10">

                                    <div class="pull-right">
                                        <div class="col-md-6 col-sm-6 pull-left">
                                            <p>Engineers Registration Board <br>
                                                Tetex Building (2nd and 4th Floor),<br>
                                                P.o Box 14942, Dar es salaam</p>
                                        </div>

                                        <div class="col-md-6 col-sm-6 pull-right">
                                            <p>Tel: +255 22 2122836<br>
                                                Email : registrar@erb.go.tz</p>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="row invoice-to">
                                <div class="col-md-4 col-sm-4 pull-left">
                                    <h4>Invoice To:</h4>
                                    <h2><?= $invoice->user->name ?></h2>
                                    <p>
                                        <br>
                                        Phone: <?= $invoice->user->phone ?><br>
                                        Email : <?= $invoice->user->email ?>
                                    </p>
                                </div>
                                <div class="col-md-4 col-sm-5 pull-right">
                                    @if($invoice->invoice_type != "sponsored")
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 inv-label">Control #</div>
                                        <div class="col-md-8 col-sm-6"><b
                                                style="font-size: 17px" class="control_number_check"><?= $invoice->number ?></b>
                                            <span id="updating"></span></div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-12" style="font-size: 14px;">
                                            <b class="control_number_check">Sponsored Invoice</b></div></div>
                                    @endif
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 inv-label">Date #</div>
                                        <div class="col-md-8 col-sm-7"><?= date('d M Y', strtotime($invoice->date)) ?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 inv-label">
                                            <h3>TOTAL DUE</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <td data-title="">
                                                <?php
                                                $am = $invoice->invoiceFee()->sum('amount');
                                                $am = $invoice->getAmount();

                                                $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');

                                                $unpaid = $am - $paid;
                                                ?>

                                                <h1 class="amnt-value">Tsh <?= number_format($unpaid) ?></h1>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <?php
                            $invoice_fee = $invoice->invoiceFee()->get();
                            if ($invoice->invoice_type == "sponsored") {
                                ?>
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Events</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        ?>
                                        <?php
                                        $names = '';
                                        foreach ($invoice_fee as $fees) {
                                            ?>
                                            <?php $names .= $fees->item_name . ',' ?>
                                            <tr>
                                                <td><?= $i ?></td>
                                                <td>
                                                    <h4>
                                                        <?= isset($fees) && !empty($fees) ? $fees->note : '' ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">{{ $fees->users->count() }}</td>
                                                <td class="text-center"><?= isset($fees) && !empty($fees) ? number_format($fees->amount * $fees->users->count()) : 0 ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>


                                    </tbody>
                                </table>
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">Participant</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $x = 1;
                                        $users = $invoice->users()->get();
                                        foreach ($users as $user) {
                                            $user = \App\Model\User::find($user->user_id);
                                            ?>
                                            <tr>
                                                <td><?= $x ?></td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->name
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->email
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->phone
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">{{ money($invoice->invoiceFee()->sum('amount')) }}</td>
                                            </tr>
                                            <?php
                                            $x++;
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            <?php } elseif ($invoice->invoice_type == "bulk") {
                                ?>
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Events</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        ?>
                                        <?php
                                        $names = '';
                                        foreach ($invoice_fee as $fees) {
                                            ?>
        <?php $names .= $fees->item_name . ',' ?>
                                            <tr>
                                                <td><?= $i ?></td>
                                                <td>
                                                    <h4>
        <?= isset($fees) && !empty($fees) ? $fees->note : '' ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">{{ $fees->users->count() }}</td>
                                                <td class="text-center"><?= isset($fees) && !empty($fees) ? number_format($fees->amount * $fees->users->count()) : 0 ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>


                                    </tbody>
                                </table>
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">Participant</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $x = 1;
                                        $users = $invoice->users()->get();
                                        foreach ($users as $user) {
                                            $user = \App\Model\User::find($user->user_id);
                                            ?>
                                            <tr>
                                                <td><?= $x ?></td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->name
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->email
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">
                                                    <h4> <?php
                                                        echo $user->phone
                                                        ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">{{ money($invoice->invoiceFee()->sum('amount')) }}</td>
                                            </tr>
                                            <?php
                                            $x++;
                                        }
                                        ?>

                                    </tbody>
                                </table>
<?php } else {
    ?>
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Events</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        ?>
                                        <?php
                                        $names = '';
                                        foreach ($invoice_fee as $fees) {
                                            ?>
        <?php $names .= $fees->item_name . ',' ?>
                                            <tr>
                                                <td><?= $i ?></td>
                                                <td>
                                                    <h4>
        <?= isset($fees) && !empty($fees) ? $fees->note : '' ?>
                                                    </h4>
                                                </td>
                                                <td class="text-center">1</td>
                                                <td class="text-center"><?= isset($fees) && !empty($fees) ? number_format($fees->amount) : 0 ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>


                                    </tbody>
                                </table>
<?php } ?>
                            <div class="row">
                                <div class="col-md-8 col-xs-7 payment-method">

                                    <p><b style="color:#0066cc">FOR BANKS</b>
                                        <br/>
                                        Use the INVOICE NUMBER to make payments in the Bank selected, thereafter a
                                        confirmation SMS & email will be sent to the mobile number and email you
                                        used during the Booking.
                                        <br/>
                                        <b>(You are advised to print this invoice and submit it to the bank along
                                            with the appreciate amount)</b>


                                    </p>
                                    <p>
                                        <br/><b style="color:#0066cc">FOR MOBILE</b><br/>
                                        Use the CONTROL NUMBER from the system as the reference number to make
                                        payments in the selected Mobile Company.</p>
                                    <br/>
                                    <p><b style="color:#0066cc">NB;</b><br/>
                                        in case you face any challenge, please call +255 222780228 (INETS CO LTD) OR
                                        +255 22 2122836 -(ERB)</p>

                                    <br>

                                </div>
                                <div class="col-md-4 col-xs-5 invoice-block pull-right">
                                    <ul class="unstyled amounts">


                                        <li>Sub - Total amount : <?= number_format($invoice->getAmount()) ?></li>
                                        <li>Paid Amount : <?= $paid > 0 ? $paid : 0 ?> </li>
                                        <li>Discount :___</li>
                                        <li class="grand-total">Grand Total : Tsh <?= number_format($unpaid) ?></li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>

</div>
<script type="text/javascript">
    control_number_check = function () {
        var number = $('.control_number_check').text();
        if (number.length < 6) {
            $('#updating').html('please wait, updating........');
            setTimeout(function () {
                window.location.reload();
            }, 4000);
        }
         if (number.length > 6) {
             $('#updating').html('');
        }

    }

    $(document).ready(control_number_check);
    print_page = function () {
        $('#head_one,#tab_panel_heading').hide();
        $('.widget-header, .btn, .breadcrumb, .clearfix').hide();
        $('#myTab').removeClass('nav-tabs');
        $('#myTab').removeClass('bar_tabs');
        window.print();
        $('#head_one,#tab_panel_heading').show();
        $('.widget-header, .btn, .breadcrumb, .clearfix').show();
        $('#myTab').addClass('nav-tabs');
        $('#myTab').addClass('bar_tabs');
    }</script>
@endsection
