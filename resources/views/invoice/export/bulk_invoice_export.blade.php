<?php
ob_start();
?>
<section id="unseen">
                        <table class="table table-bordered table-striped table-condensed dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Organization Name</th>
                                <th>Users</th>
                                <th class="numeric">Reference Number</th>
                                <th class="numeric">Amount</th>
                                <th class="numeric">Paid</th>
                                <th class="numeric">UnPaid</th>
                                <th class="numeric">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            $total_users = 0;
                            ?>
                            @foreach($invoices as $invoice)

                                <?php
                                $users = DB::table('invoice_users')->where('invoice_id', $invoice->id)->count();
                                $total_users += $users;
                                ?>
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$invoice->user->name}}</td>
                                    <td>{{$users}}</td>
                                    <td class="numeric">{{$invoice->number}}</td>
                                    <td data-title="">
                                        <?php
                                        $am = $invoice->getAmount();
                                        $total_amount += $am;
                                        echo money($am);
                                        ?>
                                    </td>

                                    <td data-title="">
                                        <?php
                                        $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                                        $total_paid += $paid;
                                        echo money($paid);
                                        ?>
                                    </td>
                                    <td data-title="">
                                        <?php
                                        $unpaid = $am - $paid;
                                        $total_unpaid += $unpaid;
                                        echo money($unpaid);
                                        ?>
                                    </td>
                                    <td class="numeric"><?php
                                        if ($invoice->status == 1) {
                                            echo '<span class="label label-success">Paid</span>';
                                        } else if ($invoice->status == 2) {
                                            echo '<span class="label label-warning">Partially Paid</span>';
                                        } else {
                                            echo '<span class="label label-danger">Not Paid</span>';
                                        }
                                        $i++;
                                        ?></td>

                
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2">Total</td>
                                <td><?=$total_users?></td>
                              
                                <td><?= money($total_amount) ?></td>
                                <td><?= money($total_paid) ?></td>
                                <td><?= money($total_unpaid) ?></td>
                                
                            </tr>
                            </tfoot>
                        </table>
                    </section>
<?php
$page = ob_get_clean();
$file_name = $file_name_export . '.xls';
Storage::disk('local')->put($file_name, $page);
?>