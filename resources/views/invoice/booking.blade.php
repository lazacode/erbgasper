@extends('layouts.app')

@section('content')
    <div class="container col-md-12">

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-8 col-sm-12">


                        <div class="modal-content">
                            <h4 class="modal-title">Book for the Event</h4>

                            <div class="modal-body">
                                <div class="panel-body">
                                    <form class="cmxform form-horizontal " id="commentForm" method="post"
                                          action="<?= url('booking') ?>">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Employer</label>
                                                <div class="col-lg-6">
                                                    <select class="form-control select2_single select2"
                                                            name="employer_id">
                                                        <option value=""></option>
                                                        <?php $userype = \App\Model\Employer::all() ?>
                                                        @foreach ($userype as $type)
                                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <?php echo form_error($errors, 'employer_id'); ?>
                                                <a data-toggle="modal" href="#myModalEmployer">Or Add new</a>
                                            </div>

                                            <div class="form-group ">
                                                <label for="name" class="control-label col-lg-3">Your Name
                                                    (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" value="<?= old('name') ?>" id="name"
                                                           name="name" minlength="2" type="text" required=""
                                                           placeholder="Firstname Lastname"
                                                           onblur="this.value = this.value.toUpperCase()"
                                                           pattern="[a-zA-Z\. ]{5,}">
                                                </div>

                                                <span class="help-block">E.g John Joseph, only string and . </span>
                                                <?php echo form_error($errors, 'name'); ?>
                                            </div>
                                            <div class="form-group ">
                                                <label for="amount" class="control-label col-lg-3">Your Phone
                                                    (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" value="<?= old('phone') ?>" id="phone"
                                                           name="phone" minlength="2" type="text" required="">
                                                </div>
                                                <?php echo form_error($errors, 'phone'); ?>
                                            </div>
                                            <div class="form-group ">
                                                <label for="user_email" class="control-label col-lg-3">Your
                                                    Email(required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" value="<?= old('email') ?>"
                                                           id="user_email" name="email" minlength="2" type="email"
                                                           required="" onblur="this.value = this.value.toLowerCase()">
                                                    <input type="hidden" name="invoice_type" value="normal">
                                                </div>
                                                <?php echo form_error($errors, 'email'); ?>
                                            </div>
                                            <div class="form-group ">

                                                <label for="number" class="control-label col-lg-3">Specialization
                                                    (Required)</label>

                                                <div class="col-lg-6">
                                                    <select class="form-control select2_single select2"
                                                            name="profession_id">
                                                        <option value=""></option>
                                                        <?php $professions = App\Model\Profession::where('invitee', 0)->get(); ?>
                                                        @foreach ($professions as $profession)
                                                            <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach
                                                        ;
                                                    </select>

                                                </div>
                                            <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                            </div>

                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Choose Event(s)</label>
                                                <div class="col-lg-6" style="color:black !important;">
                                                    <div class="col-lg-12">
                                                        @foreach($events as $event)
                                                            @if($event->virtual == "on")
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="{{ $event->id }}"
                                                                               value="{{ $event->id }}"
                                                                               class="eventsCheckbox"
                                                                               title="{{ $event->name }}">
                                                                        {{ $event->name }}
                                                                    </label>
                                                                </div>
                                                            @else
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="{{ $event->id }}"
                                                                               value="{{ $event->id }}"
                                                                               class="eventsCheckboxOff"
                                                                               title="{{ $event->name }}">
                                                                        {{ $event->name }}
                                                                    </label>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                        <?php echo form_error($errors, 'events'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3"></label>
                                            <div class="col-lg-6">
                                                <input type="text" name="events" class="eventsJson" hidden="hidden">
                                                <button class="btn btn-primary btn-lg col-sm-8" style="font-size: 13px;"
                                                        type="submit">Get Your Reference Number
                                                </button>
                                                <div class="col-lg-12" style="color:black !important;">
                                                    Reference Number will be sent to your email
                                                </div>
                                            </div>
                                        </div>
                                        <?= csrf_field() ?>
                                    </form>

                                    <div class="col-lg-12 col-xl-12" id="companyForm" hidden>
                                        <div class="card-block">

                                            <div class="table-responsive dt-responsive">
                                                <div class="card-header">
                                                    <div class="panel-body">
                                                        <div class="alert alert-info">
                                                            This will be a one invoice with multiple users within a certain organization. You need to upload excel file with list of applicants who will be paid by this invoice.
                                                            <br>
                                                            <b style="color: red;"><a href="{{ url('public/erb-sampleFile.xlsx') }}">Download Sample File</a></b></div>
                                                        <img src="<?= url('public/images/sample_excel.jpg') ?>"/>
                                                        <br/>
                                                        <div class=" form">
                                                            <br/>
                                                            <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('invoice') ?>" enctype="multipart/form-data">
                                                                <div class="form-group ">
                                                                    <label for="type" class="control-label col-lg-3">Employer</label>
                                                                    <div class="col-lg-6">
                                                                        <select class="form-control select2_single select2" name="user_id">
                                                                            <?php $userype = \App\Model\Employer::all(); ?>
                                                                            @foreach ($userype as $type)
                                                                                <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                                        </select>

                                                                    </div>
                                                                </div>


                                                                <div class="form-group ">
                                                                    <label for="amount" class="control-label col-lg-3">Your Phone
                                                                        (required)</label>
                                                                    <div class="col-lg-6">
                                                                        <input class=" form-control" value="<?= old('phone') ?>" id="phone"
                                                                               name="phone" minlength="2" type="text" required="">
                                                                    </div>
                                                                    <?php echo form_error($errors, 'phone'); ?>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="user_email" class="control-label col-lg-3">Your
                                                                        Email(required)</label>
                                                                    <div class="col-lg-6">
                                                                        <input class=" form-control" value="<?= old('email') ?>"
                                                                               id="user_email" name="email" minlength="2" type="email"
                                                                               required="" onblur="this.value = this.value.toLowerCase()">
                                                                    </div>
                                                                    <?php echo form_error($errors, 'email'); ?>
                                                                </div>

                                                                <div class="form-group ">
                                                                    <label for="type" class="control-label col-lg-3">Choose Event(s)</label>
                                                                    <div class="col-lg-6" style="color:black !important;">
                                                                        <div class="col-lg-12">
                                                                            @foreach($events as $event)
                                                                                @if($event->virtual == "on")
                                                                                    <div class="checkbox">
                                                                                        <label>
                                                                                            <input type="checkbox" id="{{ $event->id }}"
                                                                                                   value="{{ $event->id }}"
                                                                                                   class="eventsCheckbox"
                                                                                                   title="{{ $event->name }}">
                                                                                            {{ $event->name }}
                                                                                        </label>
                                                                                    </div>
                                                                                @else
                                                                                    <div class="checkbox">
                                                                                        <label>
                                                                                            <input type="checkbox" id="{{ $event->id }}"
                                                                                                   value="{{ $event->id }}"
                                                                                                   class="eventsCheckboxOff"
                                                                                                   title="{{ $event->name }}">
                                                                                            {{ $event->name }}
                                                                                        </label>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                            <?php echo form_error($errors, 'events'); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group " hidden>
                                                                    <label for="price" class="control-label col-lg-3">Price Per User</label>
                                                                    <div class="col-lg-6">

                                                                        <input class="form-control " id="number" type="text" name="amount" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="cname" class="control-label col-lg-3">Choose Excel File (required)</label>
                                                                    <div class="col-lg-6">
                                                                        <input class=" form-control" id="cname" name="file" type="file" required="">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">
                                                                    <div class="col-lg-offset-3 col-lg-6">
                                                                        <input type="text" name="events" class="eventsJson" hidden="hidden">
                                                                        <?= csrf_field() ?>
                                                                        <button class="btn btn-primary" type="submit">Upload to Create Invoice</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div id="events">
                        @foreach($events as $event)

                            <div class="col-lg-4 col-sm-12" id="event{{$event->id}}" style="display: none;">
                                <div class="feed-box text-center">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <?php
                                            $day = $event->date;

                                            // add 1 days to the date above
                                            $NewDate = date('Y-m-d', strtotime($day . " +1 days"));
                                            ?>
                                            <a title="Add to my calender"
                                               href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?= $event->name ?>&dates=<?= date('Ymd', strtotime($event->date)) ?>T224000Z/<?= date('Ymd', strtotime($NewDate)) ?>T221500Z&details=<?= $event->theme ?>.. For+more+details,+click+here:+http://engineersday.co.tz&location=<?= $event->location ?>&sf=true&output=xml"
                                               target="_blank">
                                                <div class="corner-ribon blue-ribon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </a>
                                            <span class="bar-label-value"> <?= $event->name ?></span><br/>
                                            <span>By Engineers Registration Board(ERB)</span>
                                            <p><br/><i class="fa fa-time"></i>&nbsp; <?= $event->description ?></p>
                                            <p>
                                                <br/><i class="fa fa-map-marker"></i>&nbsp; <?= $event->location . ' <br/>' ?>
                                            </p>
                                        </div>
                                        <div class="mini-stat clearfix">
                                            <div class="mini-stat-info">
                                                <span><h2 class="booking_total_amount"></h2></span>
                                            </div>
                                        </div>
                                        <hr/>

                                    </section>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-lg-4 col-sm-12" id="bookingSummary" hidden>
                        <h4 class="text-center">BOOKING SUMMARY</h4>
                        <hr>
                        <table class="table table-bordered">
                            <tr>
                                <th>TOTAL EVENTS: </th>
                                <td id="totalEvents">0</td>
                            </tr>
                            <tr>
                                <th>PRICE PER USER: </th>
                                <td id="pricePerUser">TSH 0</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form class="cmxform form-horizontal " id="commentForm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="title_page">How will you Attend ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">


                            <div class="form-group ">
                                <label for="type" class="control-label col-lg-3">How will you Attend ?</label>
                                <div class="col-lg-9" style="color:black !important;">
                                    <div class="col-lg-6">
                                        <input type="radio" onclick="clickRadio(this.value)" value="0" name="virtual"
                                               class="form-control radio"/>
                                        Physical Event (Dodoma)
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="radio" onclick="clickRadio(this.value)" value="1" name="virtual"
                                               class="form-control radio"/> Online
                                        Event (via Zoom)
                                    </div>


                                </div>
                            </div>

                            <div class="form-group" style="display: none;margin-top: 45px;" id="pdu_materials">
                                <label for="materials" class="control-label col-lg-3">Do you need PDU with ERB materials
                                    ?</label>
                                <div class="col-lg-9">
                                    <select class="form-control select2_single select2" name="virtual_material"
                                            id="material_options" onchange="changeMaterial(this.value)">
                                        <option value=""></option>
                                        <option value="1">Yes - I need PDU with ERB Materials (Bag, T-shirt etc)
                                        </option>

                                        <option value="0">No - I need PDU only without Materials</option>
                                    </select>
                                </div>
                                <?php echo form_error($errors, 'materials'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button" onclick="saveEvents()">Save changes</button>

                    </div>
                </div>
            </form>
        </div>

    </div>
    <div class="modal fade" id="eventModalOff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form class="cmxform form-horizontal " id="commentForm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="title_page">How will you Attend ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">


                            <div class="form-group ">
                                <label for="type" class="control-label col-lg-3">How will you Attend ?</label>
                                <div class="col-lg-9" style="color:black !important;">
                                    <div class="col-lg-6">
                                        <input type="radio" onclick="clickRadio(this.value)" value="0" name="virtual"
                                               class="form-control radio"/>
                                        Physical Event (Dodoma)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button" onclick="saveEvents()">Save changes</button>

                    </div>
                </div>
            </form>
        </div>

    </div>

    <div class="modal fade" id="myModalEmployer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Add New Entity</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class=" form">
                                <div class="form-group ">
                                    <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                                               required="">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="cname" class="control-label col-lg-3">Abbreviation</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="cname" name="abbreviation" type="text"
                                               required="" onblur="this.value = this.value.toUpperCase()">
                                    </div>
                                </div>
                                <!--            <div class="form-group ">
                                             <label for="curl" class="control-label col-lg-3">Phone (required)</label>
                                             <div class="col-lg-6">
                                                 <input class="form-control " id="curl" type="text" name="phone">
                                             </div>
                                         </div>
                                         <div class="form-group ">
                                             <label for="ccomment" class="control-label col-lg-3">Location(required)</label>
                                             <div class="col-lg-6">
                                                 <textarea class="form-control " id="ccomment" name="location" required=""></textarea>
                                             </div>
                                         </div>-->

                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <?= csrf_field() ?>
                        <input type="hidden" name="user" value="employer"/>
                        <input type="hidden" name="auth" value="0"/>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="submit">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        var data = {
            eventId: null,
            eventState: null,
            evnt_state_online: null,
            eventFee: 0,
        };
        var dataEvents = [];
        var totalSelectedEvents = 0;
        var pricePerUser = 0;
        var selectedCheckboxID = '';

        $('.eventsCheckbox').change(function () {
            let selectedCheckbox = $(this);
            var id = selectedCheckbox.attr('id');
            selectedCheckboxID = selectedCheckbox.attr('id');
            var title = selectedCheckbox.attr('title');
            if (selectedCheckbox.prop("checked")) {
                $(".radio").prop("checked", false);
                $('#pdu_materials').hide();
                $("#event" + id).show(300);
                $("#title_page").text(title);
                data.eventId = id;
                $('#eventModal').modal({
                    backdrop: 'static',
                    keyboard: false,
                });

                $(document).on('hidden.bs.modal', '#eventModal', function () {
                    if (!dataEvents.some(dataEvent => dataEvent.eventId === id)) {
                        selectedCheckbox.prop("checked", false);
                        $("#event" + selectedCheckbox.attr('id')).hide(300);
                    }
                });
            } else {
                var id = selectedCheckbox.attr('id');
                //dataEvents.pop(id);
                dataEvents = dataEvents.filter(item => item.eventId != id);
                totalSelectedEvents =+ dataEvents.length;
                $('#totalEvents').text(totalSelectedEvents);

                pricePerUser = dataEvents.reduce((accum,item) => accum + parseInt(item.eventFee), 0);
                $('#pricePerUser').text('TSH '+pricePerUser);
                $("#event" + id).hide(300);
            }
        });
        $('.eventsCheckboxOff').change(function () {
            let selectedCheckbox = $(this);
            var id = selectedCheckbox.attr('id');
            selectedCheckboxID = selectedCheckbox.attr('id');
            var title = selectedCheckbox.attr('title');
            if (selectedCheckbox.prop("checked")) {
                $(".radio").prop("checked", false);
                $('#pdu_materials').hide();
                $("#event" + id).show(300);
                $("#title_page").text(title);
                data.eventId = id;
                $('#eventModalOff').modal({
                    backdrop: 'static',
                    keyboard: false,
                });

                $(document).on('hidden.bs.modal', '#eventModalOff', function () {
                    if (!dataEvents.some(dataEvent => dataEvent.eventId === id)) {
                        selectedCheckbox.prop("checked", false);
                        $("#event" + selectedCheckbox.attr('id')).hide(300);
                    }
                });
            } else {
                var id = selectedCheckbox.attr('id');
                //dataEvents.pop(id);
                dataEvents = dataEvents.filter(item => item.eventId != id);
                totalSelectedEvents =+ dataEvents.length;
                $('#totalEvents').text(totalSelectedEvents);

                pricePerUser = dataEvents.reduce((accum,item) => accum + parseInt(item.eventFee), 0);
                $('#pricePerUser').text('TSH '+pricePerUser);
                $("#event" + id).hide(300);
            }
        });

        function clickRadio(val) {
            var virtual;
            if (val == 1) {
                //physical visits
                $('#pdu_materials').show();
                virtual = 1;
                data.eventState = virtual;
            } else {
                $('#pdu_materials').hide();
                virtual = 0;
                data.eventState = virtual;
                data.evnt_state_online = null;
            }
            get_booking_amount(virtual, 3);
        }

        function changeMaterial(val) {
            data.evnt_state_online = val;
            get_booking_amount(1, val);
        }

        get_booking_amount = function (a, b) {
            $.ajax({
                url: '<?= url('/booking/getAmount') ?>/?',
                method: 'get',
                data: {virtual: a, option: b, eventid: selectedCheckboxID},
                success: function (amount) {
                    data.eventFee = amount;
                    $('#event' + selectedCheckboxID + ' .booking_total_amount').html('Tsh ' + amount);
                }
            })
        }

        function saveEvents() {
            if (data.eventState == null) {
                alert("Please Choose An Option")
            } else {
                if (data.eventState === 1 && data.evnt_state_online == null) {
                    alert("Please Choose An Option")
                } else {
                    if (dataEvents.some(dataEvent => dataEvent.eventId === data.eventId)) {
                        dataEvents = dataEvents.filter(item => item.eventId != data.eventId);
                        dataEvents.push(data);
                        totalSelectedEvents =+ dataEvents.length;
                        pricePerUser = dataEvents.reduce((accum,item) => accum + parseInt(item.eventFee), 0);
                        $('#totalEvents').text(totalSelectedEvents);
                        $('#pricePerUser').text('TSH '+pricePerUser);
                        $(".eventsJson").val(JSON.stringify(dataEvents));
                        data = {
                            eventId: null,
                            eventState: null,
                            evnt_state_online: null,
                        };
                        $('#eventModal').modal('hide');
                        $('#eventModalOff').modal('hide');
                    } else {
                        dataEvents.push(data);
                        totalSelectedEvents =+ dataEvents.length;
                        pricePerUser = dataEvents.reduce((accum,item) => accum + parseInt(item.eventFee), 0);
                        $('#totalEvents').text(totalSelectedEvents);
                        $('#pricePerUser').text('TSH '+pricePerUser);
                        $(".eventsJson").val(JSON.stringify(dataEvents));
                        data = {
                            eventId: null,
                            eventState: null,
                            evnt_state_online: null,
                        };
                        $('#eventModal').modal('hide');
                        $('#eventModalOff').modal('hide');
                    }

                }
            }
        }

        $('#bookingCategories').change(function () {
            if ($(this).val() == 0) {
                $('#commentForm').show();
                $('#companyForm').hide();
                $('#bookingSummary').hide();
                $('#events').show();
            } else {
                $('#commentForm').hide();
                $('#companyForm').show();
                $('#bookingSummary').show();
                $('#events').hide();
            }
        });

    </script>
@endsection