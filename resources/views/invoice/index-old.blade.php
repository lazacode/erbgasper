@extends('layouts.app')

@section('content')

<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Invoices

            </header>
            <?php if (can_access('add_invoices')) { ?>
                <p>
                    <br/>
                    &nbsp; <a href="<?= url('invoice/create') ?>" class="btn btn-primary">Create Control Number</a>
                </p>
            <?php } ?>
            <div class="panel-body">
                <div class="position-center">
                   
                </div>
                <p></p>
                <section>
                 <table class="table table-bordered table-striped table-condensed" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th class="numeric">Reference Number</th>
                            <th class="numeric">Date</th>
                            <th class="numeric">Amount</th>
                            <th class="numeric">Payment For</th>
                            <th class="numeric">Paid Amount</th>
                            <th class="numeric">UnPaid Amount</th>
                            <th class="numeric">Payment Status</th>
                            <th class="numeric col-md-3">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td></td>
                                <td>{{$invoice->user->name}}</td>
                                <td>{{$invoice->number}}</td>
                                <td>{{ date('Y-m-d', strtotime($invoice->date))}}<td>
                                <td>Event Fee</td>
                                <td></td>
                                <td>{{$invoice->invoiceFeesPayment()->sum('paid_amount')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th class="numeric">Reference Number</th>
                            <th class="numeric">Date</th>
                            <th class="numeric">Amount</th>
                            <th class="numeric">Payment For</th>
                            <th class="numeric">Paid Amount</th>
                            <th class="numeric">UnPaid Amount</th>
                            <th class="numeric">Payment Status</th>
                            <th class="numeric col-md-3">Action</th>
                        </tr>
                    </tfoot>
                </table>

                </section>
                <section id="unseen">
                 <table id="invoice_ajax" class="table table-bordered table-striped table-condensed" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th class="numeric">Reference Number</th>
                            <th class="numeric">Date</th>
                            <th class="numeric">Amount</th>
                            <th class="numeric">Payment For</th>
                            <th class="numeric">Paid Amount</th>
                            <th class="numeric">UnPaid Amount</th>
                            <th class="numeric">Payment Status</th>
                            <th class="numeric col-md-3">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Payer Name</th>
                            <th class="numeric">Reference Number</th>
                            <th class="numeric">Date</th>
                            <th class="numeric">Amount</th>
                            <th class="numeric">Payment For</th>
                            <th class="numeric">Paid Amount</th>
                            <th class="numeric">UnPaid Amount</th>
                            <th class="numeric">Payment Status</th>
                            <th class="numeric col-md-3">Action</th>
                        </tr>
                    </tfoot>
                </table>

                </section>
              
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#invoice_ajax').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('ajaxTable/?page=invoice&type=' . request('user_type')) ?>"
            },
            "columns": [
                {"data": "row_id"},
                {"data": "name"},
                {"data": "number"},
                {"data": "created_at"},
                {"data": "amount"},
                {"data": "payment_for"},
                {"data": "paid_amount"},
                {"data": "unpaid_amount"},
                {"data": "status"},
                {"data": ""}
            ],
            "columnDefs": [{
                    "targets": 9,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        var del = '';
                        var add = '';
                        var view = '<a href="<?= url('invoice/') ?>/' + row.id + '" class="btn btn-xs btn-success">View</a>';
<?php if (can_access('add_payments')) { ?>

                            del = '<form method="POST" action="<?= url('invoice/') ?>/' + row.id + '" accept-charset="UTF-8" class=""><?=csrf_field()?><input name="_method" type="hidden" value="DELETE"><input name="type" type="hidden" value=""><input class="btn btn-xs btn-danger" type="submit" value="Delete"></form>';
<?php } ?>
<?php if (can_access('add_payments')) { ?>
                            add =row.status=='Paid'? '': '<a href="<?= url('payment/add?id=') ?>' + row.id + '" class="btn btn-primary btn-xs">Payment </a>';
<?php } ?>

                        return '<p>'+view +'&nbsp;'+ del +'&nbsp;'+ add;
                    }

                }],

            rowCallback: function (row, data) {
                //$(row).addClass('selectRow');
                $(row).attr('id', 'row' + data.id);
            }

        });

    });
</script>
@endsection
