     <link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">
        <link href="{{ asset('public/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/clndr.css') }}" rel="stylesheet">
        <!--clock css-->
        <link href="{{ asset('public/js/css3clock/css/style.css') }}" rel="stylesheet">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('public/js/morris-chart/morris.css') }}?v=1">
        <!-- Custom styles for this template -->
        <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/style-responsive.css') }}" rel="stylesheet"/>
        <div class="row" style="" id="print_div">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body invoice">
                <div class="invoice-header">
                    <div class="invoice-title col-md-3 col-xs-2">
                        <h1>invoice</h1>
                    </div>
                    <div class="invoice-info col-md-9 col-xs-10">

                        <div class="pull-right">
                            <div class="col-md-6 col-sm-6 pull-left">
                                <p>Engineers Registration Board <br>
                                    Tetex Building (2nd and 4th Floor),
                                    P.o Box 14942, Dar es salaam</p>
                            </div>

                            <div class="col-md-6 col-sm-6 pull-right">
                                <p>Tel: +255 22 2122836<br>
                                    Email : registrar@erb.go.tz<script type="text/javascript">
                                        /* <![CDATA[ */
                                        (function () {
                                            try {
                                                var s, a, i, j, r, c, l, b = document.getElementsByTagName("script");
                                                l = b[b.length - 1].previousSibling;
                                                a = l.getAttribute('data-cfemail');
                                                if (a) {
                                                    s = '';
                                                    r = parseInt(a.substr(0, 2), 16);
                                                    for (j = 2; a.length - j; j += 2) {
                                                        c = parseInt(a.substr(j, 2), 16) ^ r;
                                                        s += String.fromCharCode(c);
                                                    }
                                                    s = document.createTextNode(s);
                                                    l.parentNode.replaceChild(s, l);
                                                }
                                            } catch (e) {
                                            }
                                        })();
                                        /* ]]> */
                                    </script></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row invoice-to">
                    <div class="col-md-4 col-sm-4 pull-left">
                        <h4>Invoice To:</h4>
                        <h2><?= $invoice->user->name ?></h2>
                        <p>
                            <br>
                            Reg: <?= $invoice->user->number ?><br>
                            Phone: <?= $invoice->user->phone ?><br>
                            Email : <?= $invoice->user->email ?><script type="text/javascript">
                                /* <![CDATA[ */
                                (function () {
                                    try {
                                        var s, a, i, j, r, c, l, b = document.getElementsByTagName("script");
                                        l = b[b.length - 1].previousSibling;
                                        a = l.getAttribute('data-cfemail');
                                        if (a) {
                                            s = '';
                                            r = parseInt(a.substr(0, 2), 16);
                                            for (j = 2; a.length - j; j += 2) {
                                                c = parseInt(a.substr(j, 2), 16) ^ r;
                                                s += String.fromCharCode(c);
                                            }
                                            s = document.createTextNode(s);
                                            l.parentNode.replaceChild(s, l);
                                        }
                                    } catch (e) {
                                    }
                                })();
                                /* ]]> */
                            </script>
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-5 pull-right">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 inv-label">Invoice #</div>
                            <div class="col-md-8 col-sm-6"><b style="font-size: 17px"><?= $invoice->number ?></b></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-sm-5 inv-label">Date #</div>
                            <div class="col-md-8 col-sm-7"><?= date('d M Y', strtotime($invoice->date)) ?></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 inv-label">
                                <h3>TOTAL DUE</h3>
                            </div>
                            <div class="col-md-12">
                                <h1 class="amnt-value">Tsh <?= number_format($invoice->getAmount()) ?></h1>
                            </div>
                        </div>


                    </div>
                </div>
                <table class="table table-invoice">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $invoice_fee = $invoice->invoiceFee()->get();
                        $i = 1;
                        foreach ($invoice_fee as $fee) {
                            ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td>
                                    <h4><?= $fee->item_name ?></h4>
                                    <p><?= $fee->note ?></p>
                                </td>
                                <td class="text-center">1</td>
                                <td class="text-center"><?= number_format($fee->amount) ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>

                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-8 col-xs-7 payment-method">
                        <h4>Payment Method</h4>
                        <p>1. Banks.</p>
                        <p>2. Mobile Payments.</p>
                        <p>3. Cards.</p>
                        <br>
                        <h3 class="inv-label itatic">Thank you for your business</h3>
                    </div>
                    <div class="col-md-4 col-xs-5 invoice-block pull-right">
                        <ul class="unstyled amounts">
                            <li>Sub - Total amount : <?= number_format($invoice->getAmount()) ?></li>
                            <li>Discount :___ </li>
                            <li>TAX  ----- </li>
                            <li class="grand-total">Grand Total : Tsh <?= number_format($invoice->getAmount()) ?></li>
                        </ul>
                    </div>
                </div>



            </div>
        </section>
    </div>
</div>