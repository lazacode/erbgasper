@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <form action="">
                <div class="form-group">
                    <label for="sortEvent">FILTER BY EVENT(s)</label>
                    <select name="event" id="sortEvent" class="form-control">
                        <option></option>
                        <option value="">All Active Events</option>
                        @foreach($events as $event)
                            <option value="{{$event->id}}">{{ $event->name }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon orange"><i class="fa fa-align-justify"></i></span>
                <div class="mini-stat-info">
                    <span>{{ $total_invoices }}</span>
                    Total Invoices

                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon tar"><i class="fa fa-money"></i></span>
                <div class="mini-stat-info">
                    <span>{{ money($total_amount) }}</span>
                    Total Amounts
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
                <div class="mini-stat-info">
                    <span>{{ money($paid_amount) }}</span>
                    Paid Amounts
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="mini-stat clearfix">
                <span class="mini-stat-icon green"><i class="fa fa-money"></i></span>
                <div class="mini-stat-info">
                    <span>{{ money($unpaid_amount) }}</span>
                    Unpaid Amounts
                </div>
            </div>
        </div>
    </div>
    <!-- page start-->

    <div class="row">
        <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Sponsored Invoices
                    <?php if (can_access('add_invoices')) { ?>
                    &nbsp; <a href="<?= url('invoice/create/?bulk=1') ?>" class="btn btn-primary pull-right">
                        Create Bulk Invoice
                    </a> <br><br>
                    <?php } ?>
                </header>
                <div class="col-md-12">
                <!--                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="fa fa-user"></i></span>
                        <div class="mini-stat-info">
                            <span><?php //\App\Model\Invoice_fee::whereIn('invoice_id',\App\Model\Invoice::where('type',1)->get(['id']))->count()?></span>
                            Total sponsored Invoices
                        </div>
                    </div>-->
                </div>

                <div class="panel-body">
                    <div class="position-center">

                    </div>
                 <p><a href="<?= url('invoice/bulk?export=1') ?>" class="btn btn-primary">Export to
                        Excel</a></p>
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Organization Name</th>
                                <th>Users</th>
                                <th class="numeric">Reference Number</th>
                                <th class="numeric">Amount</th>
                                <th class="numeric">Paid</th>
                                <th class="numeric">UnPaid</th>
                                <th class="numeric">Status</th>
                                <th class="numeric col-md-3">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            $total_users = 0;
                            ?>
                            @foreach($invoices as $invoice)

                                <?php
                                $users = DB::table('invoice_users')->where('invoice_id', $invoice->id)->count();
                                $total_users += $users;
                                ?>
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$invoice->user->name}}</td>
                                    <td>{{$users}}</td>
                                    <td class="numeric">{{$invoice->number}}</td>
                                    <td data-title="">
                                        <?php
                                        $am = $invoice->getAmount();
                                        $total_amount += $am;
                                        echo money($am);
                                        ?>
                                    </td>

                                    <td data-title="">
                                        <?php
                                        $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                                        $total_paid += $paid;
                                        echo money($paid);
                                        ?>
                                    </td>
                                    <td data-title="">
                                        <?php
                                        $unpaid = $am - $paid;
                                        $total_unpaid += $unpaid;
                                        echo money($unpaid);
                                        ?>
                                    </td>
                                    <td class="numeric"><?php
                                        if ($invoice->status == 1) {
                                            echo '<span class="label label-success">Paid</span>';
                                        } else if ($invoice->status == 2) {
                                            echo '<span class="label label-warning">Partially Paid</span>';
                                        } else {
                                            echo '<span class="label label-danger">Not Paid</span>';
                                        }
                                        $i++;
                                        ?></td>

                                    <td class="numeric">
                                        <a href="<?= url('invoice/' . $invoice->id) ?>" class="btn btn-xs btn-success">View</a>
                                        <?php /*if (can_access('edit_invoices')) { */?><!--
                                        <a href="<?/*= url('invoice/' . $invoice->id . '/edit') */?>"
                                           class="btn btn-xs btn-info">Edit</a> --><?php /*} */?>
                                        <?= can_access('delete_invoices') ? btn_delete('invoice/' . $invoice->id, '') : '' ?>
                                        <?php if ($invoice->status <> 1 && can_access('add_payments')) { ?>
                                        <a href="<?= url('payment/add?id=' . $invoice->id) ?>"
                                           class="btn btn-primary btn-xs">Payment </a>
                                        <?php } ?>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2">Total</td>
                                <td><?=$total_users?></td>
                                <td></td>
                                <td><?= money($total_amount) ?></td>
                                <td><?= money($total_paid) ?></td>
                                <td><?= money($total_unpaid) ?></td>
                                <td colspan="2"></td>
                            </tr>
                            </tfoot>
                        </table>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
    <script type="text/javascript">
          $(document).ready(function () {
        $('.dataTable').dataTable();
    })
        $('#sortEvent').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?event=' + type;
        });
    </script>

@endsection
