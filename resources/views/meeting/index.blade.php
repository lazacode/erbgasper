@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <form action="">
            <div class="form-group">
                <label for="sortEvent">FILTER BY MEETING(s)</label>
                <select name="event" id="sortEvent" class="form-control">
                    <option></option>
                    @foreach($events as $event)
                    <option value="{{$event->meeting_id}}">{{ $event->topic }}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon orange"><i class="fa fa-users"></i></span>
            <div class="mini-stat-info">
                <span>{{ count($participants) }}</span>
                Total Registered Users

            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon tar"><i class="fa fa-users"></i></span>
            <div class="mini-stat-info">
                <span>{{ count($attended) }}</span>
                Total Attended Users
            </div>
        </div>
    </div>
</div>
<!-- page start-->
<div class="row">
    <h1></h1>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <b>
                    {{ $meeting['data']['topic'] }} - USERS
                </b>
            </header>

            <div class="panel-body">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue">
                            <ul class="nav nav-tabs nav-justified ">
                                <li class="active">
                                    <a data-toggle="tab" href="#members">
                                        Registered Participants
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#overview">
                                        Attended Participants
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#job-history">
                                        Event Logs
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <hr>
                        <br>
                        <div class="panel-body">
                            <div class="tab-content tasi-tab">
                                <div id="members" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped table-condensed dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Fullname</th>
                                                        <th>Email</th>
                                                        <th class="numeric">Zoom Account Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($participants as $participant)
                                                    <tr> 
                                                        <?php
                                                        $firstname = isset($participant['first_name']) ? $participant['first_name'] : '';
                                                        $lastname = isset($participant['last_name']) ? $participant['last_name'] : '';
                                                        ?>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $firstname.' '.$lastname }}</td>
                                                        <td>{{ $participant['email'] }}</td>
                                                        <td class="text-info">{{ $participant['status'] }}</td>
                                                    </tr>

                                                    @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div id="overview" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-striped table-condensed dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Fullname</th>
                                                        <th>Email</th>
                                                        <th class="numeric">Attendance Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($attended as $attendee)
                                                    <?php $user = \App\Model\User::find($attendee->user_id); ?>
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $user->name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                        <td class="text-info">{{ $attendee->status }}</td>
                                                    </tr>

                                                    @endforeach
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                <div id="job-history" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-12">

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

            <!-- page end-->
            <script type="text/javascript">
                $('#sortEvent').change(function () {
                    var type = $(this).val();
                    window.location.href = '<?= url('meeting/') ?>/' + type;
                });
            </script>

            @endsection
