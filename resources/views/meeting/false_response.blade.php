@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <form action="">
            <div class="form-group">
                <label for="sortEvent">FILTER BY MEETING(s)</label>
                <select name="event" id="sortEvent" class="form-control">
                    <option></option>
                    @foreach($events as $event)
                    <option value="{{$event->meeting_id}}">{{ $event->topic }}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="mini-stat clearfix alert alert-outline-warning">
            <span class="mini-stat-icon orange"><i class="fa fa-info"></i></span>
            <div class="mini-stat-info">
              <br/>  <h3>Zoom Event has not been activated, kindly contact your zoom administrator </h3>
            </div>

        </div>
    </div>

</div>
<!-- page start-->
<div class="row">
    <h1></h1>
</div>



<!-- page end-->
<script type="text/javascript">
    $('#sortEvent').change(function () {
        var type = $(this).val();
        window.location.href = '<?= url('meeting/') ?>/' + type;
    });
</script>

@endsection
