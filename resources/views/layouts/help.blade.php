 <?php
  $setting=\App\Model\Setting::first();
 ?><div class="modal fade" id="how" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">How it Works</h4>
                        </div>
                        <div class="modal-body">

                            <p>To participate in the event, you need to book for an event by registering your name, phone number and email. Once you register, you will receive a booking reference (invoice) number which you will use to make payments for the event</p>

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">How to Make Payment</h4>
                        </div>
                        <div class="modal-body">

                            <p>Once you have your booking reference (invoice) number, you can choose any available payment channels to make your payments. Existing channels are M-pesa ({{$setting->mno_number}}), Tigo-Pesa ({{$setting->mno_number}}), Airtel Money({{$setting->mno_number}}), NMB Bank Channels and CRDB Bank Channel</p>

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Control Number</h4>
                        </div>
                        <div class="modal-body">

                            <p>This is a special unique number that will be used to identify your payments. Each payment must be associated with unique control number.</p>

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="penalty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Penalty</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            $fee = \App\Model\Fee::first();
                            ?>
                            <p>After payment deadline (<?= $fee->end_date ?>), the invoice amount of Tsh <?= number_format($fee->amount) ?> will be subjected to a penalty fee of Tsh <?= number_format($fee->penalty_amount) ?></p>

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </div>
                </div>
            </div>