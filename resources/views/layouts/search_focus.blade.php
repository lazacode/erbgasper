<?php
$user_ids = '';
foreach ($users as $user) {
    $user_ids .= $user->id . ',';
}
$url = 'bulknametag?ids=' . $user_ids;
$url2 = 'bulknametag?single=1&ids=' . $user_ids;
?>
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            Search Results
        </header>
        <div class="panel-body">
            <div class="col-sm-12">
                <div class="row"  id="search_checked_table">
<!--                    <a type="button" target="_blank" tags='<?= $user_ids ?>' id="bnmetag_link" href="<?= url('user/bulknametag?ids=') . $user_ids ?>" value="" name="" class="btn btn-sm btn-danger"><i class="fa fa-cloud"></i> Print NameTag</a>-->
                    <a type="button" target="_blank" tags='' id="bnmetag_link" href="<?= $url ?>" value="" name="" class="btn btn-sm btn-danger link"><i class="fa fa-cloud"></i> Print NameTag (HP Printer)</a>
                    <a type="button" target="_blank" tags='' id="bnmetag_link2" href="<?= $url2 ?>" value="" name="" class="btn btn-sm btn-primary link"><i class="fa fa-cloud"></i> Print single NameTag (EPSON Printer) </a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr  id="search_checked"></tr>
                        </tbody>
                    </table>
                </div>
                <section class="panel">
                    <header class="panel-heading">
                        Users focus
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th class="numeric col-sm-2">Print Count</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $user_ids = '';
                                foreach ($users as $user) {
                                    $user_ids .= $user->id . ',';
                                    ?>
                                    <tr  id="row<?= $user->id ?>">
                                        <td>{{$i}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->nametagPrintlog()->count()}}</td>
                                        <td>                      <a href="<?= url('user/profile/' . $user->id) ?>" class="btn btn-xs btn-success">View</a>
                                            &nbsp; <input type="checkbox" class="check" checked="" name="select[]" value="<?= $user->id ?>"/>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            <span id="user_id_tags" content="<?= $user_ids ?>"></span>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
            <br/><hr/>

        </div>
    </section>
</div>
<script type="text/javascript">

    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="bs_table' + value + '">' + text + '</tr>');
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#bnmetag_link').attr('tags', param.join(","));
                $('#bnmetag_link').attr('href', url + param.join(","));

                $('#bnmetag_link2').attr('tags', param.join(","));
                $('#bnmetag_link2').attr('href', url2 + param.join(","));
                console.log(param);

            } else {
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#bnmetag_link').attr('tags', result.join(","));
                $('#bnmetag_link').attr('href', url + result.join(","));


                $('#bnmetag_link2').attr('tags', param.join(","));
                $('#bnmetag_link2').attr('href', url2 + param.join(","));

                $('#bs_table' + value).remove();
            }
        });
    }
    $(document).ready(search_checked);
</script>
