@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                SMS

            </header>
            <?php if (can_access('add_sms')) { ?>
                <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal">
                        Compose SMS
                    </a></p>
            <?php } ?>
            <div class="panel-body">
                <section id="unseen">
                    <table id="sms_ajax_example" class="table table-bordered table-striped table-condensed" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Phone</th>
                                <th class="numeric">Message</th>

                                <th class="numeric">status</th>
                                <th class="numeric">Sent time</th>
                                <th class="numeric col-md-2">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Phone</th>
                                <th class="numeric">Message</th>

                                <th class="numeric">status</th>
                                <th class="numeric">Sent time</th>
                                <th class="numeric col-md-2">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Send New SMS</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel-body">

                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Event Name</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" id="event_id" name="event_id">
                                                <option value=""></option> 
                                                <?php
                                                $events = \App\Model\Event::all();
                                                ?>
                                                @foreach($events as $event)
                                                <option value="{{$event->id}}">{{$event->name}}</option>    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">To (required)</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" name="to" id="user_type_check">
                                                <option value="0">All</option> 
                                                <?php $user_types = \App\Model\User_type::all(); ?>
                                                @foreach ($user_types as $user_type)
                                                <option value="{{$user_type->id}}">{{$user_type->name}}</option>                                                  @endforeach;
                                                <option value="virtual">Virtual Attendee All</option>
                                                <option value="physical">Physical Attendee All</option>
                                                <option value="virtual_sponsored7">Virtual Sponsored</option>
                                                <option value="physical_sponsored7">Physical Sponsored</option>
                                                <option value="virtual_sponsored9">Virtual Non-Sponsored</option>
                                                <option value="physical_sponsored9">Physical Non-Sponsored</option>
                                                <option value="write">Custom Number</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group " id="phones" style="display: none">
                                        <label for="phones" class="control-label col-lg-3">Phones</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" class="form-control"/>
                                            <span>Write numbers separated by comma</span>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Template</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" id="template" name="template">
                                                <option value=""></option> 
                                                <?php
                                                $templates = \App\Model\Sms_template::all();
                                                ?>
                                                @foreach($templates as $template)
                                                <option value="{{$template->id}}">{{$template->name}}</option>    @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Message (required)</label>
                                        <div class="col-lg-6">
                                            <textarea class=" form-control" id="message" name="message" minlength="2" width="100%" type="text" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Show Harshtag Guide</label>
                                        <div class="col-lg-6">
                                            <div>
                                                <a class="label label-default mb-2 mb-lg-0 badge badge-success" onclick="$('#collapseExample').toggle()" >
                                                    Click to Show Hashtag Guide
                                                </a> 
                                                <div id="collapseExample" style="display: none">
                                                    <div class="card mb-0 card-body">
                                                        <div class="table-responsive">

                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Tag Name</th>
                                                                        <th>What it will pick</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>#name</td>
                                                                        <td>First Name + Last Name</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>#paid_amount</td>
                                                                        <td> Paid Amount</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>#zoom_meeting_id</td>
                                                                        <td> Zoom Meeting ID</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>#zoom_meeting_url</td>
                                                                        <td> Zoom Meeting url</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>#event_name</td>
                                                                        <td>Event Name</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <?= csrf_field() ?>
                                <input type="hidden" name="created_by" value="<?= Auth::user()->id ?>"/>
                                <input type="hidden" name="user" value="sms"/>
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-success" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#sms_ajax_example').DataTable({
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            'ajax': {
                'url': "<?= url('ajaxTable/?page=sms&type=sms') ?>"
            },
            "columns": [
                {"data": "row_id"},
                {"data": "name"},
                {"data": "phone"},
                {"data": "body"},
                {"data": "status"},
                {"data": "created_at"},
                {"data": ""}
            ],
            "columnDefs": [{
                    "targets": 6,
                    "data": null,
                    "render": function (data, type, row, meta) {
                        var del = '';
                        var resend = '';
<?php if (can_access('delete_sms')) { ?>

                            del = '<form method="POST" action="<?= url('inbox/') ?>/' + row.id + '" accept-charset="UTF-8" class=""><?= csrf_field() ?><input name="_method" type="hidden" value="DELETE"><input name="type" type="hidden" value=""><input class="btn btn-xs btn-danger" type="submit" value="Delete"></form>';
<?php } ?>
<?php if (can_access('resend_sms')) { ?>
                            resend = '<a href="#" id="resend' + row.id + '" onclick="return false" onmousedown="resend(' + row.id + ')" class="btn btn-xs btn-success">resend</a>';
<?php } ?>
                        return resend + del;
                    }

                }],
            rowCallback: function (row, data) {

                //$(row).addClass('selectRow');
                $(row).attr('id', 'row' + data.id);
            }
        });

    });
    user_type_check = function () {
        $('#user_type_check').change(function () {
            var type = $(this).val();
            if (type == 'write') {
                $('#phones').show();
            } else {
                $('#phones').hide();
            }
        });
    }
    function resend(a) {
        $.ajax({
            type: 'GET',
            url: "<?= url('inbox/resend') ?>",
            data: {id: a},
            dataType: "html",
            success: function (data) {
                $('#resend' + a).html('sent');
                $('#resend' + a).attr('disabled', 'disabled');
            }
        });
    }
    template = function () {
        $('#template').change(function () {
            var template = $(this).val();
            $.ajax({
                type: 'GET',
                url: "<?= url('inbox/getTemplate') ?>",
                data: {template: template},
                dataType: "html",
                success: function (data) {
                    $('#message').html(data);
                }
            });
        });
    }
    $(document).ready(template);
    $(document).ready(user_type_check);
</script>
@endsection