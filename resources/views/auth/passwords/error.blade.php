@extends('layouts.app')

@section('content')
<div class="container">

    <div class="card-header"></div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        <form class="form-signin" style="padding: 1%; max-width: 50%" method="POST" action="{{ route('password.email') }}">
            <h2 class="form-signin-heading">{{ __('Reset Password') }}</h2>
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-12 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-lg btn-login btn-block">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </div>
        </form>

    </div>
    @endsection
